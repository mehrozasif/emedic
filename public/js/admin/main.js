$(document).ready(function(){   
	var $ = jQuery;

	$('.emedic-model-container').fadeOut(0);

	$('.emedic-model-close').click(function (e){
		e.preventDefault();

		var obj = $(this).parents('.emedic-model-container');

		obj.fadeOut();
	});

	
	setTime();

	setInterval(setTime,1000);



	if($('input[name="schedule-add-btn"]').length){
		$('input[name="schedule-add-btn"]').click(function(e){
			e.preventDefault();

			var day = $('select[name="schedule-day"]').find(":selected");
			var stime = $('input[name="schedule-start-time"]');
			var etime = $('input[name="schedule-end-time"]');
			var doctor_id = $('input[name="schedule-user-id"]');
			if(day.val() == '' || stime.val() == '' || etime.val() == ''){
				return false;
			}

			var token = $('meta[name="csrf_token_ajax"]').attr('content');

			$.ajax({
			    url: window.location.origin+'/dashboard/doctor/schedule/add',
			    type: 'post',
			    data: {
					_token:token,
					day:day.val(),
					stime:stime.val(),
					etime:etime.val(),
					doctor_id:doctor_id.val()
				},
			    dataType: 'json',
			    success: function (data) {
					if(data.success == '1'){
					    	if($('.no-schedule-tr').length){
					    		$('.no-schedule-tr').remove();
					    	}
					    	$('.schedules-table').append(`
					    		<tr>
					    			<th>`+data.day+`</th>
					    			<td>`+data.time+`</td>
					    			<td><a href="`+data.id+`" class="remove-schedule">Remove</a></td>
					    		</tr>
					    	`);

						$('.schedules-table tr:last-of-type .remove-schedule').click(removeSchedule);
						return;
					}
					alert('Something went wrong!');
			    },
			    error: function(data){
			    	console.log(data.responseText);
			    }
			});

		});

		$('.remove-schedule').click(removeSchedule);
	}


	if($('.app-approve-btn').length){
		$('.app-approve-btn').click(function(e){
			e.preventDefault();

			
			var a = $(this);
			var container = $(this).parents('tr');
			var app_id = a.attr('href');
			var doctor_id = a.data('doctorid');

			var token = $('meta[name="csrf_token_ajax"]').attr('content');

			$.ajax({
			    url: window.location.origin+'/dashboard/doctor/appointment/approve',
			    type: 'post',
			    data: {
					_token:token,
					app_id:app_id,
					doctor_id:doctor_id
				},
			    dataType: 'json',
			    success: function (data) {

			    	console.log(data);
					if(data.success == '1'){
						container.remove();
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
			    },
			    error: function(data){
			    	console.log(data.responseText);
			    }
			});

		});
	}

	if($('.app-decline-btn').length){
		$('.app-decline-btn').click(function(e){
			e.preventDefault();

			
			var a = $(this);
			var container = $(this).parents('tr');
			var app_id = a.attr('href');

			var token = $('meta[name="csrf_token_ajax"]').attr('content');

			$.ajax({
			    url: window.location.origin+'/dashboard/doctor/appointment/decline',
			    type: 'post',
			    data: {
					_token:token,
					app_id:app_id,
				},
			    dataType: 'json',
			    success: function (data) {
					if(data.success == '1'){
						container.remove();
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
			    },
			    error: function(data){
			    	console.log(data.responseText);
			    }
			});

		});
	}

	if($('.select-doctor-schedule-form').length){
		$('.select-doctor-schedule-form').submit(function(e){
			e.preventDefault();

			var patient_id = $('.select-doctor-schedule-patient-id').val();
			var doctor_id = $('.select-doctor-schedule-doctor-id').val();

			var token = $('meta[name="csrf_token_ajax"]').attr('content');


			var app_date = $('input[name="select-doctor-schedule-date"]').val();
			var app_time = $('input[name="select-doctor-schedule-time"]').val();
			var type = $('select[name="select-doctor-schedule-type"]').find(":selected");


			var reason = $('textarea[name="select-doctor-schedule-reason"]');

			if(app_date == '' || app_time == '' || reason.val() == '' || type.val() == ''){
				alert('Enter valid inputs');
				return false;
			}

			$.ajax({
				url: window.location.origin+'/request_appointment',
				type: 'post',
				data: {
					_token:token,
					patient_id:patient_id,
					doctor_id:doctor_id,
					reason:reason.val(),
					app_date:app_date,
					app_time:app_time,
					app_type:type.val(),
					approved:'yes',
					isAjax:'yes'
				},
				dataType: 'json',
				success: function (data) {

					$('.emedic-model-container').fadeOut();
					doctorCallAppointmentS = false;
					if(data.success == '1'){
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
				},
				error: function(data){
					$('.emedic-model-container').fadeOut();
					console.log(data.responseText);
				}
			});
		});
	}


	if($('.doctor-call-appointment').length){
		$('.doctor-call-appointment').click(function(e){
			e.preventDefault();

			
			var a = $(this);
			var patient_id = a.attr('href');
			var doctor_id = a.data('doctorid');


			$('.emedic-model-container').fadeIn();

			$('.select-doctor-schedule-patient-id').val(patient_id);
			$('.select-doctor-schedule-doctor-id').val(doctor_id);

		});
	}


	if($('.doctor-mark-patient-treated').length){
		$('.doctor-mark-patient-treated').click(function(e){
			e.preventDefault();

			var report_id = $(this).attr('href');

			var token = $('meta[name="csrf_token_ajax"]').attr('content');


			$.ajax({
				url: window.location.origin+'/dashboard/doctor/report/complete',
				type: 'post',
				data: {
					_token:token,
					report_id:report_id,
				},
				dataType: 'json',
				success: function (data) {

					if(data.success == '1'){
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});
		});
	}

	if($('.app-mark-taken-btn').length){
		$('.app-mark-taken-btn').click(function(e){
			e.preventDefault();

			var id = $(this).attr('href');

			var token = $('meta[name="csrf_token_ajax"]').attr('content');

			$.ajax({
				url: window.location.origin+'/dashboard/doctor/appointment/taken',
				type: 'post',
				data: {
					_token:token,
					app_id:id,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});


		});
	}

	if($('textarea[name="post_content"]').length){
		$('textarea[name="post_content"]').summernote({
		  	height: 250,
		  	onChange: function(contents, $editable) {
		        $('textarea[name="post_content"]').html(contents);
		    },
		  	codemirror: {
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'default'
          	}
		});
	}


	if($('.blog-remove-btn').length){
		$('.blog-remove-btn').click(function(e){
			e.preventDefault();

			var confirm = window.confirm('Are you sure?');

			if(!confirm){
				return;
			}

			var id = $(this).attr('href');

			var token = $('meta[name="csrf_token_ajax"]').attr('content');

			$.ajax({
				url: window.location.origin+'/dashboard/doctor/blog/delete',
				type: 'post',
				data: {
					_token:token,
					post_id:id,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){
						window.location.reload();
						return;
					}
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});


		});

	}
	if($('.remove-image-preview').length){
		$('.remove-image-preview').click(function(e){
			var confirm = window.confirm('Are you sure?');

			if(!confirm){
				return;
			}
			
			$(this).parents('.row.mt-3').remove();
			$('input[name="delete_image"]').val('yes');

		});
	}
	
	
});
function removeSchedule(e){
	e.preventDefault();

	var confirm = window.confirm('Are you sure?');

	if(!confirm){
		return;
	}

	var token = $('meta[name="csrf_token_ajax"]').attr('content');

	var a = $(this);
	var container = $(this).parents('tr');
	var schedule_id = a.attr('href');

	jQuery.ajax({
		url: window.location.origin+'/dashboard/doctor/schedule/delete',
		type: 'post',
		data: {
			_token:token,
			schedule_id:schedule_id,
		},
		dataType: 'json',
		success: function (data) {

			if(data.success == '1'){
				container.remove();
				return;
			}

			alert('Something went wrong!');

			console.log(data);
		},
		error: function(data){
			console.log(data.responseText);
		}
	});
}
function setTime(){
	if($('.plugin-clock').length){

		$('.plugin-clock').html((moment().format("HH"))+`<span>:</span>`+(moment().format("mm")));

	}

	if($('.plugin-date').length){

		$('.plugin-date').html(moment().format('dddd')+', '+moment().format('MMMM')+' '+moment().format('D')+', '+moment().format('YYYY'));
	
	}
}