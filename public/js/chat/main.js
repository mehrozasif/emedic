$(document).ready(function(){   
	if(!$('.chat-form').length){
		return;
	}

	var doingChatAjax = false;

	// Enable pusher logging - don't include this in production
	Pusher.logToConsole = true;

	var pusher = new Pusher('549d593a81889eee8263', {
	  	cluster: 'ap2',
	   	forceTLS: true
	});
	var channel = pusher.subscribe('chat-channel');

	channel.bind('message-send', function(data) {
	   	console.log(data);

		var receiverid = $('meta[name="chat_receiver_id"]').attr('content');
		var senderid = $('meta[name="chat_sender_id"]').attr('content');

	   	if(data.receiverid == senderid){
	   		appendChatMessage(data.chat,'chat-text-left');
	   		makechatseen(data.chat.id);
	   	}
	   	else if(data.receiverid == receiverid){
	   		appendChatMessage(data.chat,'chat-text-right');
	   	}
	});

	$('.chat-view-messages').animate({ scrollTop: $('.chat-user-list-view')[0].scrollHeight}, 1000);


	function appendChatMessage(chat, classname){
		var html = `<div class="row" title="`+chat.time+`" >
                    	<div class="col-md-12">
                            <div class="chat-user-message `+classname+`">
                        		<p>`+chat.message+`</p>
                                <p><small>`+chat.time+`</small></p>
                            </div>
                  		</div>
                   	</div>`;
        if($('.chat-no-messages').length){
        	$('.chat-no-messages').remove();
        }
        $('.chat-user-list-view').append(html);
		$('.chat-view-messages').animate({ scrollTop: $('.chat-user-list-view')[0].scrollHeight}, 1000);
	}

	function makechatseen(id){
		if(doingChatAjax){
			return;
		}

		var token = $('meta[name="csrf_token_ajax"]').attr('content');

		doingChatAjax = true;
		jQuery.ajax({
			url: window.location.origin+'/chat/message/seen',
			type: 'post',
			data: {
				_token:token,
				chatid:id,
			},
			dataType: 'json',
			success: function (data) {
				if(data.success == '1'){
					doingChatAjax = false;
					return;
				}
			},
			error: function(data){
				console.log(data.responseText);
			}
		});

	}

	alert(window.location.origin);
	$('.chat-form').submit(function(e){
		e.preventDefault();
		if(doingChatAjax){
			return;
		}

		var form = $(this);
		var message = $('.chat-message-input',form);

		var receiverid = $('meta[name="chat_receiver_id"]').attr('content');
		var senderid = $('meta[name="chat_sender_id"]').attr('content');

		var token = $('meta[name="csrf_token_ajax"]').attr('content');

		doingChatAjax = true;
		jQuery.ajax({
			url: window.location.origin+'/chat/message/send',
			type: 'post',
			data: {
				_token:token,
				message:message.val(),
				receiverid:receiverid,
				senderid:senderid,
			},
			dataType: 'json',
			success: function (data) {
				if(data.success == '1'){
					message.val('');
					doingChatAjax = false;
					return;
				}
			},
			error: function(data){
				console.log(data.responseText);
			}
		});
	});

});
