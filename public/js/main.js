var $ = jQuery;

Doctorsearchkey = '';
Doctorsearchspec = '';
Doctorsearchgrade = '';
Doctorsearchgender = '';
$(document).ready(function(){
	/* common  */

	if($('.input-select-text').length){
		$('.input-select-text').each(function(){
			var container = $(this);
			console.log('#'+container.data('options-container'));
			container.click(function(){
				console.log('#'+container.data('options-container'));
				var options = $('#'+container.data('options-container'));
				if(options.hasClass('show')){
					options.removeClass('show');
				}
				else{
					options.addClass('show');
				}
			});
		});
	}

	if($('#search-doctor-form').length){
		$('#search-doctor-form').submit(function(e){
			e.preventDefault();
		});
	}

	$('.search_doctor_inputs').not('input[name="search-doctor"]').attr('disabled','true');

	if($('input[name="search-doctor"]').length){

		$('input[name="search-doctor"]').change(function(){

			if($(this).val() == '' || $(this).val() == ' '){
				$('.search_doctor_inputs').not(this).attr('disabled','true');
			}
			else{
				$('.search_doctor_inputs').not(this).removeAttr('disabled');
			}
			Doctorsearchkey = $(this).val();
			searchInputChange();
		});
	}

	if($('select[name="search_doctor_specialization"]').length){

		$('select[name="search_doctor_specialization"]').change(function(){
			Doctorsearchspec = $(this).val();
			searchInputChange();
		});
	}

	if($('select[name="search_doctor_grade"]').length){

		$('select[name="search_doctor_grade"]').change(function(){
			Doctorsearchgrade = $(this).val();
			searchInputChange();
		});
	}

	if($('select[name="search_doctor_gender"]').length){

		$('select[name="search_doctor_gender"]').change(function(){
			Doctorsearchgender = $(this).val();
			searchInputChange();
		});
	}

	$('.search_clear_filters').click(function(e){
		e.preventDefault();

		$('select[name="search_doctor_gender"] > option:first-of-type,select[name="search_doctor_grade"] > option:first-of-type,select[name="search_doctor_specialization"] > option:first-of-type').prop('selected','selected');
		Doctorsearchspec = '';
		Doctorsearchgrade = '';
		Doctorsearchgender = '';
		searchInputChange();
	});


	if($('.aplhabetic-letters-inputs .aplhabetic-letter').length){
		$('.aplhabetic-letters-inputs .aplhabetic-letter').click(function(e){
			e.preventDefault();

			var char = $(this).data('id');


			var token = $('meta[name="csrf_token"]').attr('content');

			$.ajax({
				url: window.location.origin+'/blog-search/character',
				type: 'post',
				data: {
					_token:token,
					char:char,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){
						$('.health-library-results-row').html(data.html); //output the post links html
						return;
					}
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});
		});
	}

	if($('.profile-edit-btn').length){
		$('.profile-edit-btn').click(function(e){
			e.preventDefault();

			var btn = $(this);

			var obj = btn.parent();

			var form = $('.profile-field-change-form',obj);

			if(!form.hasClass('opened')){
				form.addClass('opened');
				form.slideDown();
				$('> p',form.parent()).slideUp();
			}
			else{

				form.removeClass('opened');
				form.slideUp();
				$(' > p',form.parent()).slideDown();
			}
		});
	}

	if($('.profile-edit-dp-btn').length){
		$('.profile-edit-dp-btn').click(function(e){
			e.preventDefault();

			$('input[name="profile_image"]').click();
		});
	}
	if($('input[name="profile_image"]').length){
		$('input[name="profile_image"]').change(function(e){
			e.preventDefault();

			var token = $('meta[name="csrf_token"]').attr('content');
			var formData = new FormData();
			formData.append('profile_image', this.files[0]);
			formData.append('_token', token);
			$.ajax({
			    url: window.location.origin+'/profile-dp-edit',
			    type: 'POST',
			    data: formData,
			    contentType: false,
			    processData: false,
			    //Ajax events
			    success: function(data){

			        console.log(data);
			        if(data.success == '1'){
			        	console.log(data.imagepath);
			        	$('.profile-image > img').attr('src',data.imagepath);
			        }
			        else{
						alert('Something went wrong!');
			        }

			    },
			    error: function(data){

			        console.log(data.responseText);

			    }

			});
		});
	}

	if($('.profile-remove-dp-btn').length){
		$('.profile-remove-dp-btn').click(function(e){
			e.preventDefault();


			var token = $('meta[name="csrf_token"]').attr('content');

			var parent = $(this).parents('.profile-image');

			var img = $('img',parent);

			$.ajax({
				url: window.location.origin+'/profile-dp-remove',
				type: 'post',
				data: {
					_token:token,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){

						img.attr('src',data.imagesrc);
						return;
					}
					console.log(data);
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});
		});
	}

	if($('.profile-field-change-form').length){
		$('.profile-field-change-form').submit(function(e){
			e.preventDefault();
			var token = $('meta[name="csrf_token"]').attr('content');
			var form = $(this);

			var table = form.data('table');

			var val = $('.input-val',form).val();
			var field = $('.input-val',form).attr('name');

			$.ajax({
				url: window.location.origin+'/profile-edit',
				type: 'post',
				data: {
					_token:token,
					table:table,
					field:field,
					value:val,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){

						form.removeClass('opened');
						form.slideUp();
						$(' > p',form.parent()).text(val).slideDown();

						return;
					}
					form.removeClass('opened');
					form.slideUp();
					$(' > p',form.parent()).slideDown();
					
					console.log(data);
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});
		});
	}


	if($('.rate-doctor-btn').length){
		$('.rate-doctor-btn').click(function(e){

		});
	}

	if($('.search-doctor-rating-stars-form').length){
		$('.search-doctor-rating-stars-form').mousemove(function(e){

			if($(this).hasClass('selected')){
				return e;
			}
			var parentOffset = $(this).parent().offset(); 
		   	var relX = e.pageX - parentOffset.left;

		    $('.rating-full',$(this)).css({'width': relX+"px"}); 
		    var widthperc = relX*100 / $('.rating-empty',$(this)).outerWidth();

		    widthperc = (widthperc * 5) / 100;

		    if(widthperc < 0){
		    	widthperc = 0;
		    }

		    if(widthperc > 4.6){
		    	widthperc = 5.0;
		    }


		    widthperc = parseFloat( widthperc ).toFixed(2);

		    var form = $(this).parents('.rate-doctor-form');

		    $('input[name="rating_doctor"]',form).val(widthperc);

		    $('.rate-num',form).html(widthperc);

		});

		$('.search-doctor-rating-stars-form').click(function(e){
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}
			else{
				$(this).addClass('selected');
			}
		});

		$('.rate-doctor-form').submit(function(e){
			e.preventDefault();

			var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
			var form = $(this);
			var doctorID = $('input[name="doctor_id"]',form).val();
			var rating = $('input[name="rating_doctor"]',form).val();
			var comment = $('textarea[name="comment"]',form).val();

			var confirm = window.confirm("Are you sure about this feedback?\nRate: "+rating+"\nComment: "+comment+"\nFeedback can't be changed.");

			if(!confirm){
				return;
			}

			$.ajax({
				url: window.location.origin+'/rate-doctor',
				type: 'post',
				data: {
					_token:CSRF_TOKEN,
					doctor_id:doctorID,
					rating:rating,
					comment:comment,
				},
				dataType: 'json',
				success: function (data) {
					if(data.success == '1'){

						window.location.reload();
						return;
					}
					
					console.log(data);
					alert('Something went wrong!');
				},
				error: function(data){
					alert('Something went wrong!!');
					console.log(data.responseText);
				}
			});

		});
	}


});

function searchInputChange(){

	var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
	$('.search_doctor_inputs').attr('disabled','true');
	$('.search-doctor-results-container').addClass('search-doctor-results-container-after');
	$.ajax({
	    /* the route pointing to the post function */
		url: '/search_doctor',
	    type: 'POST',
	    /* send the csrf-token and the input to the controller */
	    data: {
	    	_token: CSRF_TOKEN, 
	    	searchkey:Doctorsearchkey,
	    	specialization:Doctorsearchspec,
	    	grade:Doctorsearchgrade,
	    	gender:Doctorsearchgender,
	    },
	    dataType: 'JSON',
	    /* remind that 'data' is the response of the AjaxController */
	    success: function (data) { 
	        console.log(data);

	        if(data.status == 'success'){
	        	$('.search-doctor-results').html(data.html);
	        	$('.search-doctor-results-count').text(data.count);
	        }
	        else{
	        	$('.search-doctor-results-count').text('');
	        }

			$('input[name="search-doctor"]').removeAttr('disabled');
			if(Doctorsearchkey != "" && Doctorsearchkey != " "){
				$('.search_doctor_inputs').not('input[name="search-doctor"]').removeAttr('disabled');
			}
			$('.search-doctor-results-container').removeClass('search-doctor-results-container-after');
	    },
	    error:function(response){
			console.log(response.responseText);

			$('input[name="search-doctor"]').removeAttr('disabled');
			
			if(Doctorsearchkey != "" && Doctorsearchkey != " "){
				$('.search_doctor_inputs').not('input[name="search-doctor"]').removeAttr('disabled');
			}
			$('.search-doctor-results-container').removeClass('search-doctor-results-container-after');
	    }
	}); 
}