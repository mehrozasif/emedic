<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\chat;

use App\Http\Controllers\ChatController;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','display_name','name', 'email', 'password','gender','address','dob','phoneno','image','type','status','last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOnline() {

        $online = (($this->last_online > (new \DateTime('-1 minutes'))->format('Y-m-d H:i:s')) && Auth::check()) ? true : false;

        /*if($online):
            $this->status = '1';
        else:
            $this->status = '0';
        endif;
        $this->save();*/

        return $online;
    }

    public function isAdmin(){

        if($this->type == 'super admin' || $this->type == 'admin'):
            return TRUE;
        endif;

        return FALSE;
    }

    public function getAge(){
        if(empty($this->dob)):
            return '';
        endif;

        return (new \DateTime($this->dob))->diff(Carbon::now())
             ->format('%y years');
    }

    public function hasRole($role = ''){
        
        if(empty($role)):
            return FALSE;
        endif;

        if($this->type == $role):
            return TRUE;
        endif;

        return FALSE;
    }

    public function getUnseenAllChat(){

        //$chatlistDB = DB::select("SELECT * FROM `chats` WHERE `seen`='0' AND ( `receiverid`='{$this->id}' )");

        $chatlistDB = Chat::where( ['receiverid'=> $this->id, 'seen' => '0'] )->get();
        /*$chatlistDB = Chat::where( ['receiverid'=> $this->id, 'seen' => '0'] )
            ->orWhere( [ 'senderid' => $this->id, 'seen'=>'0' ] )->orderBy( 'updated_at', 'DESC' )->get();*/

        $chatlist = [];


        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();


            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;

            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['self'] = $ruser;
            if($suser->id == $this->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['self'] = $suser;
            endif;
        endforeach;


        return $chatlist;
    }

    public function getUnseenChat(){

        //$chatlistDB = DB::select("SELECT * FROM `chats` WHERE `seen`='0' AND ( `receiverid`='{$this->id}' )");

        $chatlistDB = Chat::where( ['receiverid'=> $this->id, 'seen' => '0'] )->get();
        /*$chatlistDB = Chat::where( ['receiverid'=> $this->id, 'seen' => '0'] )
            ->orWhere( [ 'senderid' => $this->id, 'seen'=>'0' ] )->orderBy( 'updated_at', 'DESC' )->get();*/

        $chatlist = [];


        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();


            if(ChatController::chatUserInArray($chatlist,$ruser->id,$suser->id) ):
                continue;
            endif;

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;

            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['self'] = $ruser;
            if($suser->id == $this->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['self'] = $suser;
            endif;
        endforeach;


        return $chatlist;
    }

    public function getAllChat(){

        $chatlistDB = Chat::where('receiverid',$this->id)->orWhere('senderid',$this->id)->orderBy('updated_at','DESC')->get();


        $chatlist = [];


        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();


            if(ChatController::chatUserInArray($chatlist,$ruser->id,$suser->id) ):
                continue;
            endif;

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;

            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;


            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['doctor'] = $ruser;
            if($suser->id == $this->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['doctor'] = $suser;
            endif;
        endforeach;

        
        return $chatlist;
    }

}
