<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Doctor;

class Patient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'father_name', 'blood_group', 'symtoms', 'allergies', 'area', 'history'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function getDoctors(){
        $doctors = [];

        $doctordb = Doctor::all();

        foreach ($doctordb as $key => $doctor) {

            if(empty($doctor->patients)):
                continue;
            endif;

            $dexp = array_filter(explode(',',$doctor->patients));

            foreach ($dexp as $value) {
                if($this->user_id == $value):
                    $doctors[$key] = [];
                    $doctors[$key]['user'] = User::where('id',$doctor->user_id)->get()->first();
                    $doctors[$key]['doctor'] = $doctor;
                endif;
            }
        }

        return $doctors;
    }

    public function getDoctorsWithRating(){
        $doctors = [];

        $doctordb = Doctor::all();

        foreach ($doctordb as $key => $doctor) {

            if(empty($doctor->patients)):
                continue;
            endif;

            $dexp = array_filter(explode(',',$doctor->patients));

            foreach ($dexp as $value) {
                if($this->user_id == $value):
                    $doctors[$key] = [];
                    $doctors[$key]['user'] = User::where('id',$doctor->user_id)->get()->first();
                    $doctors[$key]['doctor'] = $doctor;
                    $rdb = FeedbacksDoctor::where(['to_id' => $doctor->user_id, 'from_id' => $this->user_id])->get();

                    $doctors[$key]['rated'] = FALSE;
                    if(!empty($rdb) && !$rdb->isEmpty()):
                        $rdb = $rdb->first();
                        $doctors[$key]['rated'] = $rdb;
                        $doctors[$key]['rating'] = floatval(number_format(floatval($rdb->rating) / floatval(1),2,'.',''));
                    endif;
                endif;
            }
        }

        return $doctors;
    }


    public function hasDoctor(int $id){

        $doctor = Doctor::where(['user_id'=>$id])->get();

        if(empty($doctor) || $doctor->isEmpty()):
            return FALSE;
        endif;
        $doctor = $doctor->first();
        if(empty($doctor->patients)):
            return FALSE;
        endif;

        $dexp = array_filter(explode(',',$doctor->patients));

        foreach ($dexp as $value) {
            if($this->user_id == $value):
                return TRUE;
            endif;
        }

        return FALSE;
    }


}
