<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\User;
use App\Doctor;
use App\FeedbacksDoctor;
use App\Education;
use App\Patient;
use App\Report;
use App\Schedule;
use App\Appointment;
use App\Post;

use Validator;

class DoctorSearchController extends Controller
{
	public function searchDoctor()
    {

        $users = User::where('type','doctor')->get();
        $doctors = Doctor::all();

        $specializations = [];
        $grades = [];
        $ratings = [];
        $i=0;

        foreach ($doctors as $doctor):
            $specialization = $doctor->specialization;
            $grade = $doctor->grade;

            $rating = FeedbacksDoctor::where('to_id',$doctor->user_id)->get();
            $ratings[$i] = [];
            if(!empty($rating) && !$rating->isEmpty()):

            $rate = [];
            $comments = 0;
            foreach($rating as $r):
                $rate[] = $r->rating;
                if(!empty($r->comment)):
                    $comments++;
                endif;
            endforeach;

            $voters = array_sum($rate);

            $ratings[$i]['rating'] = floatval(number_format(floatval($voters) / floatval(count($rating)),2,'.',''));
            $ratings[$i]['rated'] = count($rating); 
            $ratings[$i]['comments'] = $comments; 


            endif;

            if(!in_array($specialization, $specializations)):
                $specializations[] = $specialization;
            endif;

            if(!in_array($grade, $grades)):
                $grades[] = $grade;
            endif;
            $i++;

        endforeach;


        return View('search_doctor',compact('users','doctors','grades','ratings','specializations'));
    }


    public function ajaxSearchDoctor(Request $req){

        $validate = Validator::make($req->all(),[
            'searchkey' => 'required',
            'specialization' => 'required',
            'grade' => 'required',
            'gender' => 'required',
        ]);

        $search = false;
        $e = false;
        $where = false;
        if(!empty($req->searchkey) && empty($req->specialization) && empty($req->grade) && empty($req->gender))
        {
            $e = 'A';
            /*$search = DB::select("SELECT * FROM `doctors` LEFT JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `type`='doctor' GROUP BY `uers`.`id`");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`type`='doctor'");

            $where = [];

        }

        if(!empty($req->searchkey)  && !empty($req->specialization) && empty($req->grade) && empty($req->gender))
        {
            $e .= 'B';

            /*
            $search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `doctors`.`specialization`='{$req->specialization}'  AND `users`.`type`='doctor'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`type`='doctor'");

            $where = [
                '`specialization`' => $req->specialization
            ];

        }

        if(!empty($req->searchkey)   && !empty($req->grade) && empty($req->specialization) && empty($req->gender))
        {
            $e .= 'C';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `doctors`.`grade`='{$req->grade}' AND `users`.`type`='doctor'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`type`='doctor'");

            $where = [
                '`grade`' => $req->grade,
            ];

        }

        if(!empty($req->searchkey)   && !empty($req->gender) && empty($req->grade) && empty($req->specialization) )
        {
            $e .= 'D';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor'");

            $where = [];

        }




        //Gen & Grad
        if(!empty($req->searchkey)   && !empty($req->gender) && !empty($req->grade) && empty($req->specialization) )
        {
            $e .= 'E';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor' AND `doctors`.`grade`='{$req->grade}'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor'");

            $where = [
                '`grade`' => $req->grade,
            ];

        }


        //Gen & grade
        if(!empty($req->searchkey)   && !empty($req->gender) && empty($req->grade) && !empty($req->specialization) )
        {
            $e .= 'F';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor' AND `doctors`.`specialization`='{$req->specialization}'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor'");

            $where = [
                '`specialization`' => $req->specialization,
            ];

        }

        if(!empty($req->searchkey)   && empty($req->gender) && !empty($req->grade) && !empty($req->specialization) )
        {
            $e .= 'G';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `doctors`.`grade`='{$req->grade}' AND `users`.`type`='doctor' AND `doctors`.`specialization`='{$req->specialization}'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`type`='doctor'");

            $where = [
                '`specialization`' => $req->specialization,
                '`grade`' => $req->grade,
            ];

        }

        if(!empty($req->searchkey)   && !empty($req->gender) && !empty($req->grade) && !empty($req->specialization) )
        {
            $e .= 'H';
            /*$search = DB::select("SELECT * FROM `doctors` INNER JOIN `users` ON `users`.`display_name` LIKE '%{$req->searchkey}%' AND `doctors`.`grade`='{$req->grade}' AND `users`.`type`='doctor' AND `doctors`.`specialization`='{$req->specialization}' AND `users`.`gender`='{$req->gender}'");*/

            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`display_name` LIKE '%{$req->searchkey}%' AND `users`.`gender`='{$req->gender}' AND `users`.`type`='doctor'");

            $where = [
                '`specialization`' => $req->specialization,
                '`grade`' => $req->grade,
            ];

        }


        if(empty($req->searchkey)){
            $e .= 'I';
            $usertablesearch = DB::select("SELECT * FROM `users` WHERE `users`.`type`='doctor' LIMIT 12");

            $where = [];
        }


        $html = '';
        $count = 0;
        foreach ($usertablesearch as $key => $data) {

            $found = FALSE;
            if(empty($where) || !$where):
                $ddata = Doctor::where('user_id',$data->id)->get()->first();
                $found = TRUE;
            else:
                $query = "SELECT * FROM `doctors` WHERE";
                $i=0;
                foreach ($where as $key => $value) {
                    if($i ==0):
                        $query .= " {$key}='{$value}'";
                        $i++;
                        continue;
                    endif;
                    $query .= " AND {$key}='{$value}'";
                }
                $query .= " AND `user_id`='{$data->id}'";
                $ddata = DB::select($query);

                if(!empty($ddata) && !empty($ddata[0])):
                    $ddata = $ddata[0];
                    $found = TRUE;
                endif;
            endif;

            if(!$found):
                continue;
            endif;

            $count++;
            $doctordata = [];
            $doctordata['user'] = $data;
            $doctordata['doctor'] = $ddata;

            $rating = FeedbacksDoctor::where('to_id',$data->id)->get();


            $ratings = [];
            
            if(!empty($rating) && !$rating->isEmpty()):
            $rate = [];
            $comments = 0;
            foreach($rating as $r):
                $rate[] = $r->rating;
                if(!empty($r->comment)):
                    $comments++;
                endif;
            endforeach;

            $voters = array_sum($rate);

            $ratings['rating'] = floatval(number_format(floatval($voters) / floatval(count($rating)),2,'.',''));
            $ratings['rated'] = count($rating); 
            $ratings['comments'] = $comments; 

            endif;

            $html .= view('ajax_search_doctor',compact('doctordata','ratings'));
        }
        if(empty($doctordata) || empty($doctordata['user']) || empty($doctordata['doctor'])):
            $html .= view('ajax_search_doctor_no_result');
        endif;

        $response = array(
          'status' => 'success',
          'msg' => 'Succeeded',
          'count'=> $count,
          'html'=> $html,
          'e' => $e
        );
        return response()->json($response); 

        
        $response = array(
          'status' => 'failed',
          'msg' => 'Unexpected Error',
          'query'=> $doctordata,
          'e' => $e
        );

        return response()->json($response); 
    }
}