<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\User;
use App\Doctor;
use App\FeedbacksDoctor;
use App\Education;
use App\Patient;
use App\Report;
use App\Schedule;
use App\Appointment;
use App\Post;
use App\Alert;

class Pages extends Controller
{

    public function home()
    {
        $lastestposts = Post::orderBy('id','desc')->take(4)->get();
        return View('front_page',compact('lastestposts'));
    }

    public function doctorProfile($id){

        $doctor = Doctor::where('user_id',$id)->get();
        if(!$doctor || $doctor->isEmpty()):
            return redirect('/search-doctor');
        endif;

        $doctor = $doctor->first();


        $user = User::where('id',$id)->get()->first();

        $rating = FeedbacksDoctor::where('to_id',$doctor->user_id)->get();

        $ratings = [];
        if(!empty($rating) && !$rating->isEmpty()):
        $rate = [];
        $comments = 0;
        foreach($rating as $r):
            $rate[] = $r->rating;
            if(!empty($r->comment)):
                $comments++;
            endif;
        endforeach;

        $voters = array_sum($rate);

        $ratings['rating'] = floatval(number_format(floatval($voters) / floatval(count($rating)),2,'.',''));
        $ratings['rated'] = count($rating); 
        $ratings['comments'] = $comments; 

        endif;



        $appointment_link = route('login');
        if(Auth::guard()->check()):
            if(Auth::user()->type == 'patient'):
                $appointment_link = url('/request_appointment/'.$doctor->user_id);
            else:
                $appointment_link = '';
            endif;
        endif;

        $chatlink = route('login');
        if(Auth::guard()->check()):
            if(Auth::user()->id == $user->id):
                $chatlink = '';
            elseif(Auth::user()->hasRole('patient')):
                $chatlink = url('/profile/messages/'.$user->name);
            else:
                $chatlink = url('/dashboard/messages/'.$user->name);
            endif;
        endif;


        $educations = Education::where('user_id',$id)->orderBy('id', 'desc')->get();


        $certifications = array_reverse(array_values(explode(',', $doctor->certifications)));

        $awards = array_reverse(array_values(explode(',', $doctor->awards)));


        $schdedulesDB = Schedule::where('doctor_id',$doctor->id)->get();

        $schedules = [];

        $schedules['monday'] = [];
        $schedules['tuesday'] = [];
        $schedules['wednesday'] = [];
        $schedules['thursday'] = [];
        $schedules['friday'] = [];
        $schedules['saturday'] = [];
        $schedules['sunday'] = [];

        $maxschedulecols = 0;

        foreach ($schdedulesDB as $schedule) {

            if($schedule->day == 'monday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'tuesday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'wednesday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'thursday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'friday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'saturday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'sunday'){
                $schedules[$schedule->day][] = $schedule;
            }

            if((sizeof($schedules[$schedule->day])) > $maxschedulecols){
                $maxschedulecols = (int) (sizeof($schedules[$schedule->day]));
            }
        }

        return view('doctor_profile',compact('user','doctor','appointment_link','ratings','educations','certifications','awards','schedules','maxschedulecols','chatlink'));
    }

    public function doctorProfileByName($name){

        $user = User::where('name',$name)->get();
        if(!$user || $user->isEmpty()):
            return redirect('/search-doctor');
        endif;

        $user = $user->first();

        return $this->doctorProfile($user->id);
    }

    public function doctorAppointment($id){

        $user = Auth::user();
        if(!$user->hasRole('patient')){
            return redirect('/');
        }

        $doctor = Doctor::where('user_id',$id)->get();
        if(!$doctor || $doctor->isEmpty()):
            return redirect('/search-doctor');
        endif;

        $doctor = $doctor->first();


        $doctor_user = User::where('id',$id)->get()->first();

        $patient_user = Patient::where('user_id',$user->id)->get()->first();

        $hasreport = Report::where('patient_id',$user->id)->get();

        if(!$hasreport){
            $hasreport = 'no';
        }
        else{
            $hasreport = 'yes';
        }


        $schdedulesDB = Schedule::where(['doctor_id'=>$doctor->id])->get();

        $apps_time = [];

        $apps_time['monday'] = [];
        $apps_time['tuesday'] = [];
        $apps_time['wednesday'] = [];
        $apps_time['thursday'] = [];
        $apps_time['friday'] = [];
        $apps_time['saturday'] = [];
        $apps_time['sunday'] = [];


        foreach ($schdedulesDB as $schedule) {
            if($schedule->day == 'monday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'tuesday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'wednesday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'thursday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'friday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'saturday'){
                $apps_time[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'sunday'){
                $apps_time[$schedule->day][] = $schedule;
            }
        }


        return view('request_appointment',compact('doctor','doctor_user','user','patient_user','hasreport','apps_time'));
    }

    public function healthLibrary()
    {
        return View('health_library');
    }

    public function search()
    {
        return $this->searchDoctor();
    }

    public function alerts(){

        $alerts = Alert::all();

        $alertsMax = Alert::orderBy('affected_no','DESC')->take(1)->get()->first();
        $alertsMin = Alert::orderBy('affected_no','ASC')->take(1)->get()->first();
        return view('alerts', compact('alerts','alertsMax','alertsMin'));
    }
    public function blog()
    {

        $posts = Post::orderBy('id', 'ASC')->take(15)->get();

        $doctors = [];
        foreach ($posts as $key => $post) {
            $doctors[$key] = User::where('id',$post->author_id)->get()->first(); //TO GET AUTHOR'S DISPLAY NAME
        }

        $healthposts = Post::orderBy('id', 'desc')->take(5)->get();


        return View('blog',compact('posts','doctors','healthposts'));
    }
    public function viewPost($slug){

        $post = Post::where('slug',$slug)->get();

        if(empty($post) || $post->isEmpty()):
            return redirect('/blog');
        endif;

        $post = $post->first();

        $doctor = User::where('id',$post->author_id)->get()->first();


        $healthposts = Post::orderBy('id', 'desc')->take(5)->get();

        return view('blog_post',compact('post','doctor','healthposts'));
    }

    public function contact()
    {
        return View('contact');
    }

    public function services()
    {
        return View('services');
    }

    public function patientsVisitors()
    {
        return View('patients_visitors');
    }

    public function reports()
    {
        return View('reports');
    }
    /* NOT USED
    public function registerDoctor(){

        if(Auth::check()):
            return redirect('/');
        endif;

        return View('auth.doctor_register');
    }

    */
    

    public function profile()
    {
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return redirect('/');
        endif;

        $patientdata = Patient::where('user_id',$user->id)->get()->first();

        return view('profile', compact('user','patientdata'));
    }

    public function profileAppointments(){

        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $patientdata = Patient::where('user_id',$user->id)->get()->first();

        $appointments = Appointment::where(['patient_id'=>$user->id,'approved'=>'1'])->orderBy('id', 'DESC')->get();

        $app_data = [];

        foreach ($appointments as $key => $app) {
            $app_data[$key]['app'] = $app;
            $app_data[$key]['doctor'] = User::where('id',$app->doc_id)->get()->first();
            $app_data[$key]['schedule'] = str_replace('&', ' ', $app->date);

            $olddate = (strtotime(explode('&',$app->date)[0])) < time();

            $oldtime = (strtotime(explode('&',$app->date)[1])) < time();

            $app_data[$key]['missed'] = FALSE;
            $app_data[$key]['taken'] = FALSE;

            if($app_data[$key]['app']->taken == '1'){
                $app_data[$key]['taken'] = TRUE;
            }
            elseif( $olddate && $oldtime  ){
                $app_data[$key]['missed'] = TRUE;
            }
            
        }

        return view('profile_appointments', compact('user','patientdata','app_data'));

    }

    public function profileNotifications(){
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $patientdata = Patient::where('user_id',$user->id)->orderBy('id', 'DESC')->get()->first();

        return view('profile_notifications', compact('user','patientdata'));
    }

    public function profileDoctors(){
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $patientdata = Patient::where('user_id',$user->id)->orderBy('id', 'DESC')->get()->first();

        $doctors = $patientdata->getDoctorsWithRating();

        

        

        return view('profile_doctors', compact('user','patientdata','doctors'));
    }

    public function profileMessages(){
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $patientdata = Patient::where('user_id',$user->id)->get()->first();

        return view('profile_messages', compact('user','patientdata'));
    }
    public function profileReports(){
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $reports = Report::where('patient_id',$user->id)->orderBy('id', 'DESC')->get();

        $data = [];

        foreach ($reports as $key => $report) {
            $data[$key] = [];
            $data[$key]['report'] = $report;
            $data[$key]['user'] = User::where('id',$report->doctor_id)->get()->first();

        }

        return view('profile_reports', compact('user','patientdata','data'));
    }
    public function ProfileByName($name){

        $user = User::where('name',$name)->get();

        if(!$user || $user->isEmpty()):
            return redirect('/');
        endif;

        $user = $user->first();

        $loggedUser = Auth::user();

        if($loggedUser->hasRole('patient') || $loggedUser->name == $name):
            return redirect('/profile');
        endif;

        if(!$loggedUser->hasRole('doctor') && !$loggedUser->hasRole('admin')):
            return redirect('/');
        endif;


        if($loggedUser->hasRole('doctor') ):
            $doctortable = Doctor::where('user_id',$loggedUser->id)->get()->first();
            if(!$doctortable->hasPatient($user->id)):
                return redirect('/dashboard');
            endif;
        endif;

        $patientdata = Patient::where('user_id',$user->id)->get()->first();

        return view('profile_patient', compact('user', 'patientdata'));
    }
    public function pageReportView($report_id){

        $patient = Auth::user();
        if( !$patient->hasRole('patient') && !self::isAdmin()):
            return redirect('/dashboard');
        endif;

        $report = Report::where('id',$report_id)->get();
        
        if(empty($report) || $report->isEmpty()):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');

        return view('dashboard.report',compact('data'));
    }

    
}
