<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


use App\User;
use App\Post;
use App\Doctor;
use App\Patient;
use App\Option;
use App\Report;
use App\Activity;
use App\Schedule;
use App\Appointment;
use App\chat;

use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    protected $pusher;



    public function __construct(){
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );
        $this->pusher = new \Pusher\Pusher(
            '549d593a81889eee8263',  //API KEY
            '311bd8461297ce593045', //API SECRET
            '704822', //APP ID
            $options //OPTIONS FOR CLUSTER
        );

    }

    public function messageSend(Request $req){

        $validate = Validator::make($req->all(),[
            'message'=> 'required',
            'senderid'=> 'required',
            'receiverid'=> 'required',
        ]);

        if($validate->passes()){

            $success = Chat::create([
                'receiverid' => $req->receiverid,
                'senderid' => $req->senderid,
                'message' => $req->message,
                'seen' => '0',
            ]);

            if($success):

                $this->pusher->trigger('chat-channel', 'message-send', [
                    'receiverid' => $req->receiverid,
                    'senderid' => $req->senderid,
                    'chat' =>[ 
                        'time' => $success->created_at->format('jS F, Y h:i a'),
                        'message' => $success->message,
                        'id' => $success->id,
                    ],
                ]);

                try { 
                    DB::connection('mysql2')->insert("INSERT INTO `chats` (`receiverid`, `senderid`, `message` ) values (?, ?, ?)", [ $req->receiverid, $req->senderid, $req->message]);
                } catch(\Illuminate\Database\QueryException $ex){ 
                    //nothing
                }

                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);

            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Plese enter valid inputs',
        ]);
    }

    public function messageSeen(Request $req){

        $validate = Validator::make($req->all(),[
            'chatid'=> 'required',
        ]);

        if($validate->passes()){

            $success = Chat::where('id',$req->chatid)->get()->first();

            $success->seen = '1';

            $success = $success->save();

            if($success):

                $this->pusher->trigger('chat-channel', 'message-seen', $success);

                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Plese enter valid inputs',
        ]);
    }

    public function messages(){
        $user = Auth::user();

        if($user->hasRole('doctor')):
            return $this->DoctorMessages();
        elseif($user->hasRole('admin')):
            return $this->AdminMessages();
        endif;

        return $this->PatientMessages();

    }

    public function messageChatUser($name){
        $user = Auth::user();

        if($user->hasRole('doctor')):
            return $this->DoctorChatUser($name);
        elseif($user->hasRole('admin')):
            return $this->AdminChatUser($name);
        endif;

        return $this->PatientChatUser($name);

    }

    public function AdminChatUser($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $userdetails = User::where('name',$name)->get();

        if(empty($userdetails) || $userdetails->isEmpty()):
            return redirect('/dashboard/messages');
        endif;

        $userdetails = $userdetails->first();

        
        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctorscount = count(Doctor::all());

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $seendb = Chat::where(['receiverid'=>$admin->id,'senderid'=>$userdetails->id])->get();

        foreach($seendb as $seen):
            if($seen->seen == '1'):
                continue;
            endif;

            $seen->seen = '1';

            $seen->save();

        endforeach;


        $chatlistDB = Chat::where(['receiverid'=>$userdetails->id,'senderid'=>$admin->id])->get();

        $chatlistDB1 = Chat::Where(['receiverid'=>$admin->id,'senderid'=>$userdetails->id])->get();

        $chatlistDB = $chatlistDB->merge($chatlistDB1)->sortBy('created_at');


        $chatlist = [];

        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;
            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['patient'] = $ruser;
            if($suser->id == $admin->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['patient'] = $suser;
            endif;

        endforeach;

        return View('dashboard.admin.chat.chat_user',compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','chatlist','userdetails'));
    }

    public function PatientChatUser($name){
        $user = Auth::user();

        if($user->hasRole('doctor') ):
            return $this->doctorProfile(Auth::user()->id);
        endif;

        $patientdata = Patient::where('user_id',$user->id)->get()->first();


        $doctor = User::where('name',$name)->get();

        if(empty($doctor) || $doctor->isEmpty()):
            return redirect('/messages');
        endif;

        $doctor = $doctor->first();

        $seendb = Chat::where(['receiverid'=>$user->id,'senderid'=>$doctor->id])->get();

        foreach($seendb as $seen):
            if($seen->seen == '1'):
                continue;
            endif;

            $seen->seen = '1';

            $seen->save();

        endforeach;


        $chatlistDB = Chat::where(['receiverid'=>$user->id,'senderid'=>$doctor->id])->get();

        $chatlistDB1 = Chat::Where(['receiverid'=>$doctor->id,'senderid'=>$user->id])->get();

        $chatlistDB = $chatlistDB->merge($chatlistDB1)->sortBy('created_at');


        $chatlist = [];

        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;
            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['patient'] = $ruser;
            if($suser->id == $user->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['patient'] = $suser;
            endif;

        endforeach;

        return view('patient.chat.chat_user', compact('user','doctor','chatlist'));
    }

    public function DoctorChatUser($name){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $patientdetail = User::where('name',$name)->get();

        if(empty($patientdetail) || $patientdetail->isEmpty()):
            return redirect('/dashboard/messages');
        endif;

        $patientdetail = $patientdetail->first();


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patientsdb = array_filter( explode(',',$doctortable->patients));

        $patients = count( $patientsdb );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        $seendb = Chat::where(['receiverid'=>$doctor->id,'senderid'=>$patientdetail->id])->get();

        foreach($seendb as $seen):
            if($seen->seen == '1'):
                continue;
            endif;

            $seen->seen = '1';

            $seen->save();

        endforeach;


        $chatlistDB = Chat::where(['receiverid'=>$patientdetail->id,'senderid'=>$doctor->id])->get();

        $chatlistDB1 = Chat::Where(['receiverid'=>$doctor->id,'senderid'=>$patientdetail->id])->get();

        $chatlistDB = $chatlistDB->merge($chatlistDB1)->sortBy('created_at');


        $chatlist = [];

        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();

            
            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;
            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['doctor'] = $ruser;
            if($suser->id == $doctor->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['doctor'] = $suser;
            endif;

        endforeach;


        return view('dashboard.doctor.chat.chat_user',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','chatlist','patientdetail'));

    }

    public static function chatUserInArray(array $chatlist,int $id,int $otherid){

        foreach($chatlist as $chat){
            if($chat['sender']->id == $id && $chat['receiver']->id == $otherid):
                return TRUE;
            endif;

            if($chat['receiver']->id == $id && $chat['sender']->id == $otherid):
                return TRUE;
            endif;

        }

        return FALSE;
    }
    


    public function AdminMessages(){
        $admin = Auth::user();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctorscount = count(Doctor::all());

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $chatlistDB = Chat::where('receiverid',$admin->id)->orWhere('senderid',$admin->id)->orderBy('updated_at','DESC')->get();


        $chatlist = [];


        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();


            if($this->chatUserInArray($chatlist,$ruser->id,$suser->id) ):
                continue;
            endif;

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;

            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;


            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['doctor'] = $ruser;
            if($suser->id == $admin->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['doctor'] = $suser;
            endif;
        endforeach;



        return view('dashboard.admin.chat.home',compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','chatlist'));
    }


    public function PatientMessages(){
        $user = Auth::user();

       
        $chatlistDB = Chat::where('receiverid',$user->id)->orWhere('senderid',$user->id)->orderBy('updated_at','DESC')->get();


        $chatlist = [];

        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();

            if( $this->chatUserInArray($chatlist,$ruser->id,$suser->id) ):
                continue;
            endif;

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;
            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;

            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['patient'] = $ruser;
            if($suser->id == $user->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['patient'] = $suser;
            endif;
        endforeach;



        return view('profile_messages',compact('user','chatlist'));
    }

    public function DoctorMessages(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patientsdb = array_filter( explode(',',$doctortable->patients));

        $patients = count( $patientsdb );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());



        $chatlistDB = Chat::where('receiverid',$doctor->id)->orWhere('senderid',$doctor->id)->orderBy('updated_at','DESC')->get();




        $chatlist = [];

        foreach($chatlistDB as $key => $chatdb):
            $ruser = User::where('id',$chatdb->receiverid)->get()->first();
            $suser = User::where('id',$chatdb->senderid)->get()->first();

            if($this->chatUserInArray($chatlist,$ruser->id,$suser->id)  ):
                continue;
            endif;

            $chatlist[$key] = [];
            $chatlist[$key]['chat'] = $chatdb;

            $chatlist[$key]['sender'] = $suser;
            $chatlist[$key]['receiver'] = $ruser;


            $chatlist[$key]['otheruser'] = $suser;
            $chatlist[$key]['doctor'] = $ruser;
            if($suser->id == $doctor->id):
                $chatlist[$key]['otheruser'] = $ruser;
                $chatlist[$key]['doctor'] = $suser;
            endif;
        endforeach;

        return view('dashboard.doctor.chat.home',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','chatlist'));
    }
}
