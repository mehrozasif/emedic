<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

use App\User;
use App\Patient;
use App\Appointment;
use App\FeedbacksDoctor;
use App\Schedule;
use App\Post;
use App\Doctor;

use Validator;

use Illuminate\Support\Facades\DB;

class API extends Controller
{

    public static $apikey = '22689b2c-bdb0-4b1f-8a33-51328e875de3';
    
    public function login(Request $req)
    {

        if(empty($req->token) || $req->token != API::$apikey):
            return response()->json([
                'success' => '0',
                'message' => 'Api Auth failed',
            ]);
        endif;

        
        if($user = Auth::attempt(['email'=>$req->email,'password'=>$req->password],$req->remember)):

            if(Auth::user()->type == 'doctor' ):

                $doctor = Doctor::where('user_id',Auth::user()->id)->get()->first();

                return response()->json([
                        'success' => '1',
                        'message' => 'Successfully logged in',
                        'user' => Auth::user(),
                        'doctor' => $doctor,
                        'type' => 'doctor',
                    ]);
            elseif(Auth::user()->type == 'admin'):
                return response()->json([
                        'success' => '1',
                        'message' => 'Successfully logged in',
                        'user' => Auth::user(),
                        'type' => 'admin',
                ]);
            endif;

            $patient = Patient::where('user_id',Auth::user()->id)->get()->first();
            return response()->json([
                'success' => '1',
                'message' => 'Successfully logged in',
                'user' => Auth::user(),
                'patient' => $patient,
                'type' => 'patient',
            ]);
        endif;

        return response()->json([
            'success' => '0',
            'message' => 'Email or Password is invalid',
        ]);
    }

    public function registerPatient(Request $req){


        if(empty($req->token) || $req->token != API::$apikey):
            return response()->json([
                'success' => '0',
                'message' => 'Auth Token failed',
            ]);
        endif;


        $first_name = ' ';
        if(!empty($req->first_name)){
            $fname = $req->first_name;
        }
        $last_name = ' ';
        if(!empty($req->last_name)){
            $last_name = $req->last_name;
        }
        $display_name = ' ';
        if(!empty($req->display_name)){
            $display_name = $req->display_name;
        }
        $email = ' ';
        if(!empty($req->email)){
            $email = $req->email;
        }
        $password = ' ';
        if(!empty($req->password)){
            $password = $req->password;
        }
        $address = ' ';
        if(!empty($req->address)){
            $address = $req->address;
        }
        $dob = ' ';
        if(!empty($req->dob)){
            $dob = $req->dob;
        }
        $phoneno = ' ';
        if(!empty($req->phoneno)){
            $phoneno = $req->phoneno;
        }
        $gender = ' ';
        if(!empty($req->gender)){
            $gender = $req->gender;
        }
        $father_name = ' ';
        if(!empty($req->father_name)){
            $father_name = $req->father_name;
        }
        $blood_group = ' ';
        if(!empty($req->blood_group)){
            $blood_group = $req->blood_group;
        }
        $area = ' ';
        if(!empty($req->area)){
            $area = $req->area;
        }
        $history = 'no';
        if(!empty($req->history)){
            $history = $req->history;
        }


        $pass_crypt = Hash::make($password);

        $symtoms = '';
        $allergies = '';

        if( !empty($req->symtoms) ):
            $symtoms = $req->symtoms;
        endif;

        if( !empty($req->allergies) ):
            $allergies = $req->allergies;
        endif;

        $user = User::create([
            'name' => CustomAuthController::get_user_name($req->display_name),
            'first_name' => $first_name,
            'last_name' => $last_name,
            'display_name' => $display_name,
            'email' => $email,
            'password' => $pass_crypt,
            'type' => 'patient',
            'address' => $address,
            'gender' => $gender,
            'dob' => $dob,
            'phoneno' => $phoneno,
            'status' => '0',
            'last_online' => new \DateTime
        ]);

        if($req->history = 'yes'):
            $history = 1;
        else:
            $history = 0;
        endif;

        $patient = Patient::create([
            'user_id'=>$user->id,
            'father_name' => $father_name,
            'blood_group' => $blood_group,
            'symtoms' => $symtoms,
            'allergies' => $allergies,
            'area' => $area,
            'history' => $history,
        ]);

        if($user):
            return response()->json([
                'success' => '1',
                'message' => 'Successfully Registered',
                'user' => $user,
                'patient' => $patient,
                'type' => 'patient',
            ]);
        endif;

        return response()->json([
            'success' => '0',
            'message' => 'Unknown error',
        ]);
    }
    public function registerDoctor(Request $req){
        

        if(empty($req->token) || $req->token != API::$apikey):
            return response()->json([
                'success' => '0',
                'message' => 'Auth Token failed',
            ]);
        endif;


        $first_name = ' ';
        if(!empty($req->first_name)){
            $fname = $req->first_name;
        }
        $last_name = ' ';
        if(!empty($req->last_name)){
            $last_name = $req->last_name;
        }
        $display_name = ' ';
        if(!empty($req->display_name)){
            $display_name = $req->display_name;
        }
        $email = ' ';
        if(!empty($req->email)){
            $email = $req->email;
        }
        $password = ' ';
        if(!empty($req->password)){
            $password = $req->password;
        }
        $address = ' ';
        if(!empty($req->address)){
            $address = $req->address;
        }
        $dob = ' ';
        if(!empty($req->dob)){
            $dob = $req->dob;
        }
        $phoneno = ' ';
        if(!empty($req->phoneno)){
            $phoneno = $req->phoneno;
        }
        $gender = ' ';
        if(!empty($req->gender)){
            $gender = $req->gender;
        }

        $grade = ' ';
        if(!empty($req->grade)){
            $grade = $req->grade;
        }

        $department = ' ';
        if(!empty($req->department)){
            $department = $req->department;
        }

        $bio = ' ';
        if(!empty($req->bio)){
            $bio = $req->bio;
        }

        $certifications = ' ';
        if(!empty($req->certifications)){
            $certifications = $req->certifications;
        }

        $interests = ' ';
        if(!empty($req->interests)){
            $interests = $req->interests;
        }

        $specialization = ' ';
        if(!empty($req->specialization)){
            $specialization = $req->specialization;
        }

        $awards = ' ';
        if(!empty($req->awards)){
            $awards = $req->awards;
        }


        $pass_crypt = bcrypt($req->password);


        $imagepath = NULL;

        $user = User::create([
            'name' => CustomAuthController::get_user_name($req->display_name),
            'first_name' => $first_name,
            'last_name' => $last_name,
            'display_name' => $display_name,
            'email' => $email,
            'password' => $pass_crypt,
            'type' => 'doctor',
            'address' => $address,
            'gender' => $gender,
            'dob' => $dob,
            'image'=> $imagepath,
            'phoneno' => $phoneno,
            'status' => '0',
            'last_online' => NULL
        ]);


        $doctor = Doctor::create([
            'user_id'=>$user->id,
            'grade' => $grade,
            'department' => $department,
            'bio' => $bio,
            'certifications' => $certifications,
            'interests' => $interests,
            'awards' => $awards,
            'specialization' => $specialization,
        ]);

        if($user && $doctor):
            return response()->json([
                'success' => '1',
                'message' => 'Successfully Registered',
                'user' => $user,
                'doctor' => $doctor,
                'type' => 'doctor',
            ]);
        endif;
    }


    
}