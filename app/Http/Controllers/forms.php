<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

use App\User;
use App\Patient;
use App\Appointment;
use App\FeedbacksDoctor;
use App\Schedule;
use App\Post;
use App\Report;
use App\Contact;

use Validator;

use Illuminate\Support\Facades\DB;

class Forms extends Controller
{

    public function doctorAppointmentSubmit(Request $request){

        $this->validate($request, [
            'doctor_id'=>'required|int',
            'patient_id' => 'required|int',
            'reason'=>'required|string|min:15|max:350',
            'app_date'=>'required',
            'app_time'=>'required',
            'app_type'=>'required',
        ]);

        $approved = (empty($request->approved)) ? '0':'1';

        $appointment = Appointment::create([
            'doc_id'=> $request->doctor_id,
            'patient_id' => $request->patient_id,
            'type' => $request->app_type,
            'reason' => $request->reason,
            'date' => \date('jS F, Y',strtotime($request->app_date)).'&'.\date('h:i a',strtotime($request->app_time)),
            'approved' => $approved,
            'taken' => '0',
        ]);

        

        if($appointment):
            if(!empty(($request->isAjax))){
                return response()->json([
                    'success' =>'1',
                    'msg' => 'OK'
                ]);
            }
            return redirect()->back()->with('succ_msg', 'Appointment has been sent to doctor.');
        endif;

        if(!empty(($request->isAjax))){
            return response()->json([
                'success' =>'0',
                'msg' => 'Something went wrong!',
            ]);
        }
        return redirect()->back()->with('err_msg', 'Something went wrong, please try again.');
    }

    public function contact(Request $request){

        $this->validate($request, [
            'name'=>'required',
            'email' => 'required|email',
            'subject'=>'required',
            'message'=>'required',
        ]);


        $contact = Contact::create([
            'name'=> $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message
        ]);


        if($contact):
            if(!empty(($request->isAjax))){
                return response()->json([
                    'success' =>'1',
                    'msg' => 'OK'
                ]);
            }
            return redirect()->back()->with('succ_msg', 'Your query has been sent to admin.');
        endif;

        if(!empty(($request->isAjax))){
            return response()->json([
                'success' =>'0',
                'msg' => 'Something went wrong!',
            ]);
        }
        return redirect()->back()->with('err_msg', 'Something went wrong, please try again.');
    }




    /*
        AJAX
    */

    public function ajaxSearchPostChar(Request $req){ //REQUEST CLASS ALL DATA SUBMITTED FROM JS AJAX

        $validate = Validator::make($req->all(),[
            'char'=> 'required',
        ]);

        if($validate->passes()){

            $posts = Post::where('title', 'LIKE',  $req->char. '%' )->get();

            $html = '';

            if(empty($posts) || $posts->isEmpty()):
                $html = <<<HTML
                    <div class="col-md-12">
                        <h3 class="text-center">No Results Found.</h3>
                    </div>
HTML;
            endif;

                //OUTPUT OF BLOG
            foreach ($posts as $key => $post) {

                $url = url('/post/'.$post->slug);

                $image = asset('images/default-image.png');
                if(!empty($post->featured_image)):
                    $image = asset($post->featured_image);
                endif;
                $html .= <<<HTML
                    <div class="col-md-4 mb-2">
                        <div class="health-library-link">
                            <img src="$image" width="100%" />
                            <a href="{$url}">$post->title</a>
                        </div>
                    </div>
HTML;
            }

            if($posts):
                return response()->json([
                    'success' => '1',
                    'html' => $html,
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }


    public function ajaxChangeProfileDP(Request $req){
        if(!Auth::guard()->check() || (!Auth::user()->hasRole('patient') && !Auth::user()->hasRole('admin') ) ):
            return response()->json([
                'success' => '0',
                'msg' => 'Auth Failed'
            ]);
        endif;

        $validate = Validator::make($req->all(),[
            'profile_image'=> 'required',
        ]);


        if(!$validate->passes()){
            return response()->json([
                'success' => '0',
                'msg' => 'Missing required fields',
                'res' => $req->all()
            ]);
        }

        $imagepath = '';


        if(!empty($req->profile_image)){

            $user = Auth::user();

            if(!empty($user->image) && File::exists(public_path($user->image)) ):
                File::delete(public_path($user->image));
            endif;

            $imageName = time().'.'.$req->profile_image->getClientOriginalExtension();
            $req->profile_image->move(public_path('/profileimages'), $imageName);

            $imagepath = '/profileimages/'.$imageName;

            $user->image = $imagepath;

            $success = $user->save();

            if($success){
                return response()->json([
                    'success' => '1',
                    'imagepath'=> url('/').$user->image
                ]);
            }

            return response()->json([
                'success' => '0',
                'msg'=> 'Error'
            ]);

        }

        return response()->json([
            'success' => '0',
            'msg'=> 'Unexpected error'
        ]);

    }

    public function ajaxRemoveProfileDP(Request $req){
        if(!Auth::guard()->check() || (!Auth::user()->hasRole('patient') && !Auth::user()->hasRole('admin') ) ):
            return response()->json([
                'success' => '0',
                'msg' => 'Auth Failed'
            ]);
        endif;

        $user = Auth::user();


        if(!empty($user->image) && File::exists(public_path($user->image)) ):
            File::delete(public_path($user->image));
        endif;


        $user = Auth::user();

        $user->image = NULL;

        $success = $user->save();

        if($success){
            return response()->json([
                'success' => '1',
                'imagesrc'=> asset('images/default-user.jpg')
            ]);
        }

        return response()->json([
            'success' => '0',
            'msg'=> 'Error'
        ]);


    }

    public function pageReportDownload($report_id){

        $patient = Auth::user();
        if( !$patient->hasRole('patient') && !self::isAdmin()):
            return redirect('/dashboard');
        endif;

        $report = Report::where('id',$report_id)->get();

        if(empty($report) || $report->isEmpty()):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');


        $html = view('dashboard.report', compact('data') );

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();


        $dom->getElementsByTagName('body')->item(0)->setAttribute('style', 'background-color:#fff;margin: 0;');

        $html = $dom->saveHTML();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML( $html );
        return $pdf->stream();
    }

    public function ajaxChangeProfileData(Request $req){

        if(!Auth::guard()->check() || ( !Auth::user()->hasRole('patient') && !Auth::user()->hasRole('admin') ) ):
            return response()->json([
                'success' => '0',
                'msg' => 'Auth Failed'
            ]);
        endif;

        $validate = Validator::make($req->all(),[
            'field'=> 'required',
            'table'=> 'required',
            'value'=> 'required',
        ]);

        if($validate->passes()){

            $success = FALSE;
            if($req->table == 'user'):
                $table = Auth::user();
            else:
                $table = Patient::where('user_id',Auth::user()->id)->get()->first();
            endif;

            $field = $req->field;
            $table->$field = $req->value;

            $success = $table->save();

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Missing required fields',
            'res' => $req->all()
        ]);
    }

    public function rateDoctor(Request $req){
        if(!Auth::guard()->check() || !Auth::user()->hasRole('patient') ):
            return response()->json([
                'success' => '0',
                'msg' => 'Auth Failed'
            ]);
        endif;

        $validate = Validator::make($req->all(),[
            'doctor_id'=> 'required',
            'rating'=> 'required',
        ]);


        if($validate->passes()){

            $success = FALSE;

            $user = Auth::user();

            $rating = number_format($req->rating,2,'.','');


            $success = FeedbacksDoctor::create([
                'to_id' => $req->doctor_id,
                'from_id' => $user->id,
                'rating' => $rating,
                'comment' => empty($req->comment) ? '':$req->comment,
            ]);

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Missing required fields',
            'res' => $req->all()
        ]);
    }
}
