<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

use App\User;
use App\Post;
use App\Doctor;
use App\Patient;
use App\Option;
use App\Report;
use App\Activity;
use App\Schedule;
use App\Appointment;
use App\Alert;

class DoctorPanel extends Controller
{

	public function home()
    {
    	$admin = Auth::user();

    	if($admin->hasRole('doctor')):
            return $this->doctorHome();
        elseif($admin->hasRole('patient')):
            return redirect('/');
        elseif(AdminPanel::isAdmin()):
            return redirect('/dashboard/admin/');
    	endif;

        return redirect('/');
    }

    public function doctorHome(){

        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        return View('dashboard.doctor.dashboard',compact('doctor','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count'));
    }

    public function pageProfile(){

        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get();


        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/dashboard');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $schedules = Schedule::where('doctor_id', $doctortable->id)->get();


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        return view('dashboard.doctor.profile',compact('doctor','doctortable','patients','patients_treated','reports_generated','schedules','appointments_requested_count','appointments_count'));
    }

    public function pageSchedules(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $schedules = Schedule::where('doctor_id', $doctortable->id)->get();

        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        return view('dashboard.doctor.schedules',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','schedules','appointments_requested_count','appointments_count'));

    }

    public function pageAppRequest(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $appointments_requested = Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get();

        $appointments_requested_count = count($appointments_requested);

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        $apps_req = [];

        foreach ($appointments_requested as $key => $appointment_requested) {

            $apps_req[$key]['app'] = $appointment_requested;
            $apps_req[$key]['patient'] = User::where('id',$appointment_requested->patient_id)->get()->first();

            $apps_req[$key]['schedule_time'] = str_replace('&', ' ', $appointment_requested->date);
        }



        return view('dashboard.doctor.app_requests',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','apps_req'));
    }

    public function pageAppBooked(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());


        $appointments_booked = Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get();

        $appointments_count = count($appointments_booked);


        $apps_book = [];

        foreach ($appointments_booked as $key => $appointment_booked) {

            $apps_book[$key]['app'] = $appointment_booked;
            $apps_book[$key]['patient'] = User::where('id',$appointment_booked->patient_id)->get()->first();
            //$schedule = Schedule::where('id',$appointment_booked->schedule_id)->get();

            /*$time = 'Schedule Deleted!';
            if($schedule && !$schedule->isEmpty()){
                $schedule = $schedule->first();

                $time = "{$schedule->day} ({$schedule->time})";
            } */

            $olddate = (strtotime(explode('&',$appointment_booked->date)[0])) < time();

            $oldtime = (strtotime(explode('&',$appointment_booked->date)[1])) < time();

            $apps_book[$key]['missed'] = FALSE;
            $apps_book[$key]['taken'] = FALSE;

            if($apps_book[$key]['app']->taken == '1'){
                $apps_book[$key]['taken'] = TRUE;
            }
            elseif( $olddate && $oldtime  ){
                $apps_book[$key]['missed'] = TRUE;
            }

            $apps_book[$key]['schedule_time'] = str_replace('&', ' ', $appointment_booked->date);
        }



        return view('dashboard.doctor.app_booked',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','apps_book'));
    }
    public function pageBlog(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        $posts = Post::where('author_id',$doctor->id)->get();

        return view('dashboard.doctor.blogs',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','posts'));

    }
    public function pageAddPost(){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        return view('dashboard.doctor.add_blog',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count'));

    }
    public function pageEditPost(int $id){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/dashboard');
        endif;


        $post = Post::where(['id'=>$id,'author_id'=>$doctor->id])->get();

        if(empty($post) || $post->isEmpty()):
            return redirect('/dashboard');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty()):
            return redirect('/dashboard');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        $post = $post->first();

        return view('dashboard.doctor.edit_blog',compact('doctor', 'doctortable','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','post'));
    }

    public function pagePatients(){

        $doctor = Auth::user();
        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get();


        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;

        $doctortable = $doctortable->first();

        $patientsdata = array_filter( explode(',',$doctortable->patients));

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $doctor_patients = [];
        $i=0;
        foreach ($patientsdata as $patient_id) {
            $patientdata = Patient::where('user_id',$patient_id)->get()->first();
            $userdata = User::where('id',$patient_id)->get()->first();
            $doctor_patients[$i] = [];
            $doctor_patients[$i]['id'] =  $patientdata->user_id;
            $doctor_patients[$i]['name'] =  $userdata->display_name;
            $doctor_patients[$i]['email'] =  $userdata->email;
            $doctor_patients[$i]['gender'] =  $userdata->gender;
            $doctor_patients[$i]['dob'] =  $userdata->dob;
            $doctor_patients[$i]['age'] =  $userdata->getAge();
            $doctor_patients[$i]['image'] =  $userdata->image;
            $doctor_patients[$i]['type'] =  $userdata->type;
            $doctor_patients[$i]['status'] =  $userdata->status;
            $doctor_patients[$i]['last_online'] =  $userdata->last_online;
            $i++;
        }

        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        $schdedulesDB = Schedule::where(['doctor_id'=>$doctortable->id])->get();

        $schedules = [];

        $schedules['monday'] = [];
        $schedules['tuesday'] = [];
        $schedules['wednesday'] = [];
        $schedules['thursday'] = [];
        $schedules['friday'] = [];
        $schedules['saturday'] = [];
        $schedules['sunday'] = [];

        foreach ($schdedulesDB as $schedule) {

            if($schedule->day == 'monday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'tuesday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'wednesday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'thursday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'friday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'saturday'){
                $schedules[$schedule->day][] = $schedule;
            }
            elseif($schedule->day == 'sunday'){
                $schedules[$schedule->day][] = $schedule;
            }
        }

        return view('dashboard.doctor.patients',compact('doctor','patients','patients_treated','reports_generated','doctor_patients','appointments_requested_count','appointments_count','schedules'));
    }
    public function pagePatient($id){

        $doctor = Auth::user();
        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();

        if(!$doctortable->hasPatient($id)){
            return redirect('/');
        }


        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $patientdata = Patient::where('user_id',$id)->get()->first();

        if(empty($patientdata)):
            return redirect('/dashboard/patients');
        endif;

        $userdata = User::where('id',$patientdata->user_id)->get()->first();

        $patient = [];
        $patient['id'] = $userdata->id;
        $patient['name'] =  $userdata->display_name;
        $patient['email'] =  $userdata->email;
        $patient['gender'] =  $userdata->gender;
        $patient['dob'] =  $userdata->dob;
        $patient['age'] =  $userdata->getAge();
        $patient['image'] =  $userdata->image;
        $patient['type'] =  $userdata->type;
        $patient['status'] =  $userdata->status;
        $patient['last_online'] =  $userdata->last_online;

        $patient['father_name'] =  $patientdata->father_name;
        $patient['blood_group'] =  $patientdata->blood_group;
        $patient['symtoms'] =  $patientdata->symtoms;
        $patient['allergies'] =  $patientdata->allergies;
        $patient['area'] =  $patientdata->area;

        $patient['history'] = Report::where('patient_id',$patient['id'])->get()->reverse();


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        return view('dashboard.doctor.patient',compact('doctor','patients','patients_treated','reports_generated','patient','appointments_requested_count','appointments_count'));
    }

    public function pageReports(){
        $doctor = Auth::user();
        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();


        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());


        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        $reports = Report::where('doctor_id',$doctor->id)->get();

        $patientsdata = [];

        foreach ($reports as $key => $report) {
            $patientsdata[$key] = User::where('id',$report->patient_id)->get()->first();
        }
        $user = [];
        return view('dashboard.doctor.all_reports',compact('doctor','patients','patients_treated','reports_generated','user','reports','appointments_requested_count','appointments_count','reports','patientsdata'));
    }
    public function pageReportPatient($id){
        $doctor = Auth::user();
        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();

        if(!$doctortable->hasPatient($id)){
            return redirect('/');
        }

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $patientdata = Patient::where('user_id',$id)->get()->first();

        if(empty($patientdata)):
            return redirect('/dashboard/doctor/patients');
        endif;



        $user = User::where('id',$patientdata->user_id)->get()->first();

        $reports = Report::where('patient_id',$patientdata->user_id)->get();

        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        return view('dashboard.doctor.reports',compact('doctor','patients','patients_treated','reports_generated','user','reports','appointments_requested_count','appointments_count'));

    }

    public function pageReportCreate($user_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') ):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());


        $user = User::where('id',$user_id)->get()->first();


        return View('dashboard.doctor.report_form',compact('doctor','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','user'));


    }
    public function pageReportEdit($report_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') ):
            return redirect('/');
        endif;

        $doctortable = Doctor::where('user_id',$doctor->id)->get()->first();

        $patients = count( array_filter( explode(',',$doctortable->patients)) );

        $patients_treated = count( array_filter( explode(',',$doctortable->patients_treated)));

        $reports_generated  = count(Report::where('doctor_id',$doctor->id)->get());

        $appointments_requested_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['doc_id' => $doctor->id,'approved'=>'1'])->get());

        $report = Report::where(['id'=>$report_id,'finished'=>'0'])->get();

        if(empty($report) || $report->isEmpty() ):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $user = User::where('id',$report->patient_id)->get()->first();



        return View('dashboard.doctor.report_form_edit',compact('doctor','patients','patients_treated','reports_generated','appointments_requested_count','appointments_count','user','report'));


    }
    public function pageReportView($report_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') && !self::isAdmin()):
            return redirect('/');
        endif;

        $report = Report::where('id',$report_id)->get();

        if(empty($report) || $report->isEmpty()):
            return redirect('/');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');

        return view('dashboard.report',compact('data'));
    }
    public function pageReportDownload($report_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') && !self::isAdmin()):
            return redirect('/dashboard');
        endif;

        $report = Report::where('id',$report_id)->get();

        if(empty($report) || $report->isEmpty()):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');


        $html = view('dashboard.report', compact('data') );

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();


        $dom->getElementsByTagName('body')->item(0)->setAttribute('style', 'background-color:#fff;margin: 0;');

        $html = $dom->saveHTML();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML( $html );
        return $pdf->stream();
    }








    public function formScheduleAdd(Request $req){

        $validate = Validator::make($req->all(),[
            'day'=> 'required',
            'stime'=> 'required',
            'etime'=> 'required',
            'doctor_id'=> 'required',
        ]);

        if($validate->passes()){


            $success = Schedule::create([
                'doctor_id' => $req->doctor_id,
                'day' => $req->day,
                'time' =>  \date('h:i a',strtotime($req->stime)).'-'.\date('h:i a',strtotime($req->etime)),
             //   'booked' => '0',
            ]);

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                    'day' => ucfirst($req->day),
                    'time' => \date('h:i a',strtotime($req->stime)).'-'.\date('h:i a',strtotime($req->etime)),
                    'id' => $success->id,
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }
    public function formScheduleDelete(Request $req){

        $validate = Validator::make($req->all(),[
            'schedule_id'=> 'required',
        ]);

        if($validate->passes()){

            $success = Schedule::where('id',$req->schedule_id)->get()->first()->delete();

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }

    public function pageAppTaken(Request $req){

        $validate = Validator::make($req->all(),[
            'app_id'=> 'required',
        ]);

        if($validate->passes()){

            $success = Appointment::where('id',$req->app_id)->get()->first();

            //$schedule = Schedule::where('id',$success->schedule_id)->get()->first();
            //$schedule->booked = '1';

            $success->taken = '1';


            //$doctor = Doctor::where('id',$req->doctor_id)->get()->first();

            //$doctor->addPatient($success->patient_id);

            $success->save();

           // $schedule->save();


            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }
    public function appointmentApprove(Request $req){


        if(!Auth::user()->hasRole('doctor') && !Auth::user()->hasRole('admin')):
            return response()->json([
                'success' => '0',
                'msg' => 'Auth failed',
            ]);
        endif;

        $validate = Validator::make($req->all(),[
            'app_id'=> 'required',
            'doctor_id'=> 'required',
        ]);

        if($validate->passes()){

            $success = Appointment::where('id',$req->app_id)->get()->first();

            //$schedule = Schedule::where('id',$success->schedule_id)->get()->first();
            //$schedule->booked = '1';

            $success->approved = '1';


            $doctor = Doctor::where('user_id',$req->doctor_id)->get()->first();

            $doctor->addPatient($success->patient_id);

            $success->save();

           // $schedule->save();


            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }

    public function appointmentDecline(Request $req){

        $validate = Validator::make($req->all(),[
            'app_id'=> 'required',
        ]);

        if($validate->passes()){

            $success = Appointment::where('id',$req->app_id)->get()->first()->delete();

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }

    public function pagePostSubmit(Request $req){

        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;



        $this->validate($req, [
            'post_title' => 'required|string',
            'post_content' => 'required',
        ]);

        $imagepath = '';

        if(!empty($req->post_image)){
            $imageName = time().'.'.$req->post_image->getClientOriginalExtension();
            $req->post_image->move(public_path('/postimages'), $imageName);

            $imagepath = '/postimages/'.$imageName;
        }

        $report = Post::create([
            'author_id' => $doctor->id,
            'title' => $req->post_title,
            'slug' => self::get_post_slug($req->post_title),
            'body' => $req->post_content,
            'featured_image' => $imagepath,
        ]);

        if($report):
            return redirect()->back()->with('succ_msg', 'Blog Post Created!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');

    }
    public function pageEditPostSubmit($id,Request $req){
        $doctor = Auth::user();

        if(!$doctor->hasRole('doctor')):
            return redirect('/');
        endif;

        $post = Post::where(['id'=>$id,'author_id'=>$doctor->id])->get();

        if(empty($post) || $post->isEmpty()):
            return redirect('/dashboard');
        endif;


        $doctortable = Doctor::where('user_id',$doctor->id)->get();

        if(empty($doctortable) || $doctortable->isEmpty($doctortable)):
            return redirect('/');
        endif;



        $this->validate($req, [
            'post_title' => 'required|string',
            'post_content' => 'required',
        ]);

        $post = $post->first();

        $imagepath = $post->featured_image;


        if(!empty($req->post_image)){

            if(!empty($post->featured_image) && File::exists(public_path($post->featured_image)) ):
                File::delete(public_path($post->featured_image));
            endif;

            $imageName = time().'.'.$req->post_image->getClientOriginalExtension();
            $req->post_image->move(public_path('/postimages'), $imageName);
            $imagepath = '/postimages/'.$imageName;
        }

        if($req->delete_image == 'yes'){

            if(File::exists(public_path($post->featured_image))):
                File::delete(public_path($post->featured_image));
            endif;

            $imagepath = '';
        }

        $post->title = $req->post_title;
        $post->body = $req->post_content;
        $post->featured_image = $imagepath;

        $success = $post->save();

        if($success):
            return redirect()->back()->with('succ_msg', 'Blog Post Editted!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');
    }
    public function blogPostDelete(Request $req){

        $validate = Validator::make($req->all(),[
            'post_id'=> 'required',
        ]);

        if($validate->passes()){

            $success = Post::where('id',$req->post_id)->get()->first()->delete();

            if($success):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);
    }
    public function pageReportFormSubmit(Request $req){

        $this->validate($req, [
            'doctor_id' => 'required|int',
            'patient_id' => 'required|int',
            'department' => 'required|string',
            'initial_dignosis' => 'required|string',
            'final_dignosis' => 'required|string',
            'disease' => 'required|string',
            'tdisease' => 'required|string',
            'examination' => 'required|string',
            'treatment' => 'required|string',
        ]);

        $report = Report::create([
            'doctor_id' => $req->doctor_id,
            'patient_id' => $req->patient_id,
            'department' => $req->department,
            'initial_dignosis' => $req->initial_dignosis,
            'final_dignosis' => $req->final_dignosis,
            'disease' => $req->disease,
            'tdisease' => $req->tdisease,
            'examination' => $req->examination,
            'treatment' => $req->treatment,
            'finished' => '0',
        ]);

        $puser = Patient::where('user_id', $req->patient_id)->get()->first();


        $area = Alert::where([ 'area' => $puser->area ])->get();

        if(empty($area) || $area->isEmpty()):
            Alert::create([
                'disease' => $req->disease,
                'affected_no' => '1',
                'area' => $puser->area,
                'result' => ' ',
            ]);
        else:
            $area = $area->first();
            if( $req->disease == $area->disease):
                $area->affected_no++;
                $area->save();
            else:
               Alert::create([
                    'disease' => $req->disease,
                    'affected_no' => '1',
                    'area' => $puser->area,
                    'result' => ' ',
                ]);
           endif;
        endif;


        if($report):
            return redirect()->back()->with('succ_msg', 'Report Created!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');

    }
    public function pageReportEditFormSubmit($id, Request $req){

        $this->validate($req, [
            'department' => 'required|string',
            'initial_dignosis' => 'required|string',
            'final_dignosis' => 'required|string',
            'disease' => 'required|string',
            'tdisease' => 'required|string',
            'examination' => 'required|string',
            'treatment' => 'required|string',
        ]);

        $report = Report::where(['id'=>$id,'finished'=>'0'])->get();

        if(empty($report) || $report->isEmpty() ):
            return redirect('/');
        endif;

        $report = $report->first();

        $report->department = $req->department;
        $report->initial_dignosis = $req->initial_dignosis;
        $report->final_dignosis = $req->final_dignosis;
        $report->disease = $req->disease;
        $report->tdisease = $req->tdisease;
        $report->examination = $req->examination;
        $report->treatment = $req->treatment;

        $report = $report->save();


        if($report):
            return redirect()->back()->with('succ_msg', 'Report Editted!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');
    }
    public function ReportComplete(Request $req){

        $validate = Validator::make($req->all(),[
            'report_id'=> 'required',
        ]);

        if($validate->passes()){


            $report = Report::where('id',$req->report_id)->get();

            if(!$report):
                return response()->json([
                    'success' => '0',
                    'msg' => 'Nope',
                ]);
            endif;

            $report = $report->first();

            $report->finished = '1';

            $doctor = Doctor::where('user_id',$report->doctor_id)->get()->first();

            $doctor->addTreatedPatient($report->patient_id);
            $report->save();

            if($report):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);


    }

    public static function get_post_slug($title){
        $slug = strtolower(str_replace(' ', '-', $title));

        $lp = Post::where('slug',$slug)->get();
        if(!$lp->isEmpty()):
            $i = 1;
            $slug = $slug.'-'.$i;
            $lp = Post::where('slug',$slug)->get();
            while(!$lp->isEmpty()):
                $r = 1;
                if($i >= 10):
                    $r = 2;
                elseif($i >= 100):
                    $r = 3;
                elseif($i >= 1000):
                    $r = 4;
                endif;
                $r = (int) $r*-1;
                $i++;
                $slug = substr($slug, 0, $r).$i;
                $lp = Post::where('slug',$slug)->get();
            endwhile;
        endif;

        return $slug;
    }
}
