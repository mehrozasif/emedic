<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

use App\User;
use App\Patient;
use App\Appointment;
use App\FeedbacksDoctor;
use App\Schedule;
use App\Post;

use Validator;

use Illuminate\Support\Facades\DB;

class CustomAuthController extends Controller
{
    
    public function login(Request $req)
    {
        $this->validate($req, [
            'email'=>'required|string|email|max:255|',
            'password' => 'required|string|min:6'
        ]);

        
        if(Auth::attempt(['email'=>$req->email,'password'=>$req->password],$req->remember)):

            if(Auth::user()->type == 'doctor' ):
                return redirect('/dashboard/doctor');
            elseif(Auth::user()->type == 'admin'):
                return redirect('/dashboard/admin');
            endif;

            if(!empty($req->redirect)):
                return redirect($req->redirect);
            endif;
            return redirect('/');
        endif;

        return redirect()->back()->with('err_msg', 'Incorrect email or password')->withInput($req->only(['email','remember']));
    }

    public function registerPatientView(){
        if(Auth::check()):
            return redirect('/');
        endif;

        return View('auth.patient_register');
    }
    public function registerPatient(Request $req){

        $this->validate($req, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string',
            'dob' => 'required|string',
            'phoneno' => 'required|string|min:11|max:11',
            'gender' => 'required|string',
            //Patient Info
            'father_name' => 'required|string',
            'blood_group' => 'required|string',
            'area' => 'required|string',
            'history' => 'required|string',
        ]);

        $pass_crypt = Hash::make($req->password);

        $symtoms = '';
        $allergies = '';

        if( !empty($req->symtoms) ):
            $symtoms = $req->symtoms;
        endif;

        if( !empty($req->allergies) ):
            $allergies = $req->allergies;
        endif;

        $user = User::create([
            'name' => CustomAuthController::get_user_name($req->display_name),
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'display_name' => $req->display_name,
            'email' => $req->email,
            'password' => $pass_crypt,
            'type' => 'patient',
            'address' => $req->address,
            'gender' => $req->gender,
            'dob' => $req->dob,
            'phoneno' => $req->phoneno,
            'status' => '0',
            'last_online' => new \DateTime
        ]);

        if($req->history = 'yes'):
            $history = 1;
        else:
            $history = 0;
        endif;

        $patient = Patient::create([
            'user_id'=>$user->id,
            'father_name' => $req->father_name,
            'blood_group' => $req->blood_group,
            'symtoms' => $symtoms,
            'allergies' => $allergies,
            'area' => $req->area,
            'history' => $history,
        ]);

        if($user):
            Auth::attempt(['email'=>$req->email,'password'=>$req->password],'on');
            return redirect('/');
        endif;

        return redirect()->back()->withInput($req->only(['email','name']));
    }


    public static function get_user_name($title){
        $slug = strtolower(str_replace(' ', '-', $title));

        $lp = User::where('name',$slug)->get();
        if(!$lp->isEmpty()):
            $i = 1;
            $slug = $slug.'-'.$i;
            $lp = User::where('name',$slug)->get();
            while(!$lp->isEmpty()):
                $r = 1;
                if($i >= 10):
                    $r = 2;
                elseif($i >= 100):
                    $r = 3;
                elseif($i >= 1000):
                    $r = 4;
                endif;
                $r = (int) $r*-1;
                $i++;
                $slug = substr($slug, 0, $r).$i;
                $lp = User::where('name',$slug)->get();
            endwhile;
        endif;
        
        return $slug;
    }

    /*
    public function registerDoctor(Request $req){

        $this->validate($req, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required|string',
        ]);

        $pass_crypt = bcrypt($req->password);

        $user = User::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => $pass_crypt,
            'type' => 'doctor',
            'gender' => $req->gender,
            'status' => '1',
            'last_online' => new \DateTime
        ]);

        if($user):
            return redirect('/login');
        endif;

        return redirect()->back()->withInput($req->only(['email','name']));
    }
    */
}