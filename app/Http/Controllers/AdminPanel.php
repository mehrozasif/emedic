<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Post;
use App\Doctor;
use App\Patient;
use App\Option;
use App\Report;
use App\Activity;
use App\Schedule;
use App\Education;
use App\Appointment;
use App\Contact;

class AdminPanel extends Controller
{
    public function home()
    {
    	$admin = Auth::user();

    	if($admin->hasRole('doctor')):
            return $this->doctorHome();
        elseif($admin->hasRole('patient')):
            return redirect('/');
        elseif(self::isAdmin()):
            return $this->AdminHome();
    	endif;

        return redirect('/');
    }

    public function AdminHome(){

        $admin = Auth::user();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctorscount = count(Doctor::all());

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());



        $emedic_activities = Activity::limit(10)->get();



        return View('dashboard.admin.dashboard',compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','emedic_activities'));
    }

    public function pageProfile(){

        $admin = Auth::user();

        $user = $admin;
        $totalposts=[];
        $totalvisitors=[]; $doctorscount=[]; $patientscount=[]; $reportscount=[]; $appointments_requested_count=[]; $appointments_count=[]; $emedic_activities=[];

        return View('dashboard.admin.profile',compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','emedic_activities','user'));
    }


    public function pageAdminAppRequest(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctorscount = count(Doctor::all());

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $appointments_requested = Appointment::where(['approved'=>'0'])->get();

        $appointments_requested_count = count($appointments_requested);

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $apps_req = [];

        foreach ($appointments_requested as $key => $appointment_requested) {

            $apps_req[$key]['app'] = $appointment_requested;
            $apps_req[$key]['patient'] = User::where('id',$appointment_requested->patient_id)->get()->first();
            $apps_req[$key]['doctor'] = User::where('id',$appointment_requested->doc_id)->get()->first();

            $apps_req[$key]['schedule_time'] = str_replace('&', ' ', $appointment_requested->date);
        }

        $emedic_activities = [];
        return View('dashboard.admin.app_requests',compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','emedic_activities','apps_req'));
    }

    public function pageAdminAppBooked(){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctorscount = count(Doctor::all());

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());


        $appointments_booked = Appointment::where(['approved'=>'1'])->get();

        $appointments_count = count($appointments_booked);


        $apps_book = [];

        foreach ($appointments_booked as $key => $appointment_booked) {

            $apps_book[$key]['app'] = $appointment_booked;
            $apps_book[$key]['patient'] = User::where('id',$appointment_booked->patient_id)->get()->first();
            $apps_book[$key]['doctor'] = User::where('id',$appointment_booked->doc_id)->get()->first();

            $olddate = (strtotime(explode('&',$appointment_booked->date)[0])) < time();

            $oldtime = (strtotime(explode('&',$appointment_booked->date)[1])) < time();

            $apps_book[$key]['missed'] = FALSE;
            $apps_book[$key]['taken'] = FALSE;

            if($apps_book[$key]['app']->taken == '1'){
                $apps_book[$key]['taken'] = TRUE;
            }
            elseif( $olddate && $oldtime  ){
                $apps_book[$key]['missed'] = TRUE;
            }

            $apps_book[$key]['schedule_time'] = str_replace('&', ' ', $appointment_booked->date);
        }


        return View('dashboard.admin.app_booked', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','apps_book'));
    }

    public function pageAdminDoctors(){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $doctorlist = [];
        foreach ($doctors as $key => $doctor) {
            $doctorlist[$key]['user'] = User::where('id', $doctor->user_id)->get()->first();
            $doctorlist[$key]['doctor'] = $doctor;
        }

        return View('dashboard.admin.doctors', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','doctorlist'));
    }

    public function pageAdminAddDoctor(){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        return View('dashboard.admin.add_doctor', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count'));
    }
    public function pageAdminEditDoctor($name){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $user = User::where(['name'=>$name])->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $doctor = Doctor::where('user_id',$user->id)->get()->first();


        return View('dashboard.admin.edit_doctor', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','doctor','user'));

    }

    public function pageAdminAddDoctorEducation($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $doctor = User::where(['name'=>$name])->get();

        if(empty($doctor) || $doctor->isEmpty()):
            return redirect('/dashboard');
        endif;

        $doctor = $doctor->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        return View('dashboard.admin.add_doctor_education', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','doctor'));
    }

    public function pageEditDoctorEducation($id){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $education = Education::where(['id'=>$id])->get();

        if(empty($education) || $education->isEmpty()):
            return redirect('/dashboard');
        endif;

        $education = $education->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $doctor = User::where('id',$education->user_id)->get()->first();


        return View('dashboard.admin.edit_doctor_education', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','doctor','education'));
    }

    public function pageViewDoctorEducation($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $user = User::where('name',$name)->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $educations = Education::where('user_id', $user->id)->orderBy('id','DESC')->get();

        return View('dashboard.admin.doctor_educations', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','user','educations'));
    }



    public function pagePatients(){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $patients = Patient::all();

        $doctorscount = count(Doctor::all());

        $patientscount = count($patients);

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $patientlist = [];
        foreach ($patients as $key => $patient) {
            $patientlist[$key]['user'] = User::where('id', $patient->user_id)->get()->first();
            $patientlist[$key]['patient'] = $patient;
        }

        return View('dashboard.admin.patients', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','patientlist'));
    }

    public function pagePatientAdd(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        return View('dashboard.admin.add_patient', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count'));
    }

    public function pageEditPatient($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $user = User::where(['name'=>$name])->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $patientdata = Patient::where('user_id',$user->id)->get()->first();


        return View('dashboard.admin.edit_patient', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','user','patientdata'));
    }


    public function pageBlog(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        $posts = Post::all();
        $author = [];
        foreach ($posts as $key => $post) {
            $author[$key] = User::where('id',$post->author_id)->get()->first();
        }

        return View('dashboard.admin.blogs', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','posts','author'));

    }
    public function pageAddPost(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());

        return View('dashboard.admin.add_blog', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count'));

    }
    public function pageEditPost(int $id){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $post = Post::where(['id'=>$id])->get();

        if(empty($post) || $post->isEmpty()):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $post = $post->first();

        return View('dashboard.admin.edit_blog', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','post'));

    }

    public function pageReports(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $reports = Report::orderBy('id','DESC')->get();
        $reportsdata = [];
        foreach ($reports as $key => $report) {

            $reportsdata[$key] = [];
            $reportsdata[$key]['patient'] = User::where('id',$report->patient_id)->get()->first();
            $reportsdata[$key]['doctor'] = User::where('id',$report->doctor_id)->get()->first();
            $reportsdata[$key]['report'] = $report;

        }

        return View('dashboard.admin.reports', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','reportsdata'));
    }

    public function contacts(){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;


        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $contacts = Contact::orderBy('id','DESC')->get();

        return View('dashboard.admin.contacts', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','contacts'));
    }
    public function pageDoctorReports($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $user = User::where('name',$name)->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $reports = Report::where('doctor_id', $user->id)->orderBy('id','DESC')->get();
        $reportsdata = [];
        foreach ($reports as $key => $report) {

            $reportsdata[$key] = [];
            $reportsdata[$key]['patient'] = User::where('id',$report->patient_id)->get()->first();
            $reportsdata[$key]['doctor'] = User::where('id',$report->doctor_id)->get()->first();
            $reportsdata[$key]['report'] = $report;

        }

        return View('dashboard.admin.doctor_reports', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','reportsdata','user'));
    }
    public function pagePatientReports($name){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/dashboard');
        endif;

        $user = User::where('name',$name)->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $totalposts = count(Post::all());

        $totalvisitors = (Option::where('name','visitor_count')->first());

        if(!$totalvisitors):
            $totalvisitors = '0';
        else:
            $totalvisitors = $totalvisitors->value;
        endif;

        $doctors = Doctor::all();

        $doctorscount = count($doctors);

        $patientscount = count(Patient::all());

        $reportscount = count(Report::all());

        $appointments_requested_count = count(Appointment::where(['approved'=>'0'])->get());

        $appointments_count = count(Appointment::where(['approved'=>'1'])->get());


        $reports = Report::where('patient_id', $user->id)->orderBy('id','DESC')->get();
        $reportsdata = [];
        foreach ($reports as $key => $report) {

            $reportsdata[$key] = [];
            $reportsdata[$key]['patient'] = User::where('id',$report->patient_id)->get()->first();
            $reportsdata[$key]['doctor'] = User::where('id',$report->doctor_id)->get()->first();
            $reportsdata[$key]['report'] = $report;

        }

        return View('dashboard.admin.patient_reports', compact('admin','totalposts','totalvisitors','doctorscount','patientscount','reportscount','appointments_requested_count','appointments_count','reportsdata','user'));
    }


    public function pageReportView($report_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') && !self::isAdmin()):
            return redirect('/dashboard');
        endif;

        $report = Report::where('id',$report_id)->get();

        if(empty($report) || $report->isEmpty()):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');

        return view('dashboard.report',compact('data'));
    }

    public function pageReportDownload($report_id){

        $doctor = Auth::user();
        if( !$doctor->hasRole('doctor') && !self::isAdmin()):
            return redirect('/dashboard');
        endif;

        $report = Report::where('id',$report_id)->get();

        if(empty($report) || $report->isEmpty()):
            return redirect('/dashboard');
        endif;

        $report = $report->first();

        $patient_user_data = User::where('id',$report->patient_id)->get()->first();
        $patient_data = Patient::where('user_id',$report->patient_id)->get()->first();
        $doctor_user_data = User::where('id',$report->doctor_id)->get()->first();

        $data = [];

        $data['patient_name'] = $patient_user_data->display_name;
        $data['patient_email'] = $patient_user_data->email;
        $data['patient_gender'] = $patient_user_data->gender;
        $data['patient_age'] = $patient_user_data->getAge();

        $data['doctor_name'] = $doctor_user_data->display_name;

        $data['report_department'] = $report->department;
        $data['report_initial_dignosis'] = $report->initial_dignosis;
        $data['report_final_dignosis'] = $report->final_dignosis;
        $data['report_disease'] = $report->disease;
        $data['report_tdisease'] = $report->tdisease;
        $data['report_examination'] = $report->examination;
        $data['report_treatment'] = $report->treatment;
        $data['report_created'] = $report->created_at->format('dS F, Y h:i a');
        $data['report_updated'] = $report->updated_at->format('dS F, Y h:i a');


        $html = view('dashboard.report', compact('data') );

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();


        $dom->getElementsByTagName('body')->item(0)->setAttribute('style', 'background-color:#fff;margin: 0;');

        $html = $dom->saveHTML();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML( $html );
        return $pdf->stream();
    }


    public function pagePostSubmit(Request $req){

        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/');
        endif;


        $this->validate($req, [
            'post_title' => 'required|string',
            'post_content' => 'required',
        ]);

        $imagepath = '';

        if(!empty($req->post_image)){
            $imageName = time().'.'.$req->post_image->getClientOriginalExtension();
            $req->post_image->move(public_path('/postimages'), $imageName);

            $imagepath = '/postimages/'.$imageName;
        }

        $report = Post::create([
            'author_id' => $admin->id,
            'title' => $req->post_title,
            'slug' => DoctorPanel::get_post_slug($req->post_title),
            'body' => $req->post_content,
            'featured_image' => $imagepath,
        ]);

        if($report):
            return redirect()->back()->with('succ_msg', 'Blog Post Created!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');

    }
    public function pageEditPostSubmit($id,Request $req){
        $admin = Auth::user();

        if(!$admin->hasRole('admin')):
            return redirect('/');
        endif;

        $post = Post::where(['id'=>$id])->get();

        if(empty($post) || $post->isEmpty()):
            return redirect('/dashboard');
        endif;


        $this->validate($req, [
            'post_title' => 'required|string',
            'post_content' => 'required',
        ]);

        $post = $post->first();

        $imagepath = $post->featured_image;


        if(!empty($req->post_image)){

            if(!empty($post->featured_image) && File::exists(public_path($post->featured_image)) ):
                File::delete(public_path($post->featured_image));
            endif;

            $imageName = time().'.'.$req->post_image->getClientOriginalExtension();
            $req->post_image->move(public_path('/postimages'), $imageName);
            $imagepath = '/postimages/'.$imageName;
        }

        if($req->delete_image == 'yes'){

            if(File::exists(public_path($post->featured_image))):
                File::delete(public_path($post->featured_image));
            endif;

            $imagepath = '';
        }

        $post->title = $req->post_title;
        $post->body = $req->post_content;
        $post->featured_image = $imagepath;

        $success = $post->save();

        if($success):
            return redirect()->back()->with('succ_msg', 'Blog Post Editted!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');
    }
    public function pageAdminAddDoctorSubmit(Request $req){

        $this->validate($req, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string',
            'dob' => 'required|string',
            'phoneno' => 'required|string',
            'gender' => 'required|string',
            //Doctor Info
            'grade' => 'required|int',
            'department' => 'required|string',
            'bio' => 'required|string',
            'certifications' => 'required|string',
            'interests' => 'required|string',
            'awards' => 'required|string',
            'specialization' => 'required|string',
        ]);

        $pass_crypt = bcrypt($req->password);


        $imagepath = NULL;

        if(!empty($req->profile_image)){
            $imageName = time().'.'.$req->profile_image->getClientOriginalExtension();
            $req->profile_image->move(public_path('/profileimages'), $imageName);

            $imagepath = '/profileimages/'.$imageName;
        }

        $user = User::create([
            'name' => CustomAuthController::get_user_name($req->display_name),
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'display_name' => $req->display_name,
            'email' => $req->email,
            'password' => $pass_crypt,
            'type' => 'doctor',
            'address' => $req->address,
            'gender' => $req->gender,
            'dob' => $req->dob,
            'image'=> $imagepath,
            'phoneno' => $req->phoneno,
            'status' => '0',
            'last_online' => NULL
        ]);


        $doctor = Doctor::create([
            'user_id'=>$user->id,
            'grade' => $req->grade,
            'department' => $req->department,
            'bio' => $req->bio,
            'certifications' => $req->certifications,
            'interests' => $req->interests,
            'awards' => $req->awards,
            'specialization' => $req->specialization,
        ]);

        if($user && $doctor):
            return redirect()->back()->with('succ_msg', 'Doctor Added!!!')->with('doctorname',$user->name);
        endif;


        return redirect()->back()->withInput($req->only(['email','first_name','last_name','display_name','address','gender','dob','phoneno','grade','department','bio','certifications','interests','awards','specialization']));
    }

    public function pageEditDoctorSubmit($name,Request $req){

        $this->validate($req, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'email' => 'required|string',
            'address' => 'required|string',
            'dob' => 'required|string',
            'phoneno' => 'required|string',
            'gender' => 'required|string',
            //Doctor Info
            'grade' => 'required|int',
            'department' => 'required|string',
            'bio' => 'required|string',
            'certifications' => 'required|string',
            'interests' => 'required|string',
            'awards' => 'required|string',
            'specialization' => 'required|string',
        ]);

        $user = User::where(['name'=>$name])->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $doctor = Doctor::where('user_id',$user->id)->get()->first();

        $user->first_name = $req->first_name;
        $user->last_name = $req->last_name;
        $user->display_name = $req->display_name;
        $user->email = $req->email;
        $user->address = $req->address;
        $user->dob = $req->dob;
        $user->phoneno = $req->phoneno;
        $user->gender = $req->gender;


        $doctor->awards = $req->awards;
        $doctor->specialization = $req->specialization;
        $doctor->department = $req->department;
        $doctor->bio = $req->bio;
        $doctor->certifications = $req->certifications;
        $doctor->interests = $req->interests;
        $doctor->grade = $req->grade;


        $imagepath = $user->image;

        if($req->delete_image == 'yes'){

            if(File::exists(public_path($user->image))):
                File::delete(public_path($user->image));
            endif;
            $imagepath = NULL;
        }

        if( !empty($req->profile_image) ){

            $imageName = time().'.'.$req->profile_image->getClientOriginalExtension();
            $req->profile_image->move(public_path('/profileimages'), $imageName);

            $imagepath = '/profileimages/'.$imageName;

        }

        $user->image = $imagepath;

        $user->save();

        $doctor->save();

        return redirect()->back()->with('succ_msg', 'Doctor\'s Profile Editted!!!');


    }

    public function addDoctorEducationSubmit($name,Request $req){

        $this->validate($req, [
            'title' => 'required|string|max:255',
            'degree' => 'required|string|max:255',
            'institute' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'year' => 'required|string|',
        ]);

        $user = User::where(['name'=>$name])->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $success = Education::create([
            'user_id'=>$user->id,
            'title' => $req->title,
            'degree' => $req->degree,
            'institute' => $req->institute,
            'location' => $req->location,
            'year' => $req->year
        ]);

        if($success){
            return redirect()->back()->with('succ_msg', 'Doctor Education Added!!!');
        }

        return redirect()->back()->withInput($req->all());

    }

    public function editDoctorEducationSubmit($id,Request $req){
        $this->validate($req, [
            'title' => 'required|string|max:255',
            'degree' => 'required|string|max:255',
            'institute' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'year' => 'required|string|',
        ]);

        $education = Education::where(['id'=>$id])->get();

        if(empty($education) || $education->isEmpty()):
            return redirect('/dashboard');
        endif;

        $education = $education->first();

        $education->title = $req->title;
        $education->degree = $req->degree;
        $education->institute = $req->institute;
        $education->location = $req->location;
        $education->year = $req->year;

        $success = $education->save();

        if($success){
            return redirect()->back()->with('succ_msg', 'Doctor Education Editted!!!');
        }

        return redirect()->back()->withInput($req->all());
    }

    public function RemoveDoctorEducation($id){

        $education = Education::where(['id'=>$id])->get();

        if(empty($education) || $education->isEmpty()):
            return redirect('/dashboard');
        endif;

        $education = $education->first();

        $success = $education->delete();

        if($success){
            return redirect()->back();
        }

        return redirect()->back();

    }

    public function PatientAddSubmit(Request $req){
        $this->validate($req, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string',
            'dob' => 'required|string',
            'phoneno' => 'required|string',
            'gender' => 'required|string',
            //Patient Info
            'father_name' => 'required|string',
            'blood_group' => 'required|string',
            'area' => 'required|string',
            'history' => 'required|string',
        ]);

        $pass_crypt = Hash::make($req->password);

        $user = User::create([
            'name' => CustomAuthController::get_user_name($req->display_name),
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'display_name' => $req->display_name,
            'email' => $req->email,
            'password' => $pass_crypt,
            'type' => 'patient',
            'address' => $req->address,
            'gender' => $req->gender,
            'dob' => $req->dob,
            'phoneno' => $req->phoneno,
            'status' => '0',
            'last_online' => new \DateTime
        ]);


        if(!empty($req->profile_image)){

            $imageName = time().'.'.$req->profile_image->getClientOriginalExtension();
            $req->profile_image->move(public_path('/profileimages'), $imageName);

            $imagepath = '/profileimages/'.$imageName;

            $user->image = $imagepath;

            $success = $user->save();

        }

        $symtoms = '';
        $allergies = '';

        if( !empty($req->symtoms) ):
            $symtoms = $req->symtoms;
        endif;

        if( !empty($req->allergies) ):
            $allergies = $req->allergies;
        endif;

        if($req->history = 'yes'):
            $history = 1;
        else:
            $history = 0;
        endif;

        $patient = Patient::create([
            'user_id'=>$user->id,
            'father_name' => $req->father_name,
            'blood_group' => $req->blood_group,
            'symtoms' => $symtoms,
            'allergies' => $allergies,
            'area' => $req->area,
            'history' => $history,
        ]);

        if($user && $patient):
            return redirect('dashboard/admin/patients');
        endif;

        return redirect()->back()->withInput($req->all());
    }

    public function pageEditPatientSubmit($name,Request $req){
        $this->validate($req, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'display_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'address' => 'required|string',
            'dob' => 'required|string',
            'phoneno' => 'required|string',
            'gender' => 'required|string',
            //Patient Info
            'father_name' => 'required|string',
            'blood_group' => 'required|string',
            'area' => 'required|string',
            'history' => 'required|string',
        ]);

        $user = User::where(['name'=>$name])->get();

        if(empty($user) || $user->isEmpty()):
            return redirect('/dashboard');
        endif;

        $user = $user->first();

        $patient = Patient::where('user_id',$user->id)->get()->first();

        $user->first_name = $req->first_name;
        $user->last_name = $req->last_name;
        $user->display_name = $req->display_name;
        $user->email = $req->email;
        $user->address = $req->address;
        $user->dob = $req->dob;
        $user->phoneno = $req->phoneno;
        $user->gender = $req->gender;

        $symtoms = '';
        $allergies = '';

        if( !empty($req->symtoms) ):
            $symtoms = $req->symtoms;
        endif;

        if( !empty($req->allergies) ):
            $allergies = $req->allergies;
        endif;


        $patient->father_name = $req->father_name;
        $patient->blood_group = $req->blood_group;
        $patient->symtoms = $symtoms;
        $patient->allergies = $allergies;
        $patient->area = $req->area;

        $history = 0;
        if($req->history = 'yes'):
            $history = 1;
        endif;
        $patient->history = $history;

        $imagepath = $user->image;

        if($req->delete_image == 'yes'){

            if(File::exists(public_path($user->image))):
                File::delete(public_path($user->image));
            endif;
            $imagepath = NULL;
        }

        if( !empty($req->profile_image) ){

            $imageName = time().'.'.$req->profile_image->getClientOriginalExtension();
            $req->profile_image->move(public_path('/profileimages'), $imageName);

            $imagepath = '/profileimages/'.$imageName;

        }

        $user->image = $imagepath;

        $user->save();

        $patient->save();

        return redirect(route('dashboard.admin.patients'));


    }

    public function ReportComplete(Request $req){

        $validate = Validator::make($req->all(),[
            'report_id'=> 'required',
        ]);

        if($validate->passes()){


            $report = Report::where('id',$req->report_id)->get();

            if(!$report):
                return response()->json([
                    'success' => '0',
                    'msg' => 'Nope',
                ]);
            endif;

            $report = $report->first();

            $report->finished = '1';

            $doctor = Doctor::where('user_id',$report->doctor_id)->get()->first();

            $doctor->addTreatedPatient($report->patient_id);
            $report->save();

            if($report):
                return response()->json([
                    'success' => '1',
                    'msg' => 'Ok',
                ]);
            endif;
        }
        return response()->json([
            'success' => '0',
            'msg' => 'Nope',
        ]);


    }

    public static function isAdmin($admin = 'Auth::user'){

        if(is_callable($admin)):
            $admin = $admin();
        elseif(is_int($admin)):
            $admin = User::find($admin);
        else:
            $admin = Auth::user();
        endif;

        if(empty($admin) || !$admin){
            return FALSE;
        }


        if($admin->type == 'super admin' || $admin->type == 'admin'):
            return TRUE;
        endif;

        return FALSE;
    }

    public function pagePatientStatus(Request $request , $id ){
        $status = $request->post('status');
        $patient = Patient::find($id);
        $patient->status = $status;
        $change = $patient->save();
        if($change):
            return redirect()->back()->with('succ_msg', 'Status Changed!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');
    }

    public function pagePatientReport(Request $request  ){
        $status = $request->post('status');
        $amount = $request->post('amount');
        $id = $request->post('id');

        $report = Report::find($id);
        $report->paid_status = $status;
        $report->amount = $amount;
        $change = $report->save();
        if($change):
            return redirect()->back()->with('succ_msg', 'Report Accounts Updated!!!');
        endif;

        return redirect()->back()->with('err_msg', 'Unfortunately, an error occurred');
    }

}
