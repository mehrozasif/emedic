<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Cache;
use Carbon\Carbon;
use App\User;
use App\Patient;

class UserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()){
            if(Auth::user()->type == 'patient'){
                $patient = Patient::where('user_id',Auth::user()->id)->first();
                if($patient->status == 'active'){
                    redirect('/');
                }else{
                    Auth::logout();
                    return redirect('/login')
                        ->with('message', 'Your account is not active ,please contact admin');
                }
            }
        }


        if (Auth::check() && Auth::user()->last_online < Carbon::now()->subMinutes(1)->format('Y-m-d H:i:s')) {
            $user = Auth::user();
            $user->last_online = new \DateTime;
            $user->save();
        }
        elseif(!Cache::has('check-user-status')){
            $expiresAt = Carbon::now()->addMinutes(1);
            Cache::put('check-user-status', true, $expiresAt);
            $users = User::all();
/*
            foreach ($users as $user) {
                dd(($user->last_online > (new \DateTime('-1 minutes'))->format('Y-m-d H:i:s')),$user::check());
                $online = (($user->last_online > (new \DateTime('-1 minutes'))->format('Y-m-d H:i:s')) &&     $user::check()) ? true : false;
                if($online){
                    $user->status = '1';
                }
                else{
                    $user->status = '0';
                }
            }*/
        }

        
        return $next($request);
    }
}
