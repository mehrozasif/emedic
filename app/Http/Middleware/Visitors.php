<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Option;

class Visitors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return $next($request);
        }

        if(session('visited')):
            return $next($request);
        endif;

        $count = Option::where('name','visitor_count')->first();

        if(!$count):
            Option::create([
                'name' => 'visitor_count',
                'value' => '0'
            ]);
            $count = Option::where('name','visitor_count')->first();
        endif;

        $count =  ''.($count->value + 1);
        Option::where('name','visitor_count')->first()->update(['value'=>$count]);

        session(['visited' => 'true']);

        return $next($request);
    }
}
