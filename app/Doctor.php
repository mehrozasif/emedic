<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'grade','department','bio','certifications','interests','awards','specialization','patients','patients_treated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function addPatient(int $id){
        $patients = explode(',',str_replace(' ', '', $this->patients));

        foreach($patients as $patient):
            if(((int) $patient ) == (int) $id):
                return TRUE;
            endif;
        endforeach;

        $patients[] = $id;

        $this->patients = implode(',',array_filter($patients));

        return $this->save();

    }
    public function hasPatient(int $id){
        $patients = explode(',',str_replace(' ', '', $this->patients));

        foreach($patients as $patient):
            if(((int) $patient ) == (int) $id):
                return TRUE;
            endif;
        endforeach;

        return FALSE;
    }

    public function addTreatedPatient(int $id){
        $patients = explode(',',str_replace(' ', '', $this->patients_treated));

        foreach($patients as $patient):
            if(((int) $patient ) == (int) $id):
                return TRUE;
            endif;
        endforeach;

        $patients[] = $id;

        $this->patients_treated = implode(',',array_filter($patients));

        return $this->save();

    }

    public function hasTreatedPatient(int $id){
        $patients = explode(',',str_replace(' ', '', $this->patients_treated));

        foreach($patients as $patient):
            if(((int) $patient ) == (int) $id):
                return TRUE;
            endif;
        endforeach;

        return FALSE;
    }
}
