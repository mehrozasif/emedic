<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('/login','API@login')->name('api.login');
Route::post('/register_patient','API@registerPatient')->name('api.regiter_patient');
Route::post('/register_doctor','API@registerDoctor')->name('api.regiter_doctor');