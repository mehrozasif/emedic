<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Pages@home')->name('main');

Route::get('/home','Pages@home')->name('home');

Route::get('/search-doctor','DoctorSearchController@searchDoctor')->name('find_doctor_form');

Route::post('/search_doctor','DoctorSearchController@ajaxSearchDoctor');

Route::get('/health-library','Pages@healthLibrary')->name('health_library');

Route::get('/search','DoctorSearchController@searchDoctor')->name('search_form');

Route::get('/blog','Pages@blog')->name('blog');


Route::get('/post',function(){
	return redirect('/blog');
})->name('blog');


Route::get('/post/{slug}','Pages@viewPost')->name('blog.view.post');


Route::get('/contact','Pages@contact')->name('contact');
Route::post('/contact','Forms@contact')->name('contact.submit');


Route::get('/services','Pages@services')->name('services');

Route::get('/patients-visitors','Pages@patientsVisitors')->name('patients_visitors');

Route::get('/reports','Pages@reports')->name('reports');

Route::get('/alerts','Pages@alerts')->name('alerts');


Route::post('blog-search/character','forms@ajaxSearchPostChar')->name('search.post.char');


Route::get('/dashboard',function(){
	if(!Auth::guard()->check()):
		return redirect('/');
	endif;
	$user = Auth::user();

	if($user->hasRole('doctor')):
		return redirect('/dashboard/doctor');
	endif;

	if($user->hasRole('admin')):
		return redirect('/dashboard/admin');
	endif;

	return redirect('/');

})->name('dashboard.home');


//FOR logged in user ( can be different roles)


Route::group(['middleware' => 'auth'],function(){


	Route::get('/profile','Pages@profile')->name('profile');
	Route::get('/profile/appointments','Pages@profileAppointments')->name('profile.appointments');
	Route::get('/profile/notifications','Pages@profileNotifications')->name('profile.notifications');
	Route::get('/profile/doctors','Pages@profileDoctors')->name('profile.doctors');
	Route::get('/profile/reports','Pages@profileReports')->name('profile.messages');


	Route::get('/report/{report_id}','pages@pageReportView')->where('report_id','[0-9]+');

	Route::get('/report/{report_id}/generate','Forms@pageReportDownload')->where('report_id','[0-9]+');



	Route::get('/request_appointment/{doctor_id}','Pages@doctorAppointment')->where('id','[0-9]+');
	Route::post('/request_appointment','Forms@doctorAppointmentSubmit')->name('submit_appointment');


	Route::post('profile-edit','forms@ajaxChangeProfileData');



	Route::post('profile-dp-edit','forms@ajaxChangeProfileDP');
	Route::post('profile-dp-remove','forms@ajaxRemoveProfileDP');

	Route::get('/profile/messages','ChatController@PatientMessages')->name('profile.messages');
	Route::get('/profile/messages/{name}','ChatController@PatientChatUser')->name('profile.messages.chat');

	Route::post('/chat/message/send','ChatController@messageSend')->name('chat.messages.send');
	Route::post('/chat/message/seen','ChatController@messageSeen')->name('chat.messages.seen');



	//Rating
	Route::post('/rate-doctor','Forms@rateDoctor')->name('rate.doctor');


	Route::get('/profile/{name}','Pages@profileByName')->name('profile.patient');

});

Route::group(['prefix' => 'dashboard/admin', 'middleware' => ['auth']],function(){

	Route::get('/','AdminPanel@home')->name('dashboard.admin.home');

	Route::get('/profile','AdminPanel@pageProfile')->name('dashboard.admin.profile');



});

Route::group(['prefix' => 'dashboard/doctor', 'middleware' => ['auth']],function(){

	Route::get('/','DoctorPanel@home')->name('dashboard.doctor.home');

	Route::get('/profile','DoctorPanel@pageProfile')->name('dashboard.doctor.profile');



	Route::get('/schedules','DoctorPanel@pageSchedules')->name('dashboard.doctor.schedules');
	Route::post('/schedule/add','DoctorPanel@formScheduleAdd')->name('dashboard.doctor.schedule.add');
	Route::post('/schedule/delete','DoctorPanel@formScheduleDelete')->name('dashboard.doctor.schedule.delete');

	Route::get('/appointments/requests','DoctorPanel@pageAppRequest')->name('dashboard.doctor.app.requested');
	Route::get('/appointments/booked','DoctorPanel@pageAppBooked')->name('dashboard.doctor.app.booked');
	Route::post('/appointment/taken','DoctorPanel@pageAppTaken')->name('dashboard.doctor.app.taken');
	Route::post('/appointment/approve','DoctorPanel@appointmentApprove')->name('dashboard.doctor.app.approve');
	Route::post('/appointment/decline','DoctorPanel@appointmentDecline')->name('dashboard.doctor.app.decline');

	Route::get('/blog','DoctorPanel@pageBlog')->name('dashboard.doctor.blogs');
	Route::get('/blog/create','DoctorPanel@pageAddPost')->name('dashboard.doctor.new.blog');
	Route::post('/blog/submit','DoctorPanel@pagePostSubmit')->name('dashboard.doctor.post.submit');
	Route::get('/blog/edit/{id}','DoctorPanel@pageEditPost')->name('dashboard.doctor.edit.blog');
	Route::post('/blog/edit/{id}','DoctorPanel@pageEditPostSubmit')->name('dashboard.doctor.post.edit.submit');
	Route::post('/blog/delete','DoctorPanel@blogPostDelete');


	Route::get('/patients','DoctorPanel@pagePatients')->name('dashboard.doctor.patients');
	Route::get('/patient/{id}','DoctorPanel@pagePatient')->where('id','[0-9]+')->name('dashboard.doctor.patient');


	Route::get('/reports','DoctorPanel@pageReports')->name('dashboard.doctor.reports');//ALL Reports in E-MEDIC
	Route::get('/report/patient_{id}','DoctorPanel@pageReportPatient')->name('dashboard.doctor.patient.report');
	Route::get('/report/{patient_id}/create','DoctorPanel@pageReportCreate')
		->where('id','[0-9]+')->name('dashboard.doctor.report.create');
	Route::post('/report/{patient_id}/create','DoctorPanel@pageReportFormSubmit')
		->where('id','[0-9]+')->name('dashboard.doctor.report.submit');
	Route::get('/report/{report_id}/edit','DoctorPanel@pageReportEdit')
		->where('id','[0-9]+')->name('dashboard.doctor.report.edit');
	Route::post('/report/{report_id}/edit','DoctorPanel@pageReportEditFormSubmit')
		->where('id','[0-9]+')->name('dashboard.doctor.report.edit.submit');
	Route::get('/report/{report_id}','DoctorPanel@pageReportView')
		->where('report_id','[0-9]+')->name('dashboard.doctor.report');
	Route::get('/report/{report_id}/generate','DoctorPanel@pageReportDownload')
		->where('report_id','[0-9]+')->name('dashboard.doctor.report');
	Route::post('/report/complete','DoctorPanel@ReportComplete');



	Route::get('/messages','ChatController@messages')->name('dashboard.doctor.messages');
	Route::get('/messages/{name}','ChatController@messageChatUser')->name('dashboard.doctor.messages.chat');


});



Route::group(['prefix' => 'dashboard/admin', 'middleware' => ['auth']],function(){

	Route::get('/','AdminPanel@home')->name('dashboard.admin.home');
	Route::get('/profile','AdminPanel@pageProfile')->name('dashboard.admin.profile');


	Route::get('/appointments/requests','AdminPanel@pageAdminAppRequest')->name('dashboard.admin.app.requested');
	Route::get('/appointments/booked','AdminPanel@pageAdminAppBooked')->name('dashboard.admin.app.booked');


	Route::get('/doctors','AdminPanel@pageAdminDoctors')->name('dashboard.admin.doctors');
	Route::get('/doctor/add','AdminPanel@pageAdminAddDoctor')->name('dashboard.admin.add.doctor');
	Route::get('/doctor/{name}/education/add','AdminPanel@pageAdminAddDoctorEducation')->name('dashboard.admin.doctor.add.education');
	Route::get('/doctor/{name}/educations','AdminPanel@pageViewDoctorEducation')->name('dashboard.admin.doctor.educations');
	Route::get('/doctor/education/{id}/edit','AdminPanel@pageEditDoctorEducation')->name('dashboard.admin.doctor.edit.education');
	Route::post('/doctor/education/{id}/edit','AdminPanel@editDoctorEducationSubmit')->name('dashboard.admin.doctor.edit.education.submit');
	Route::get('/doctor/education/{id}/remove','AdminPanel@RemoveDoctorEducation')->name('dashboard.admin.doctor.remove.education');
	Route::post('/doctor/{name}/education/add','AdminPanel@addDoctorEducationSubmit')->name('dashboard.admin.doctor.add.education.submit');



	Route::post('/doctor/add','AdminPanel@pageAdminAddDoctorSubmit')
		->name('dashboard.admin.add.doctor.submit'); //TODO
	Route::get('/doctor/{name}/edit','AdminPanel@pageAdminEditDoctor')->name('dashboard.admin.edit.doctor');
	Route::post('/doctor/{name}/edit','AdminPanel@pageEditDoctorSubmit')->name('dashboard.admin.edit.doctor.submit'); //TODO
	Route::post('/doctor/delete','AdminPanel@pageAdminDeleteDoctor')
		->name('dashboard.admin.delete.doctor.submit'); //TODO


	Route::get('/blog','AdminPanel@pageBlog')->name('dashboard.admin.blogs');
	Route::get('/blog/create','AdminPanel@pageAddPost')->name('dashboard.admin.new.blog');
	Route::post('/blog/submit','AdminPanel@pagePostSubmit')->name('dashboard.admin.post.submit');
	Route::get('/blog/edit/{id}','AdminPanel@pageEditPost')->name('dashboard.admin.edit.blog');
	Route::post('/blog/edit/{id}','AdminPanel@pageEditPostSubmit')->name('dashboard.admin.post.edit.submit');
	//Route::post('/blog/delete','AdminPanel@blogPostDelete'); //Doctor's route is used instead of this


	Route::get('/patients','AdminPanel@pagePatients')->name('dashboard.admin.patients');
	Route::get('/patient/add','AdminPanel@pagePatientAdd')->name('dashboard.admin.add.patient');
	Route::post('/patient/add','AdminPanel@PatientAddSubmit')->name('dashboard.admin.add.patient.submit');
	Route::get('/patient/{name}/edit','AdminPanel@pageEditPatient')->name('dashboard.admin.patient.edit');
	Route::post('/patient/{name}/edit','AdminPanel@pageEditPatientSubmit')->name('dashboard.admin.patient.edit.submit');
	Route::post('/patient/{id}/delete','AdminPanel@pageEditPatientSubmit')->name('dashboard.admin.patient.edit.submit');


	Route::get('/reports/','AdminPanel@pageReports')->name('dashboard.admin.reports');//ALL Reports in E-MEDIC
	Route::get('/report/{id}','AdminPanel@pageReportView')->name('dashboard.admin.report'); //vIEW Report By ID
	Route::get('/report/{id}/generate','AdminPanel@pageReportDownload')->name('dashboard.admin.report.download'); //Download Report By ID
	Route::get('/doctor/{name}/reports','AdminPanel@pageDoctorReports')->name('dashboard.admin.doctor.reports'); //Specific Doctor Reports
	Route::get('/patient/{name}/reports','AdminPanel@pagePatientReports')->name('dashboard.admin.patient.reports'); //Specific Patient Reports

    Route::post('/patient/status/{id}','AdminPanel@pagePatientStatus')->name('dashboard.admin.patient.status'); //Specific Patient Reports
    Route::post('/patient/status/report/paid','AdminPanel@pagePatientReport')->name('dashboard.admin.patient.report'); //Specific Patient Reports



	Route::get('/contacts','AdminPanel@contacts')->name('dashboard.admin.contacts');

	Route::get('/messages','ChatController@messages')->name('dashboard.admin.messages');
	Route::get('/messages/{name}','ChatController@messageChatUser')->name('dashboard.admin.messages.chat');

});



Route::get('/user/',function(){
	return redirect('/');
});
Route::get('/doctor/',function(){
	return redirect('/');
});
Route::get('/patient/',function(){
	return redirect('/');
});

Route::get('/doctor/{id}','Pages@doctorProfile')->where('id','[0-9]+');


Route::get('/doctor/{name}','Pages@doctorProfileByName')->name('profile.doctor');








//Forms

//Route::get('/register_doctor','Pages@registerDoctor')->name('register.doctor.form');
Route::get('/register_patient','CustomAuthController@registerPatientView')->name('register.patient.form');


//FORMS Submision


Route::post('login_submit','CustomAuthController@login')->name('emedic.login');

//Route::post('register_submit_doctor','Forms@registerDoctor')->name('emedic.register.doctor');

Route::post('register_submit_patient','CustomAuthController@registerPatient')->name('emedic.register.patient');

Route::post('search','Forms@search')->name('emedic.search');





//Auth
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


// API
