@extends('dashboard.layout')

@section('title','All Patients - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_patients_view'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop

<style type="text/css">
.red-bg{
    background-color: #ad0d0d !important;
    color: white !important;
}
.bg-white{
    background-color: #fff !important;
    color: black !important;
}
</style>

@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Patients</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Patients</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    	@if(empty($patientlist))
                    	<div class="emedic-admin-activity">
                            <div class="emedic-admin-no-result">
                                <h3>No Patients Found</h3>
                            </div>
                        @else
                        <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Age</th>
                                        <th>email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($patientlist as $patient)
                                    <tr>
                                        <td>{{ $patient['user']->display_name }}</td>
                                        <td>{{ $patient['user']->getAge() }}</td>
                                        <td>{{ $patient['user']->email }}</td>
                                        <td>
                                            <form  action="{{ route('dashboard.admin.patient.status' ,$patient['patient']->id ) }}" id="formStatus" method="POST" enctype="multipart/form-data">
                                                <select class="form-control {{ $patient['patient']->status === "inactive" ? "red-bg" : "" }}" name="status" id="status" onchange='this.form.submit()'>
                                                    <option class="{{ $patient['patient']->status === "inactive" ? "bg-white" : "" }}" value="active" {{ $patient['patient']->status === "active" ? "selected" : "" }}>Active</option>
                                                    <option class="{{ $patient['patient']->status === "inactive" ? "bg-white" : "" }}" value="inactive" {{ $patient['patient']->status === "inactive" ? "selected" : "" }}>In-Active</option>
                                                    <option class="{{ $patient['patient']->status === "inactive" ? "bg-white" : "" }}" value="delete" {{ $patient['patient']->status === "delete" ? "selected" : "" }}>Delete</option>
                                                </select>
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                        <td>
                                            <a href="{{ url('/profile/'.$patient['user']->name) }}">View Profile</a> | <a href="{{ route('dashboard.admin.patient.edit',$patient['user']->name) }}">Edit Profile</a> 
                                            | <a href="{{ url('/dashboard/admin/messages/'.$patient['user']->name) }}" >Chat</a> | 
                                            <a href="{{ route('dashboard.admin.patient.reports', $patient['user']->name) }}" >Reports</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>

@stop