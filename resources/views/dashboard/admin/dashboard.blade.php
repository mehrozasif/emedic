@extends('dashboard.layout')

@section('title','E-Medic - Dashboard')


@section('sidebar')
	@include('dashboard.admin.partials.sidebar',['active_dasboard'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop

@section('breadcrumb')
    <li>Admin Dashboard</li>
    <li>Appointment</li>
    <li class="active">Appointment Requests</li>
@stop

@section('content')

	
    @include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>E-medic Activity</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity">
                    	@if($emedic_activities->isEmpty())
                    		<div class="emedic-admin-no-result">
                    			<h3>No Activity</h3>
                    		</div>
                    	@else
                    		<table class="emedic-activity-table w-100">
                    			<thead>
                                    <tr>
                                        <th width="20%">TYPE</th>
                                        <th width="20%">User Name</th>
                                        <th width="40%">Activity</th>
                                        <th width="20%">Time</th>
                                    </tr>
                                </thead>
                            	<tbody>

                    			@foreach($emedic_activities as $activity)
                    				<tr>
                    					<td>{{ $activity['type'] }}</td>
                    					<td>{{ $activity['user_name'] }}</td>
                    					<td>{{ $activity['activity_user'] }}</td>
                    					<td>{{ $activity['updated_at'] }}</td>
                    				</tr>
                    			@endforeach

                    		</table>

                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop