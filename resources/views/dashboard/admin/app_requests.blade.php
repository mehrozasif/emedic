@extends('dashboard.layout')

@section('title','E-Medic Dashboard - Appointment Requests')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_app_req'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Appointments</li>
    <li class="active">Appointment Requests</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Appointment Requests</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    @if(empty($apps_req))
                    <div class="emedic-admin-activity">
                    	<div class="emedic-admin-no-result">
                    			<h3>No Appointment Requests</h3>
                    	</div>
                    @else
                    <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                    			<thead>
                                    <tr>
                                        <th>Patient Name</th>
                                        <th>Doctor Name</th>
                                        <th>Reason</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            	<tbody>

                    			@foreach($apps_req as $app_req)
                                    <tr>
                                        <td>{{ $app_req['patient']->display_name }}</td>
                                        <td>{{ $app_req['doctor']->display_name }}</td>
                                        <td>{{ $app_req['app']->reason }}</td>
                                        <td>{{ $app_req['schedule_time'] }}</td>
                                        <td>
                                            <a href="{{ $app_req['app']->id }}" class="app-approve-btn" data-doctorid="{{ $app_req['doctor']->id }}">Approve</a> 
                                            | <a href="{{ $app_req['app']->id }}" class="app-decline-btn" data-doctorid="{{ $app_req['doctor']->id }}">Decline</a>
                                        </td>
                                    </tr>
                    			@endforeach
                                </tbody>
                    		</table>

                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop