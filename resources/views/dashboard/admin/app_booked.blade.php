@extends('dashboard.layout')

@section('title','E-Medic Dashboard - Appointment Requests')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_app_book'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script>
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script>
@stop



@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Appointments</li>
    <li class="active">Appointment Booked</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')

	<div class="row">
		<div class="col-md-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Appointment Booked</h3>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    @if(empty($apps_book))
                    <div class="emedic-admin-activity">
                    	<div class="emedic-admin-no-result">
                    		<h3>No Appointments Booked</h3>
                    	</div>
                    @else
                    <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Patient Name</th>
                                        <th>Doctor Name</th>
                                        <th>Reason</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($apps_book as $app_book)

                                
                                    <tr>
                                        <td>{{ $app_book['patient']->display_name }}</td>
                                        <td>{{ $app_book['doctor']->display_name }}</td>
                                        <td>{{ $app_book['app']->reason }}</td>
                                        <td>
                                            @if($app_book['missed'])
                                                {{ $app_book['schedule_time'] }} ( <span class="red">Not Taken</span> )<br />
                                            @elseif($app_book['taken'])
                                                {{ $app_book['schedule_time'] }} ( <span class="green">Taken</span> )
                                            @else
                                                {{ $app_book['schedule_time'] }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                    	@endif
                    </div>
               </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
	</div>
@stop
