@extends('dashboard.layout')

@section('title',$user->display_name.'Reports - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_reports'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
    <li>Admin Dashboard</li>
    <li>Patient</li>
    <li>{{ $user->display_name }}</li>
    <li class="active">Reports</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>{{ $user->display_name }} Reports</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity">
                    	@if(empty($reportsdata))
                    		<div class="emedic-admin-no-result">
                    			<h3>No Reports For Patient {{ $user->display_name }}</h3>
                    		</div>
                    	@else
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Created Time</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($reportsdata as $reportdata)
                                    <tr>
                                        <td><a href="{{ route('profile.doctor', $reportdata['doctor']->name) }}" >{{ $reportdata['doctor']->display_name }}</a></td>
                                        <td>{{ $reportdata['report']->created_at->format('jS F,Y h:i a') }}</td>
                                        <td>
                                            @if($reportdata['report']->finished == '0')
                                            ongoing
                                            @else
                                            Treated
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('dashboard.admin.report',$reportdata['report']->id) }}">View</a> | 
                                            <a href="{{ route('dashboard.admin.report.download',$reportdata['report']->id) }}">Download</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop