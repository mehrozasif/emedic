@extends('dashboard.layout')

@section('title','Doctors - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_doctors_view'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script>
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script>
@stop



@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Doctors</li>
    <li class="active">Doctors</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')

	<div class="row">
		<div class="col-md-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Doctors</h3>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    	@if(empty($doctorlist))
                        <div class="emedic-admin-activity">
                    		<div class="emedic-admin-no-result">
                    			<h3>No Doctors Found</h3>
                    		</div>
                    	@else
                        <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th>Grade</th>
                                        <th>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($doctorlist as $doctor)
                                    <tr>
                                        <td>{{ $doctor['user']->display_name }}</td>
                                        <td>{{ $doctor['doctor']->grade }}</td>
                                        <td>{{ $doctor['doctor']->department }}</td>
                                        <td>
                                            <a href="{{ url('/doctor/'.$doctor['user']->name) }}">View Profile</a> | <a href="{{ url('dashboard/admin/doctor/'.$doctor['user']->name.'/edit') }}">Edit Profile</a>
                                            | <a href="{{ url('/dashboard/admin/messages/'.$doctor['user']->name) }}" >Chat</a> |
                                            <a href="{{ route('dashboard.admin.doctor.reports', $doctor['user']->name) }}" >Reports</a> |
                                            <a href="{{ route('dashboard.admin.doctor.add.education', $doctor['user']->name) }}" >Add Education</a> |
                                            <a href="{{ route('dashboard.admin.doctor.educations', $doctor['user']->name) }}" >View Educations</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                    	@endif
                    </div>
               </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
	</div>
@stop
