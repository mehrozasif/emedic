@extends('dashboard.layout')

@section('title','Reports - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_reports'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop
<style type="text/css">
    .red-bg{
        background-color: #ad0d0d !important;
        color: white !important;
    }
    .center{
        text-align: center;
    }
</style>


@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li class="active">Reports</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Reports</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    	@if(empty($reportsdata))
                        <div class="emedic-admin-activity">
                    		<div class="emedic-admin-no-result">
                    			<h3>No Reports Created</h3>
                    		</div>
                    	@else
                        <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th class="center">Doctor Name</th>
                                        <th class="center">Patient Name</th>
                                        <th class="center">Created Time</th>
                                        <th class="center">Status</th>
                                        <th class="center">Payment Status</th>
                                        <th class="center">Amount</th>
                                        <th class="center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($reportsdata as $reportdata)
                                    <tr>
                                        <td class="center"><a href="{{ route('profile.doctor', $reportdata['doctor']->name) }}" >{{ $reportdata['doctor']->display_name }}</a></td>
                                        <td class="center"><a href="{{ route('profile.patient', $reportdata['patient']->name) }}" >{{ $reportdata['patient']->display_name }}</a></td>
                                        <td>{{ $reportdata['report']->created_at->format('jS F,Y h:i a') }}</td>
                                        <td class="center">
                                            @if($reportdata['report']->finished == '0')
                                            ongoing
                                            @else
                                            Treated
                                            @endif
                                        </td>
                                        @if($reportdata['report']->paid_status == 'paid')
                                        <td class="center">{{ucfirst($reportdata['report']->paid_status)}}</td>
                                        @else
                                        <td class="red-bg center">{{ucfirst($reportdata['report']->paid_status)}}</td>
                                        @endif
                                        <td class="center">{{"Rs.  ".$reportdata['report']->amount.".00"}}</td>
                                        <td class="center">
                                            <button type="button" data-id="{{$reportdata['report']->id}}" class="btn btn-sm btn-default myModal"
                                                    data-toggle="modal" data-target="#myModal">Payment</button> |
                                            <a href="{{ route('dashboard.admin.report',$reportdata['report']->id) }}">View</a> | 
                                            <a href="{{ route('dashboard.admin.report.download',$reportdata['report']->id) }}">Download</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>


        <!-- Modal -->
        <div id="myModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <form action="{{ route('dashboard.admin.patient.report' ) }}" id="formStatus" method="POST" enctype="multipart/form-data">

                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                           <div class="col-sm-6 col-md-6 col-lg-6">
                            <h5 class="modal-title">Confirmation</h5>
                           </div>
                            <div class="col-sm-6 col-md-6 col-lg-6" style="text-align: end;">
                            <button type="button" class="modal-title" data-dismiss="modal">&times;</button>
                           </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <label for="">Select Status</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="" >Select Status</option>
                            <option value="paid" >Paid</option>
                            <option value="unpaid">UnPaid</option>
                        </select>
                    </div>
                    <div class="modal-body">
                        <label for="">Amount</label>
                        <input type="number" class="form-control" id="amount" name="amount" >
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" >
                //$("#myModal").modal('show');
                $(document).ready(function(){
                    $( ".myModal" ).click(function() {
                        var id = $(this).attr("data-id");
                        $("#id").val(id);
                    });
                });
        </script>

@stop
