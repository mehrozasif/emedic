@extends('dashboard.layout')

@section('title','Edit Patient '.$user->display_name.'- E-Medic Dashboard ')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_patients_add'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
    <script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script>
@stop



@section('breadcrumb')
    <li>Admin Dashboard</li>
    <li>Patients</li>
    <li>{{ $user->display_name }}</li>
    <li class="active">Edit</li>
@stop

@section('content')

    @include('dashboard.admin.partials.header')
    <?php //var_dump($errors);die(); ?>
    <div class="row">
        <div class="col-md-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Edit {{ $user->display_name }} Profile</h3>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <form action="{{ url('dashboard/admin/patient/'.$user->name.'/edit') }}" method="POST" enctype="multipart/form-data">
                            @if( session('succ_msg') )
                                <div class="succeed-msg">
                                    {{ session('succ_msg') }}
                                    <script>
                                        setTimeout(function(){
                                            window.location.href = "{{ route('dashboard.admin.patient',$user->name) }}";
                                        },2000);
                                    </script>
                                </div>
                            @endif
                            <div class="row mb-1 mt-1">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="FNameHelp">First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="FNameHelp" value="{{ $user->first_name }}" placeholder="Enter first name" required>
                                        @if ($errors->has('first_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="LNameHelp">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="LNameHelp" value="{{ $user->last_name }}" placeholder="Enter Last name" required>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                        <label for="DNameHelp">Display Name</label>
                                        <input type="text" class="form-control"  name="display_name" id="DoctorName" aria-describedby="DNameHelp" value="{{ $user->display_name }}" placeholder="Enter Display Name" required>
                                        @if ($errors->has('display_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                        <label for="FFNameHelp">Father Name</label>
                                        <input type="text" class="form-control"  name="father_name" id="DoctorName" aria-describedby="FFNameHelp" value="{{ $patientdata->father_name }}" placeholder="Enter Display Name" required>
                                        @if ($errors->has('father_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('father_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="DoctorEmailHelp">Email</label>
                                        <input type="email" class="form-control"  name="email" id="DoctorName" aria-describedby="DoctorEmailHelp" value="{{ $user->email }}" placeholder="Enter Email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="DoctoraddressHelp">Address</label>
                                        <input type="text" class="form-control"  name="address" id="address" aria-describedby="DoctoraddressHelp" value="{{ $user->address }}" placeholder="Enter Address" required>
                                        @if ($errors->has('address'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                                        <label for="areaHelp">Area</label>
                                        <input type="text" class="form-control"  name="area" id="area" aria-describedby="areaHelp" value="{{ $patientdata->area }}" placeholder="Enter Address" required>
                                        @if ($errors->has('area'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('area') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                        <label for="DoctordobHelp">Date of Birth</label>
                                        <input type="date" class="form-control"  name="dob" id="dob" aria-describedby="DoctordobHelp" value="{{ $user->dob }}" required>
                                        @if ($errors->has('dob'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phoneno') ? ' has-error' : '' }}">
                                        <label>Phone no.</label>
                                        <input type="tel" class="form-control"  name="phoneno" id="address" placeholder="Enter Phone nio." value="{{ $user->phoneno }}" required>
                                        @if ($errors->has('phoneno'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('phoneno') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label>Gender</label>
                                        <select class="form-control" name="gender" required>
                                            <option value="" disabled selected>Select Gender</option>
                                            <option value="male" {{ $user->gender == 'male' ? 'selected':'' }}>Male</option>
                                            <option value="female" {{ $user->gender == 'female' ? 'selected':'' }}>Female</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                                        <label>Bloog Group</label>
                                        <select class="form-control" name="blood_group" required>
                                            <option selected disabled>Select your Blood Group</option>
                                            <option value="unknown" {{ $patientdata->blood_group == 'unknown' ? 'selected':'' }}>Unknown</option>
                                            <option value="A+" {{ $patientdata->blood_group == 'A+' ? 'selected':'' }}>A+</option>
                                            <option value="A-" {{ $patientdata->blood_group == 'A-' ? 'selected':'' }}>A-</option>
                                            <option value="B+" {{ $patientdata->blood_group == 'B+' ? 'selected':'' }}>B+</option>
                                            <option value="B-" {{ $patientdata->blood_group == 'B-' ? 'selected':'' }}>B-</option>
                                            <option value="O+" {{ $patientdata->blood_group == 'O+' ? 'selected':'' }}>O+</option>
                                            <option value="O-" {{ $patientdata->blood_group == 'O-' ? 'selected':'' }}>O-</option>
                                            <option value="AB+" {{ $patientdata->blood_group == 'AB+' ? 'selected':'' }}>AB+</option>
                                            <option value="AB-" {{ $patientdata->blood_group == 'AB-' ? 'selected':'' }}>AB-</option>
                                        </select>
                                        @if ($errors->has('blood_group'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('blood_group') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('symtoms') ? ' has-error' : '' }}">
                                        <label for="symtoms" class="control-label">Symtoms</label>
                                        <textarea type="text" name="symtoms" id="symtoms" class="form-control"  placeholder="Enter your Symtoms" >{{ $patientdata->symtoms }}</textarea>
                                         @if ($errors->has('symtoms'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('symtoms') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('allergies') ? ' has-error' : '' }}">
                                        <label for="allergies" class="control-label">Allergies</label>
                                        <textarea type="text" name="allergies" id="allergies" class="form-control" placeholder="Enter your Allergies" >{{ $patientdata->allergies }}</textarea>
                                         @if ($errors->has('allergies'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('allergies') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mt-3 text-center{{ $errors->has('history') ? ' has-error' : '' }}">
                                        <label for="history" class="control-label">Already a patient at Emedic?</label><br />
                                        <input type="radio" name="history" id="history" {{ $patientdata->history == '1' ? 'checked':'' }} value="yes" required /> Yes
                                        <input type="radio" name="history" id="history" {{ $patientdata->history == '0' ? 'checked':'' }} value="no" required /> No
                                        @if ($errors->has('history'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('history') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Profile Image</label>
                                        <input type="file" class="form-control"  name="profile_image" id="interests" accept="image/*">
                                        @if ($errors->has('profile_image'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('profile_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    @if(!empty($user->image))
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <div class="post-image-preview">
                                                    <img src="{{ asset($user->image) }}" width="100%" />
                                                    <div class="remove-image-preview"><i class="fa fa-close"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="delete_image" value="no" />
                                    <button type="submit" class="btn btn-primary">Edit Patient</button>
                                </div>
                            </div>
                            @if( session('err_msg') )
                                <div class="error-msg">
                                    {{ session('err_msg') }}
                                </div>
                            @endif
                            {{ csrf_field() }}
                        </form>
                    </div>
               </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
    </div>
@stop
