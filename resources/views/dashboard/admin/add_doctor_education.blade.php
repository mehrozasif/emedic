@extends('dashboard.layout')

@section('title','Add '.$doctor->display_name.' Education - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_doctors_add'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop

@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Doctors</li>
    <li>{{ $doctor->display_name }}</li>
    <li class="active">Add Education</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    <?php //var_dump($errors);die(); ?>
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Add {{ $doctor->display_name }}'s Education</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                    	<form action="{{ route('dashboard.admin.doctor.add.education.submit', $doctor->name) }}" method="POST">
                            @if( session('succ_msg') )
                                <div class="succeed-msg">
                                    {{ session('succ_msg') }}
                                    <script>
                                        setTimeout(function(){
                                            window.location.href = "{{ route('dashboard.admin.doctors') }}";
                                        },2000);
                                    </script>
                                </div>
                            @endif
                            <div class="row mb-1 mt-1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="NameHelp">Doctor Name</label>
                                        <input type="text" class="form-control" disabled id="NameHelp" aria-describedby="NameHelp" value="{{ $doctor->display_name }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="TitleHelp">Title</label>
                                        <input type="text" class="form-control" name="title" id="title" aria-describedby="TitleHelp" value="{{ old('title') }}" placeholder="Enter Title" required>
                                        @if ($errors->has('title'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="degreeHelp">Degree</label>
                                        <input type="text" class="form-control"  name="degree" id="degree" aria-describedby="degreeHelp" value="{{ old('degree') }}" placeholder="Enter Degree" required>
                                        @if ($errors->has('degree'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('degree') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="InstituteHelp">Institute</label>
                                        <input type="text" class="form-control"  name="institute" id="institute" aria-describedby="instituteHelp" value="{{ old('institute') }}" placeholder="Enter Institute" required>
                                        @if ($errors->has('institute'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('institute') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="LocationHelp">Location</label>
                                        <input type="text" class="form-control"  name="location" id="location" aria-describedby="LocationHelp" value="{{ old('location') }}" placeholder="Enter Location" required>
                                        @if ($errors->has('location'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('location') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="yearHelp">Year</label>
                                        <input type="number" class="form-control"  name="year" id="year" aria-describedby="yearHelp" placeholder="Enter Year" required>
                                        @if ($errors->has('year'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('year') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary">Add Doctor Education</button>
                                </div>
                            </div>
                            @if( session('err_msg') )
                                <div class="error-msg">
                                    {{ session('err_msg') }}
                                </div>
                            @endif
                            {{ csrf_field() }}
                        </form>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop