

@extends('dashboard.layout')

@section('title','Edit Doctor - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_doctors_add'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script>
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script>
@stop



@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Doctors</li>
    <li>{{ $doctor->display_name }}</li>
    <li class="active">Edit</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    <?php //var_dump($errors);die(); ?>
	<div class="row">
		<div class="col-md-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Edit {{ $doctor->display_name }} Doctor</h3>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                    	<form action="{{ route('dashboard.admin.edit.doctor.submit',$user->name) }}" method="POST" enctype="multipart/form-data">
                            @if( session('succ_msg') )
                                <div class="succeed-msg">
                                    {{ session('succ_msg') }}
                                    <script>
                                        setTimeout(function(){
                                            window.location.href = "{{ route('dashboard.admin.doctors') }}";
                                        },2000);
                                    </script>
                                </div>
                            @endif
                            <div class="row mb-1 mt-1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctorFNameHelp">First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="DoctorFNameHelp" value="{{ $user->first_name }}" placeholder="Enter first name" required>
                                        @if ($errors->has('first_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctorLNameHelp">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="DoctorLNameHelp" value="{{ $user->last_name }}" placeholder="Enter Last name" required>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctorDNameHelp">Doctor Name</label>
                                        <input type="text" class="form-control"  name="display_name" id="DoctorName" aria-describedby="DoctorDNameHelp" value="{{ $user->display_name }}" placeholder="Enter Display Name" required>
                                        @if ($errors->has('display_name'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctorEmailHelp">Email</label>
                                        <input type="email" class="form-control"  name="email" id="DoctorName" aria-describedby="DoctorEmailHelp" value="{{ $user->email }}" placeholder="Enter Email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctoraddressHelp">Address</label>
                                        <input type="text" class="form-control"  name="address" id="address" aria-describedby="DoctoraddressHelp" value="{{ $user->address }}" placeholder="Enter Address" required>
                                        @if ($errors->has('address'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="DoctordobHelp">Date of Birth</label>
                                        <input type="date" class="form-control"  name="dob" id="dob" aria-describedby="DoctordobHelp" value="{{ $user->dob }}" required>
                                        @if ($errors->has('dob'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone no.</label>
                                        <input type="tel" class="form-control"  name="phoneno" id="address" placeholder="Enter Phone nio." value="{{ $user->phoneno }}" required>
                                        @if ($errors->has('phoneno'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('phoneno') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select class="form-control" name="gender" required>
                                            <option value="" disabled selected>Select Gender</option>
                                            <option value="male" {{ $user->gender == 'male' ? 'selected':'' }}>Male</option>
                                            <option value="female" {{ $user->gender == 'female' ? 'selected':'' }}>Female</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Grade</label>
                                        <input type="number" class="form-control"  name="grade" id="grade" placeholder="Enter Grade" value="{{ $doctor->grade }}" required>
                                        @if ($errors->has('grade'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('grade') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <input type="text" class="form-control"  name="department" id="department" value="{{ $doctor->department }}" placeholder="Enter Department" required>
                                        @if ($errors->has('department'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('department') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Bio</label>
                                        <textarea class="form-control" name="bio" placeholder="Enter Doctor's Bio">{{ $doctor->bio }}</textarea>
                                        @if ($errors->has('bio'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('bio') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Certifications</label>
                                        <input type="text" class="form-control"  name="certifications" id="certifications" placeholder="Seperated by comma e.g certification,certification 1" value="{{ $doctor->certifications }}" required>
                                        @if ($errors->has('certifications'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('certifications') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Interests</label>
                                        <input type="text" class="form-control"  name="interests" id="interests" placeholder="Seperated by comma e.g interest,interest1" value="{{ $doctor->interests }}" required>
                                        @if ($errors->has('interests'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('interests') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Specialization</label>
                                        <input type="text" class="form-control"  name="specialization" id="specialization" placeholder="Enter Specialization" value="{{ $doctor->specialization }}" required>
                                        @if ($errors->has('specialization'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('specialization') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Awards</label>
                                        <input type="text" class="form-control"  name="awards" id="interests" placeholder="Seperated by comma e.g award,award1" value="{{ $doctor->awards }}" required>
                                        @if ($errors->has('awards'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('awards') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Profile Image</label>
                                        <input type="file" class="form-control"  name="profile_image" accept="image/*">
                                        @if ($errors->has('profile_image'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('profile_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    @if(!empty($user->image))
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <div class="post-image-preview">
                                                    <img src="{{ asset($user->image) }}" width="100%" />
                                                    <div class="remove-image-preview"><i class="fa fa-close"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="delete_image" value="no" />
                                    <button type="submit" class="btn btn-primary">Edit Doctor</button>
                                </div>
                            </div>
                            @if( session('err_msg') )
                                <div class="error-msg">
                                    {{ session('err_msg') }}
                                </div>
                            @endif
                            {{ csrf_field() }}
                        </form>
                    </div>
               </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
	</div>
@stop
