@extends('dashboard.layout')

@section('title','Contacts Dashboard - E-Medic')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_contacts'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
    <script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
    <script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop

@section('breadcrumb')
    <li>Admin Dashboard</li>
    <li class="active">Contacts</li>
@stop

@section('content')

    
    @include('dashboard.admin.partials.header')
    
    <div class="row">
        <div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Contacts</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-scroll">
                        <table class="emedic-activity-table w-100 p-1">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Time</th>
                            </tr>
                            @if(empty($contacts))
                                <tr>
                                    <td colspan="3">No Posts found.</td>
                                </tr>
                            @endif
                            @foreach($contacts as $key => $contact)
                                <tr>
                                    <td>{{ $contact->name }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->subject }}</td>
                                    <td>{{ $contact->message }}</td>
                                    <td>{{ $contact->created_at->format('dS F, Y h:i a') }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
    </div>
@stop






