@extends('layout')

@section('title',$user->display_name.' Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<div class="profile-remove-dp-btn"><i class="fa fa-close"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-10 mb-3 mt-5 ml-auto mr-auto">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Personal Details</h2>
						</div>
						<div class="container-fluid">
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="profile-edit-btn"><i class="fa fa-edit"></i></div>
								<div class="col-md-6">
									<h5>Display Name</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->display_name }}</p>
									<form action="#" method="POST" class="form-group hidden profile-field-change-form" data-table="user">
										<div class="row">
											<div class="col-md-8">
												<input type="text" name="display_name" class="form-control my-input input-val" value="{{ $user->display_name }}" />
											</div>
											<div class="col-md-4">
												<input type="submit" class="form-control" value="Change" />
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<span class="profile-edit-btn"><i class="fa fa-edit"></i></span>
								<div class="col-md-6">
									<h5>Email</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->email }}</p>
									<form action="#" method="POST" class="form-group hidden profile-field-change-form" data-table="user">
										<div class="row">
											<div class="col-md-8">
												<input type="email" name="email" class="form-control my-input input-val" value="{{ $user->email }}" />
											</div>
											<div class="col-md-4">
												<input type="submit" class="form-control" value="Change" />
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Age</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->getAge() }}</p>
								</div>
							</div>
							<div class="row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Gender</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->gender }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<span class="profile-edit-btn"><i class="fa fa-edit"></i></span>
								<div class="col-md-6">
									<h5>Address</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->address }}</p>
									<form action="#" method="POST" class="form-group hidden profile-field-change-form" data-table="user">
										<div class="row">
											<div class="col-md-8">
												<input type="text" name="address" class="form-control my-input input-val" value="{{ $user->address }}" />
											</div>
											<div class="col-md-4">
												<input type="submit" class="form-control" value="Change" />
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<span class="profile-edit-btn"><i class="fa fa-edit"></i></span>
								<div class="col-md-6">
									<h5>Phone No.</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->phoneno }}</p>
									<form action="#" method="POST" class="form-group hidden profile-field-change-form" data-table="user">
										<div class="row">
											<div class="col-md-8">
												<input type="tel" name="phoneno" class="form-control my-input input-val" value="{{ $user->phoneno }}" />
											</div>
											<div class="col-md-4">
												<input type="submit" class="form-control" value="Change" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection