@extends('dashboard.layout')

@section('title','Edit Post - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['$active_blog'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop


@section('scripts')

    <script type="text/javascript" src="{{ asset('js/admin/codemirror/codemirror.js') }} "></script>  
    <script type='text/javascript' src="{{ asset('js/admin/codemirror/mode/htmlmixed/htmlmixed.js') }} "></script>
    <script type='text/javascript' src="{{ asset('js/admin/codemirror/mode/xml/xml.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/admin/summernote/summernote.js') }}"></script>

	<script type='text/javascript' src="{{ asset('js/lib/moment.min.js') }}"></script> 
	<script type='text/javascript' src="{{ asset('js/admin/actions.js') }}"></script> 
@stop


    

@section('breadcrumb')
    <li>Admin Dashboard</li>
    <li>Blogs</li>
    <li>Edit Post</li>
    <li class="active">{{ $post->title }}</li>
@stop

@section('content')

    @include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Edit Post '{{ $post->title }}'</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <form action="{{ route('dashboard.admin.post.edit.submit',$post->id) }}" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}
                            @if( session('succ_msg') )
                                <div class="succeed-msg">
                                    {{ session('succ_msg') }}
                                    <script>
                                        setTimeout(function(){
                                            window.location.href = "{{ route('dashboard.admin.blogs') }}";
                                        },2000);
                                    </script>
                                </div>
                            @endif
                            <div class="row mt-1 mb-1">
                                <div class="col-md-12">
                                    <h3>Title</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="post_title" placeholder="Enter the post Title" value="{{ $post->title }}" required />
                                </div>
                            </div>
                            <div class="row mt-1 mb-1">
                                <div class="col-md-12">
                                    <h3>Content</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea rows="10" class="summernote" name="post_content" placeholder="Enter the post Content" required>{{ $post->body }}</textarea>
                                </div>
                            </div>
                            <div class="row mt-1 mb-1">
                                <div class="col-md-12">
                                    <h3>Featured Image</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" class="form-control" name="post_image" accept="image/*" />
                                    @if(!empty($post->featured_image))
                                    <div class="row mt-3">
                                        <div class="col-md-3">
                                            <div class="post-image-preview">
                                                <img src="{{ asset($post->featured_image) }}" width="100%" />
                                                <div class="remove-image-preview"><i class="fa fa-close"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row mt-1 mb-1">
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="delete_image" value="no" />
                                    <button type="submit" class="btn btn-primary">Edit Post</button>
                                </div>
                            </div>
                        </form>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop