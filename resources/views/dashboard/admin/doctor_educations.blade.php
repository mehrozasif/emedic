@extends('dashboard.layout')

@section('title',$user->display_name.' Educations- E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_doctors_view'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Doctors</li>
    <li>{{ $user->display_name }}</li>
    <li class="active">Educations</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>{{ $user->display_name }} Educations</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    	@if(empty($educations))
                        <div class="emedic-admin-activity">
                    		<div class="emedic-admin-no-result">
                    			<h3>No Education Found of this doctor</h3>
                    		</div>
                    	@else
                        <div class="emedic-admin-activity-full">
                    		<table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>degree</th>
                                        <th>Insitiute</th>
                                        <th>Location</th>
                                        <th>Year</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($educations as $education)
                                    <tr>
                                        <td>{{ $education->title }}</td>
                                        <td>{{ $education->degree }}</td>
                                        <td>{{ $education->institute }}</td>
                                        <td>{{ $education->location }}</td>
                                        <td>{{ $education->year }}</td>
                                        <td>
                                            <a href="{{ route('dashboard.admin.doctor.remove.education',$education->id) }}">remove</a> | <a href="{{ route('dashboard.admin.doctor.edit.education',$education->id) }}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                    	@endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop