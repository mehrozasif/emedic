@extends('dashboard.layout')

@section('title','Chat - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.admin.partials.sidebar',['active_messages' => TRUE])
@stop

@section('topbar')
    @include('dashboard.admin.partials.topbar')
@stop

@section('headmeta')
    <meta name="chat_receiver_id" content="{{ $userdetails->id }}">
    <meta name="chat_sender_id" content="{{ $admin->id }}">
@stop

@section('scripts')

    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script type='text/javascript' src="{{ asset('js/lib/moment.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/admin/actions.js') }}"></script>



    <script type='text/javascript' src="{{ asset('js/chat/main.js') }}"></script>

@stop



@section('breadcrumb')
	<li>Admin Dashboard</li>
    <li>Messages</li>
    <li class="active">{{ $userdetails->display_name }}</li>
@stop

@section('content')

	@include('dashboard.admin.partials.header')

	<div class="row">
		<div class="col-md-12">
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>{{ $userdetails->display_name }} Messages</h3>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    <div class="chat-view-messages">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chat-user-list">
                                    <div class="chat-user-list-view">
                                        @if(empty($chatlist))
                                            <div class="row pt-3 pb-3 chat-no-messages">
                                                <div class="col-md-12">
                                                    <h2 class="text-center mt-3 mb-3">No new Messages</h2>
                                                </div>
                                            </div>
                                        @endif
                                        @foreach($chatlist as $chat)
                                            <div class="row" title="{{ $chat['chat']->created_at->format('jS F, Y h:i a') }}" >
                                                <div class="col-md-12">
                                                    <div class="chat-user-message{{ ( $chat['otheruser']->id == $chat['sender']->id) ? ' chat-text-left':' chat-text-right' }}">
                                                        <p>{{ $chat['chat']->message }}</p>
                                                        <p><small>{{ $chat['chat']->created_at->format('jS F, Y h:i a') }}</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chat-form">
                        <form action="#" class="chat-form" method="POST">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <textarea class="form-control chat-message-input" placeholder="Enter your message..." ></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary w-100 h-100 chat-message-btn" value="Send" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
               </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
	</div>
@stop
