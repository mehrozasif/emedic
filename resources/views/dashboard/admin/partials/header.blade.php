<div class="row">
		<div class="col-md-3">
			<!-- START WIDGET VISITED -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-eye"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $totalvisitors }}</div>
                        <div class="widget-title">Visitors</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET VISITED -->
		</div>
		<div class="col-md-3">
            <!-- START WIDGET POSTS -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='{{ url('dashboard/admin/doctors') }}';">
                    <div class="widget-item-left">
                        <img src="{{ asset('images/default-user-doctor.jpg') }}" width="100%" />
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $doctorscount }}</div>
                        <div class="widget-title">Doctors</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET POSTS -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET POSTS -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='{{ url('dashboard/admin/patients') }}';">
                    <div class="widget-item-left">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $patientscount }}</div>
                        <div class="widget-title">Patients</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET POSTS -->
        </div>
		<div class="col-md-3">
			<!-- START WIDGET POSTS -->
                <div class="widget widget-default widget-item-icon"onclick="location.href='{{ url('dashboard/admin/blog') }}';">
                    <div class="widget-item-left">
                        <img src="{{ asset('images/post.png') }}" width="100%" />
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $totalposts }}</div>
                        <div class="widget-title">Blog Posts</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET POSTS -->
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-3">
            <!-- START WIDGET VISITED -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ route('dashboard.admin.app.requested') }}';">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $appointments_requested_count }}</div>
                    <div class="widget-title">Appointments Requests</div>
                </div>                          
            </div>                            
            <!-- END WIDGET VISITED -->
        </div>

        <div class="col-md-3">
            <!-- START WIDGET VISITED -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ url('dashboard/admin/appointments/booked') }}';">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $appointments_count }}</div>
                    <div class="widget-title">Appointments Booked</div>
                </div>                          
            </div>                            
            <!-- END WIDGET VISITED -->
        </div>
		
		<div class="col-md-3">
			<!-- START WIDGET POSTS -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ url('dashboard/admin/reports') }}';">

                    <div class="widget-item-left">
                         <span class="fa fa-file"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $reportscount }}</div>
                        <div class="widget-title">Reports Generated</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET POSTS -->
		</div>
        <div class="col-md-3">
            <!-- START WIDGET CLOCK -->
            <div class="widget widget-info widget-padding-sm pt-30">
                <div class="widget-big-int plugin-clock">00:00</div>                            
                <div class="widget-subtitle plugin-date">Loading...</div>  
            </div>                        
            <!-- END WIDGET CLOCK -->
        </div>
	</div>