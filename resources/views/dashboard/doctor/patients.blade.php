@extends('dashboard.layout')

@section('title','Patients Dashboard - E-Medic')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_patients'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop

@section('scripts')
    <script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
    <script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script>
@stop



@section('breadcrumb')
    <li>Doctor Dashboard</li>
    <li class="active">Patients</li>
@stop

@section('content')

    @include('dashboard.doctor.partials.header')
    
    <div class="row">
        <div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Patients</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        @if(empty($doctor_patients))
                            <div class="emedic-admin-no-result">
                                <h3>No Patient</h3>
                            </div>
                        @else
                            <table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($doctor_patients as $patient)
                                    <tr>
                                        <td>{{ $patient['name'] }}</td>
                                        <td>{{ $patient['email'] }}</td>
                                        <td>{{ $patient['gender'] }}</td>
                                        <td>{{ $patient['age'] }}</td>
                                        <td><a href="/dashboard/doctor/patient/{{ $patient['id'] }}">View</a> | <a href="/dashboard/doctor/report/patient_{{ $patient['id'] }}">Reports</a> <br /> <a href="{{ $patient['id'] }}" data-doctorid="{{ $doctor->id }}" class="doctor-call-appointment">Call for Appoinment</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @endif
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
    </div>
@stop

@section('footer')
    <div class="emedic-model-container">
        <div class="emedic-model-bg"></div>
        <div class="emedic-select-schedule emedic-model">
            <div class="emedic-model-close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <div class="emedic-header">
                <h2>Select Schedule</h2>
            </div>
            <div class="emedic-content">
                <form action="#" class="select-doctor-schedule-form">
                    <!-- 
                    <select class="form-control" name="select-doctor-schedule" required>
                        <option disabled selected>Select Your Schedule</option>
                        @if(empty($schedules['monday']) && empty($schedules['tuesday']) && empty($schedules['wednesday']) && empty($schedules['thursday']) && empty($schedules['friday']) && empty($schedules['saturday']) && empty($schedules['sunday']) )
                                <option value="" disabled selected>All Schedules are booked or not found</option>
                        @else
                            <option value="" disabled selected>Select Appointment time</option>
                        @endif

                        @foreach($schedules['monday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach

                        @foreach($schedules['tuesday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                        @foreach($schedules['wednesday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                        @foreach($schedules['thursday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                        @foreach($schedules['friday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                        @foreach($schedules['saturday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                        @foreach($schedules['sunday'] as $app_time)
                            <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                        @endforeach
                    </select>
                    -->
                    <div class="form-group">
                        <label for="app_date" class="control-label">Appointment Date</label>
                        <input type="date" name="select-doctor-schedule-date" id="select-doctor-schedule-date" class="form-control" placeholder="Enter the appointment date" required />
                    </div>
                    <div class="form-group">
                        <label for="app_time" class="control-label">Appointment Time</label>
                        <input type="time" name="select-doctor-schedule-time" id="select-doctor-schedule-time" class="form-control" placeholder="Enter the appointment time" required />
                    </div>
                    <div class="select-schedule-submit-container">
                        <textarea class="form-control mt-3 mb-3" name="select-doctor-schedule-reason" placeholder="Enter the reason" required minlength="15"></textarea>
                        <select class="form-control mt-3 mb-3" name="select-doctor-schedule-type" required>
                            <option value="" disabled selected>Select The Appointment type</option>
                            <option value="normal">Normal</option>
                            <option value="emergency">Emergency</option>
                        </select>
                        <input class="w-50 m-auto" type="submit" name="select-doctor-schedule-submit" value="Call for Appointment" />
                        <input type="hidden" class="select-doctor-schedule-patient-id" /> 
                        <input type="hidden" class="select-doctor-schedule-doctor-id" /> 
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
