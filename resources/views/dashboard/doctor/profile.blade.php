@extends('dashboard.layout')

@section('title','E-Medic - Profile Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_profile'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li class="active">Profile</li>
@stop

@section('content')

	@include('dashboard.doctor.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Profile</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                    	<div class="">
                            <table class="emedic-activity-table w-100 p-1">
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $doctor->email }}</td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td>{{ ucfirst($doctor->gender) }}</td>
                                </tr>
                                <tr>
                                    <th>Date of Birth</th>
                                    <td>{{ $doctor->dob }}</td>
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td>{{ $doctor->getAge() }}</td>
                                </tr>
                                <tr>
                                    <th>Grade</th>
                                    <td>{{ $doctortable->grade }}</td>
                                </tr>
                                <tr>
                                    <th>BIO</th>
                                    <td>{{ $doctortable->bio }}</td>
                                </tr>
                                <tr>
                                    <th>Specialization</th>
                                    <td>{{ $doctortable->specialization }}</td>
                                </tr>
                                <tr>
                                    <th>Certifications</th>
                                    <td>{{ str_replace(',',', ',$doctortable->certifications) }}</td>
                                </tr>
                                <tr>
                                    <th>Interest</th>
                                    <td>{{ str_replace(',',', ',$doctortable->interests) }}</td>
                                </tr>
                                <tr>
                                    <th>Awards</th>
                                    <td>{{ str_replace(',',', ',$doctortable->awards) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->



            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Appointment Schedules</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="{{--route('dashboard.doctor.schedules')--}}"><span class="fa fa-cog"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <table class="emedic-activity-table w-100 p-1 schedules-table">
                            <tr>
                                <th>Day</th>
                                 <th>Time</th>
                             </tr>
                             @if(empty($schedules) || $schedules->isEmpty())
                                 <tr class="no-schedule-tr">
                                    <td colspan="2"><h3 class="text-center">No Schedules Added</h3></td>
                                </tr>
                            @endif
                            @foreach($schedules as $schedule)
                                <tr>
                                    <th>{{ ucfirst($schedule->day) }}</th>
                                    <td>{{ $schedule->time }}</td>
                                </tr>
                            @endforeach
                            <tr class="mt-2">
                                <td colspan="2" class="text-center"><a href="{{ route('dashboard.doctor.schedules') }}">Add Appointment Schedules</a></td>
                            </tr>
                        </table>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop