@extends('dashboard.layout')

@section('title',$user->name.' Reports - Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_reports'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop


@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Reports</li>
    <li class="active">{{ $user->name }}</li>
@stop

@section('content')

	
    @include('dashboard.doctor.partials.header')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h2>{{ $user->display_name }} Reports</h2>
                    </div>                      
                </div>                                
                <div class="panel-body padding-0">
                        @if(empty($reports) || $reports->isEmpty() )

                        <div class="emedic-admin-activity">
                            <div class="emedic-admin-no-result">
                                <h3>No Patient Reports.<a href="{{ url('/dashboard/doctor/report/'.$user->id).'/create' }}">Click here to Make one.</a></h3>
                            </div>
                        @else
                        <div class="emedic-admin-activity-full">
                            <table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Report Id</th>
                                        <th>Modified At</th>
                                        <th>Generated At</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ( $i=0 )
                                    @foreach($reports as $key => $report)
                                    @php ( $i=0 )
                                    
                                    <tr>
                                        <td>Report {{ $key }}</td>
                                        <td>{{ $report->updated_at->format('dS F, Y h:i a') }}</td>
                                        <td>{{ $report->created_at->format('dS F, Y h:i a') }}</td>
                                        <td>
                                            @if($report->finished == '0')
                                            ongoing
                                            @else
                                            Treated
                                            @endif
                                        </td>
                                        <td><a href="{{ url('/dashboard/doctor/report/'.$report->id) }}">View</a>
                                         @if($report->finished == '0')
                                         @php ( $i++ )
                                         | <a href="{{ url('/dashboard/doctor/report/'.$report->id.'/edit') }}">Edit</a> 
                                         @endif
                                         | <a href="{{ url('/dashboard/doctor/report/'.$report->id.'/generate') }}">Download</a>
                                         @if($report->finished == '0')
                                         <br />
                                         <a href="{{ $report->id }}" class="doctor-mark-patient-treated">Mark Patient as Treated</a></td>
                                         @endif
                                    </tr>
                            @endforeach
                                    @php ($i=0)
                                    @if($i == 0)
                                        <tr>
                                            <th colspan="5" class="text-center"><a href="{{ url('/dashboard/doctor/report/'.$user->id).'/create' }}">Click here to make a new report.</a></th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
    </div>
@stop