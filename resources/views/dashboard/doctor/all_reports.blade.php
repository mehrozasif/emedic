@extends('dashboard.layout')

@section('title',' Reports - Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_reports'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop


@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Reports</li>
@stop

@section('content')

	
    @include('dashboard.doctor.partials.header')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h2>Your Reports</h2>
                    </div>                      
                </div>                                
                <div class="panel-body padding-0">
                        @if(empty($reports) || $reports->isEmpty() )

                        <div class="emedic-admin-activity">
                            <div class="emedic-admin-no-result">
                                <h3>No Report Generated</a></h3>
                            </div>
                        @else
                        <div class="emedic-admin-activity-full">
                            <table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Patient Name</th>
                                        <th>Modified At</th>
                                        <th>Generated At</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($reports as $key => $report)
                                    <tr>
                                        <td>{{ $patientsdata[$key]->display_name }}</td>
                                        <td>{{ $report->updated_at->format('dS F, Y h:i a') }}</td>
                                        <td>{{ $report->created_at->format('dS F, Y h:i a') }}</td>
                                        <td>
                                            @if($report->finished == '0')
                                            ongoing
                                            @else
                                            Treated
                                            @endif
                                        </td>
                                        <td><a href="{{ url('/dashboard/doctor/report/'.$report->id) }}">View</a>
                                         @if($report->finished == '0')
                                         | <a href="{{ url('/dashboard/doctor/report/'.$report->id.'/edit') }}">Edit</a> 
                                         @endif
                                         | <a href="{{ url('/dashboard/doctor/report/'.$report->id.'/generate') }}">Download</a>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
@stop