<div class="row">
		<div class="col-md-4">
			<!-- START WIDGET VISITED -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='{{ route('dashboard.doctor.patients') }}';">
                    <div class="widget-item-left">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $patients }}</div>
                        <div class="widget-title">Patients</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET VISITED -->
		</div>
		<div class="col-md-4">
			<!-- START WIDGET REGISTRED -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $patients_treated }}</div>
                        <div class="widget-title">Patients treated</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET REGISTRED -->
		</div>
		<div class="col-md-4">
			<!-- START WIDGET POSTS -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ route('dashboard.doctor.reports') }}';">
            <div class="widget-item-left">
                        <img src="{{ asset('images/post.png') }}" width="100%" />
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{ $reports_generated }}</div>
                        <div class="widget-title">Reports generated</div>
                    </div>                          
                </div>                            
                <!-- END WIDGET POSTS -->
		</div>
	</div>
    <div class="row">
        <div class="col-md-4">
            <!-- START WIDGET VISITED -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ route('dashboard.doctor.app.requested') }}';">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $appointments_requested_count }}</div>
                    <div class="widget-title">Appointments Requests</div>
                </div>                          
            </div>                            
            <!-- END WIDGET VISITED -->
        </div>

        <div class="col-md-4">
            <!-- START WIDGET VISITED -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{ route('dashboard.doctor.app.booked') }}';">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $appointments_count }}</div>
                    <div class="widget-title">Appointments Booked</div>
                </div>                          
            </div>                            
            <!-- END WIDGET VISITED -->
        </div>
        <div class="col-md-4">
            <!-- START WIDGET CLOCK -->
            <div class="widget widget-info widget-padding-sm pt-30" >
                <div class="widget-big-int plugin-clock">00:00</div>                            
                <div class="widget-subtitle plugin-date">Loading...</div>  
            </div>                        
            <!-- END WIDGET CLOCK -->
        </div>
    </div>
	<div class="hr" ></div>