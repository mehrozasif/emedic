            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="{{ url('/dashboard') }}">Dashboard</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{ !empty($doctor->image) ? $doctor->image : asset('images/default-user.jpg') }}" alt="{{ $doctor->name }}"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{ !empty($doctor->image) ? $doctor->image : asset('images/default-user.jpg') }}" alt="{{ $doctor->name }}"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{ $doctor->display_name }}</div>
                                <div class="profile-data-title">{{ ucwords($doctor->type) }}</div>
                            </div>
                            
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li @if(isset($active_dasboard)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.home') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>   
                    <li @if(isset($active_messages)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.messages') }}"><span class="fa fa-comments"></span> <span class="xn-text">Messages</span></a>
                    </li>
                    <li @if(isset($active_profile)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.profile') }}"><span class="fa fa-user"></span> <span class="xn-text">Profile</span></a>
                    </li> 
                    <li @if(isset($active_schedules)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.schedules') }}"><span class="fa fa-user"></span> <span class="xn-text">Schedules</span></a>
                    </li> 
                    <li class="xn-openable @if(isset($active_app_req) || isset($active_app_book)) active @endif">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Appointments</span></a>
                        <ul>
                            <li @if(isset($active_app_req)) class="active" @endif><a href="{{ route('dashboard.doctor.app.requested') }}">Requested</a></li>
                            <li @if(isset($active_app_book)) class="active" @endif><a href="{{ route('dashboard.doctor.app.booked') }}">Booked</a></li>
                        </ul>
                    </li>
                    <li @if(isset($active_patients)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.patients') }}"><span class="fa fa-user"></span> <span class="xn-text">Patients</span></a>                        
                    </li>
                    <li class="xn-openable @if(isset($active_blog) || isset($active_add_post)) active @endif">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Blogs</span></a>
                        <ul>
                            <li @if(isset($active_blog)) class="active" @endif><a href="{{ route('dashboard.doctor.blogs') }}">View Posts</a></li>
                            <li @if(isset($active_add_post)) class="active" @endif><a href="{{ route('dashboard.doctor.new.blog') }}">Add new Post</a></li>
                        </ul>
                    </li>
                    <li @if(isset($active_reports)) class="active" @endif>
                        <a href="{{ route('dashboard.doctor.reports') }}"><span class="fa fa-user"></span> <span class="xn-text">Reports</span></a>                        
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->