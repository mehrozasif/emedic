                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span class="fa fa-sign-out"></span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>                   
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">{{ count(Auth::user()->getUnseenAllChat()) == '0'? '':count(Auth::user()->getUnseenAllChat()) }}</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span>Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">{{ count(Auth::user()->getUnseenAllChat()) }}</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                @foreach(Auth::user()->getAllChat() as $chat)
                                @php

                                $chatmessagelink = '';
                                if(Auth::user()->hasRole('doctor')):
                                    $chatmessagelink = route('dashboard.doctor.messages.chat', $chat['otheruser']->name);
                                elseif(Auth::user()->hasRole('admin')):
                                    $chatmessagelink = route('dashboard.doctor.messages.chat', $chat['otheruser']->name);
                                endif;

                                @endphp
                                <a href="{{ $chatmessagelink }}" class="list-group-item">
                                    <div class="list-group-status"></div>
                                    <img src="{{ !empty($chat['otheruser']->image)? $chat['otheruser']->image: asset('images/default-user.jpg') }}" class="pull-left" alt="{{ $chat['otheruser']->display_name }}"/>
                                    <span class="contacts-title">{{ $chat['otheruser']->display_name }}</span>
                                    <p>{{ $chat['chat']->message }}</p>
                                </a>
                                @endforeach
                                
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="{{ route('dashboard.doctor.messages') }}">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li>
                    <!-- END MESSAGES -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->   
                