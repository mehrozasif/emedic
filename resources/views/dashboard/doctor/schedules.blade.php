@extends('dashboard.layout')

@section('title','E-Medic - Schedules Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_schedules'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Profile</li>
    <li class="active">Schedules</li>
@stop

@section('content')

	@include('dashboard.doctor.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Appointment Schedules</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                            <table class="emedic-activity-table w-100 p-1 schedules-table">
                                <tr>
                                    <th>Day</th>
                                    <th>Time</th>
                                    <th>Action</th>
                                </tr>
                                @if(empty($schedules) || $schedules->isEmpty())
                                    <tr class="no-schedule-tr">
                                        <td colspan="3"><h3 class="text-center">No Schedules Added</h3></td>
                                    </tr>
                                @endif
                                @foreach($schedules as $schedule)
                                    <tr>
                                        <th>{{ ucfirst($schedule->day) }}</th>
                                        <td>{{ $schedule->time }}</td>
                                        <td><a href="{{ $schedule->id }}" class="remove-schedule">Remove</a></td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="hr"></div>
                            <table class="emedic-activity-table w-100 p-1">
                                <tr>
                                    <th colspan="3" class="text-center">Add Schedule</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Day</th>
                                    <th class="text-center">Start Time</th>
                                    <th class="text-center">End Time</th>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <select name="schedule-day" class="form-control">
                                            <option disabled selected></option>
                                            <option value="sunday">Sunday</option>
                                            <option value="monday">Monday</option>
                                            <option value="tuesday">Tuesday</option>
                                            <option value="wednesday">Wednesday</option>
                                            <option value="thursday">Thursday</option>
                                            <option value="friday">Friday</option>
                                            <option value="saturday">Saturday</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="time" class="form-control" name="schedule-start-time" />
                                    </td>
                                    <td>
                                        <input type="time" class="form-control" name="schedule-end-time" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center">
                                        <input type="hidden" class="form-control" name="schedule-user-id"  value="{{ $doctortable->id }}" />
                                        <input type="submit" class="form-control w-50 m-auto" name="schedule-add-btn" value="Add Schedule" />
                                    </td>
                                </tr>
                            </table>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop