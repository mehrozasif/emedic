@extends('dashboard.layout')

@section('title','Edit '.$user->display_name.' Report - Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_reports'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop


@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Reports</li>
    <li>Edit</li>
    <li class="active">{{ $user->display_name }}</li>
@stop

@section('content')

	
    @include('dashboard.doctor.partials.header')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h2>Edit {{ $user->display_name }}'s Report</h2>
                    </div>                      
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <form action="{{ route('dashboard.doctor.report.edit.submit',$report->id) }}" method="POST">
                            @if( session('succ_msg') )
                                <div class="succeed-msg">
                                    {{ session('succ_msg') }}
                                    <script>
                                        setTimeout(function(){
                                            window.location.href = "{{url('/dashboard/doctor/patient/'. $user->id) }}";
                                        },2000);
                                    </script>
                                </div>
                            @endif
                            <div class="row mb-1 mt-1">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="PatientNameHelp">Patient Name</label>
                                        <input type="text" class="form-control" disabled id="PatientName" aria-describedby="PatientNameHelp" value="{{ $user->display_name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="PatientAgeHelp">Age</label>
                                        <input type="text" class="form-control" disabled id="Patient" aria-describedby="PatientAgeHelp" name="age" value="{{ $user->getAge() }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="DepartmentHelp">Department</label>
                                        <input type="text" class="form-control" id="Department" aria-describedby="DepartmentHelp" name="department" placeholder="Enter the department" value="{{ $report->department }}" required />
                                        @if ($errors->has('department'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('department') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="DoctorNameHelp">Doctor Name</label>
                                        <input type="text" class="form-control" disabled id="DoctorName" aria-describedby="DoctorNameHelp" value="{{ $doctor->display_name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="InitDignosisHelp">Initial Dignosis</label>
                                        <textarea rows="7" class="form-control" name="initial_dignosis" id="InitDignosis" aria-describedby="InitDignosisHelp" required >{{ $report->initial_dignosis }}</textarea>
                                        @if ($errors->has('initial_dignosis'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('initial_dignosis') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="FinalDignosisHelp">Final Dignosis</label>
                                        <textarea rows="7" class="form-control" name="final_dignosis" id="FinalDignosis" aria-describedby="FinalDignosisHelp">{{ $report->final_dignosis }}</textarea>
                                        @if ($errors->has('final_dignosis'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('final_dignosis') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="DiseaseHelp">Disease</label>
                                        <input type="text" class="form-control" name="disease" id="Disease" aria-describedby="DiseaseHelp" value="{{ $report->disease }}" required />
                                        @if ($errors->has('disease'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('disease') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="TDiseaseHelp">Type of Disease</label>
                                        <input type="text" class="form-control" name="tdisease" id="TDisease" aria-describedby="TDiseaseHelp" value="{{ $report->tdisease }}" required/>
                                        @if ($errors->has('tdisease'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('tdisease') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="ExaminationHelp">Examination</label>
                                        <textarea rows="7" class="form-control" name="examination" id="Examination" aria-describedby="ExaminationHelp" required>{{ $report->examination }}</textarea>
                                        @if ($errors->has('examination'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('examination') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="TreatmentHelp">Treatment</label>
                                        <textarea rows="7" class="form-control" name="treatment" id="Treatment" aria-describedby="TreatmentHelp" >{{ $report->treatment }}</textarea>
                                        @if ($errors->has('treatment'))
                                            <span class="help-block red">
                                                <strong>{{ $errors->first('treatment') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary">Edit Report</button>
                                </div>
                            </div>
                            @if( session('err_msg') )
                                <div class="error-msg">
                                    {{ session('err_msg') }}
                                </div>
                            @endif
                            <input type="hidden" name="patient_id" value="{{ $user->id }}" />
                            <input type="hidden" name="doctor_id" value="{{ $doctor->id }}" />
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
@stop