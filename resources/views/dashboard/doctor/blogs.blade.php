@extends('dashboard.layout')

@section('title','Blog - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_blog'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop

@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Blog</li>
    <li class="active">Blog Post</li>
@stop

@section('content')

	@include('dashboard.doctor.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>All Blog Posts</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <table class="emedic-activity-table w-100 p-1">
                            <tr>
                                <th>Title</th>
                                <th>Posted Date</th>
                                <th>Updated Date</th>
                                <th>Action</th>
                            </tr>
                            @if(empty($posts))
                                <tr>
                                    <td colspan="3">No Posts found.</td>
                                </tr>
                            @endif
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->created_at->format('dS F, Y h:i a') }}</td>
                                    <td>{{ $post->updated_at->format('dS F, Y h:i a') }}</td>
                                    <td>
                                        <a href="{{ url('/post/'.$post->slug) }}">View</a>
                                        | <a href="{{ url('/dashboard/doctor/blog/edit/'.$post->id) }}">Edit</a>
                                        | <a href="{{ $post->id }}" class="blog-remove-btn">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop