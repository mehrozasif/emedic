@extends('dashboard.layout')

@section('title','Messages - E-Medic Dashboard')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_messages'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop


@section('headmeta') 

@stop

@section('scripts')

	<script type='text/javascript' src="{{ asset('js/lib/moment.min.js') }}"></script> 
	<script type='text/javascript' src="{{ asset('js/admin/actions.js') }}"></script>
@stop


@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li class="active">Messages</li>
@stop

@section('content')

	@include('dashboard.doctor.partials.header')
    
	<div class="row">
		<div class="col-md-12"> 
            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Messages</h3>
                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">  
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                                    
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity">
                    	<div class="row">
                    		<div class="col-md-12">
                    			<div class="chat-user-list">
                    				<div class="chat-user-list-view">
                    					@if(empty($chatlist))
                    						<div class="row pt-3 pb-3">
                    							<h2 class="text-center mt-3 mb-3">No new Messages</h2>
                    						</div>
                    					@endif
                    					@foreach($chatlist as $chat)
                    						<div class="row chat-user" title="Click to view" onclick="window.location.href= '{{ route('dashboard.doctor.messages.chat',$chat['otheruser']->name) }}'">
                    							<div class="col-md-12">
                    								<div class="d-flex just-between">
                    									<h3>{{ $chat['otheruser']->display_name }}</h3>
                    									<div class="chat-user-action">View messages</div>
                    								</div>
                    							</div>
                    						</div>
                    					@endforeach
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
               </div>                                    
            </div>
            <!-- END USERS ACTIVITY BLOCK -->
                            
        </div>
	</div>
@stop