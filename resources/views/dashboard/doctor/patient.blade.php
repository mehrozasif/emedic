@extends('dashboard.layout')

@section('title','Patient '. $patient['name'] .' Dashboard - E-Medic')


@section('sidebar')
    @include('dashboard.doctor.partials.sidebar',['active_patients'=>TRUE])
@stop

@section('topbar')
    @include('dashboard.doctor.partials.topbar')
@stop


@section('scripts')
	<script type='text/javascript' src='{{ asset('js/lib/moment.min.js') }}'></script> 
	<script type='text/javascript' src='{{ asset('js/admin/actions.js') }}'></script> 
@stop



@section('breadcrumb')
	<li>Doctor Dashboard</li>
    <li>Patients</li>
    <li class="active">{{ $patient['name'] }}</li>
@stop

@section('content')

	@include('dashboard.doctor.partials.header')

    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h2>{{ $patient['name'] }}</h2>
                    </div>                      
                </div>                                
                <div class="panel-body padding-0">
                    <div class="emedic-admin-activity-full">
                        <table class="emedic-activity-table w-100 p-1">
                            <tr>
                                <th>Email</th>
                                <td>{{ $patient['email'] }}</td>
                            </tr>
                            <tr>
                                <th>Father Name</th>
                                <td>{{ $patient['father_name'] }}</td>
                            </tr>
                            <tr>
                                <th>Gender</th>
                                <td>{{ ucfirst($patient['gender']) }}</td>
                            </tr>
                            <tr>
                                <th>Date of Birth</th>
                                <td>{{ $patient['dob'] }}</td>
                            </tr>
                            <tr>
                                <th>Age</th>
                                <td>{{ $patient['age'] }}</td>
                            </tr>
                            <tr>
                                <th>Blood Group</th>
                                <td>{{ $patient['blood_group'] }}</td>
                            </tr>
                            <tr>
                                <th>Symtoms</th>
                                <td>{{ $patient['symtoms'] }}</td>
                            </tr>
                            <tr>
                                <th>Allergies</th>
                                <td>{{ $patient['allergies'] }}</td>
                            </tr>
                            <tr>
                                <th>area</th>
                                <td>{{ $patient['area'] }}</td>
                            </tr>
                            <tr>
                                <th>Last Online</th>
                                <td>{{ $patient['last_online'] }}</td>
                            </tr>
                        </table>
                    </div>
                </div>                                    
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h2>Patient History</h2>
                    </div>                                   
                </div>                                
                <div class="panel-body padding-0">
                        @if(empty($patient['history']) || $patient['history']->isEmpty())
                        <div class="emedic-admin-activity">
                            <div class="emedic-admin-no-result">
                                <h3>No Patient History</h3>
                            </div>
                        @else

                        <div class="emedic-admin-activity-full">
                            <table class="emedic-activity-table w-100">
                                <thead>
                                    <tr>
                                        <th>Report ID</th>
                                        <th>Last Updated</th>
                                        <th>Generated At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($patient['history'] as $key => $report)
                                    <tr>
                                        <td>Report {{ $key }}</td>
                                        <td>{{ $report->updated_at->format('dS F, Y h:i a') }}</td>
                                        <td>{{ $report->created_at->format('dS F, Y h:i a') }}</td>
                                        <td><a href="{{ url('/dashboard/doctor/report/'.$report['id']) }}">View</a> | <a href="{{ url('/dashboard/doctor/report/'.$report['id'].'/generate') }}">Download</a></td>
                                     </tr>
                                 @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
@stop