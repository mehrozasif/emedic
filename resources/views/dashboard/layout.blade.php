<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>@yield('title')</title>          
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf_token_ajax" content="{{ csrf_token() }}">
        
        @section('headmeta') @show
        
        <link rel="shortcut icon" href="{{asset('images/logo.png')}}" type="image/x-icon">
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/admin/main.css') }}"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        @section('styles') @show


        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            @section('sidebar') @show
            <div class="page-content">
                @section('topbar') @show
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="{{ url('/dashboard') }}">Home</a></li>                    
                    @section('breadcrumb') @show
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    @section('content') @show
                </div>
            </div>
        </div>

        @section('footer') @show
        <!-- END PAGE CONTAINER --> 
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{asset('audio/alert.mp3')}}" preload="auto"></audio>
        <audio id="audio-fail" src="{{asset('audio/fail.mp3')}}" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS --> 

        <script type='text/javascript' src="{{ asset('js/admin/popper.min.js') }}"></script> 
        <script type='text/javascript' src="{{ asset('js/admin/tooltip.min.js') }}"></script>
        
        <script src="{{asset('js/lib/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/lib/jquery-ui.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/lib/bootstrap.min.js')}}" type="text/javascript"></script>

        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        @section('scripts') @show
        <script type='text/javascript' src='{{ asset('js/admin/main.js') }}'></script>  
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






