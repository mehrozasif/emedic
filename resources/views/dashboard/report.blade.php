<!DOCTYPE html>
<html lang="en" style="margin: 0;background-color: #fff;">
    <head>        
        <!-- META SECTION -->
        <title>{{ $data['patient_name'] }} Report</title>          
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />     
    </head>
    <body style="background-color:#000;margin: 0;">
        <div style="max-width:800px;position: relative;background-color: white;margin-left:auto;margin-right:auto;padding: 5px 10px;">
            <div style="width: 100%;display: block;border-bottom: 1px solid darkblue;margin-bottom: 10px;color: darkblue;">
                <h1 style="margin:0;">E-Medic</h1>
                <h2 style="margin:0;text-indent: 50px;line-height: 0.2;margin-bottom: 10px;font-size: 15px;margin-top: 5px;">Digitizing your world</h2>
            </div>
            <div style="width: 778px;position: relative;padding: 0 10px 10px;display: flex;margin-bottom: 10px;">
                <div style="width: 100%;position: relative;">
                    <table style="width:100%;position: relative;">
                        <tr style="text-align: left">
                            <th>Patient Name</th>
                            <td>{{ $data['patient_name'] }}</td>
                            <td colspan="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <th>Case#</th>
                            <td>0002412</td>
                        </tr>
                        <tr style="text-align: left">
                            <th>Patient Age </th>
                            <td>{{ $data['patient_age'] }}</td>
                            <td colspan="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <th>Collected At</th>
                            <td>Not Collected</td>
                        </tr>
                        <tr style="text-align: left">
                            <th>Department </th>
                            <td>{{ $data['report_department'] }}</td>
                            <td colspan="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <th>Created At</th>
                            <td>{{ $data['report_created'] }}</td>
                        </tr>
                        <tr style="text-align: left">
                            <th>Doctor </th>
                            <td>{{ $data['doctor_name'] }}</td>
                            <td colspan="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <th>Last Updated At</th>
                            <td>{{ $data['report_updated'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 778px;position: relative;padding: 0 10px;border-bottom:1px solid darkblue">
                <h2>Medical Report</h2>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <h3>Intial Dignosis:</h3>
                <p style="text-indent: 125px">{{ $data['report_initial_dignosis'] }}</p>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <h3>Final Dignosis:</h3>
                <p style="text-indent: 125px">{{ $data['report_final_dignosis'] }}</p>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <p><strong>Disease: </strong> {{ $data['report_disease'] }}</p>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <p><strong>Type of Disease: </strong> {{ $data['report_tdisease'] }}</p>
            </div>
            <div style="page-break-after: always;"></div>
            <div style="width: 778px;position: relative;padding: 0 10px;border-bottom:1px solid darkblue">
                <h2>Examination</h2>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <p>{{ $data['report_examination'] }}</p>
            </div>
            <div style="width: 778px;position: relative;padding: 0 10px;border-bottom:1px solid darkblue">
                <h2>Treatment (Medicine)</h2>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <p>{{ $data['report_treatment'] }}</p>
            </div>
            <div style="width: 778px;position: relative;padding: 0 10px;border-bottom:1px solid darkblue">
                <h2>Patient History</h2>
            </div>
            <div style="width: 770px;position: relative;padding: 0 15px;margin-bottom: 8px;">
                <p>No History Available</p>
            </div>
        </div>
    </body>
</html>