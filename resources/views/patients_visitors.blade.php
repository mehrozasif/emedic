@extends('layout')

@section('title','Patients & Visitors - E-Medic')

@section('content')

<div class="patients-visitors-container bg-pattern">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-12 mb-3">
				<h2>Patient & Visitor</h2>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="carouselExampleControls" class="carousel slide mt-5 mb-5" data-ride="carousel">
						  <div class="carousel-inner">
							    <div class="carousel-item active">
							      <img class="d-block w-100" src="{{ asset('/images/hospital-safety3.jpg') }}" alt="First slide">
							    </div>
							    <div class="carousel-item">
							      <img class="d-block w-100" src="{{ asset('/images/Beacon-Hospital-large-1024x607.jpg') }}" alt="Second slide">
							    </div>
							    <div class="carousel-item">
							      <img class="d-block w-100" src="{{ asset('/images/image.jpg') }}" alt="Third slide">
							    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    	<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
						    	<span class="sr-only">Next</span>
						  </a>	
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-6 mb-5">
					<div class="patients-container">
        				<img src="{{asset('images/icon-patients-green.svg')}}" />
				        <h5 class="mt-2">Want to check up?</h5>
				        <a href="{{ url('search-doctor') }}" /><h3>Request for Appointment</h3></a>
				    </div>	
				</div>
				<div class="col-md-6 mb-5">
					<div class="patients-container">
        				<img src="{{asset('images/icon-patients-green.svg')}}" />
				        <h5 class="mt-2">Have A query?</h5>
				        <a href="{{ route('contact') }}" /><h3>Contact Us</h3></a>
				    </div>	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection