<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="csrf_token" content="{{ csrf_token() }}"> <!-- FOR AJAX -->
        <meta name="csrf_token_ajax" content="{{ csrf_token() }}"> <!-- FOR AJAX -->
        @section('headmeta') @show

        <title>@yield('title')</title>

        <link rel="shortcut icon" href="{{ asset('images/logo.png')}}" type="image/x-icon">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,800" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/lib/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/main.css')}}" rel="stylesheet" type="text/css" />
        @section('styles') @show

        <!-- *Styles -->

	</head>
	<body class="emedic-body @yield('body-classes')">
		@include('partials.main_nav')
		<div class="emedic-content">
			@section('content') @show
		</div>
		@include('partials.footer')
		<!-- Scripts -->
		<script src="{{asset('js/lib/jquery.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/lib/bootstrap.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/main.js')}}" type="text/javascript" ></script>
		@section('scripts') @show

		<!-- *Scripts -->
	</body>
</html>