@extends('layout')

@section('title','Doctor Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-3 mt-5">
					@include('partials.profile_nav', ['active_messages'=>TRUE])
				</div>
				<div class="col-md-8 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Messages</h2>
						</div>
						<div class="container-fluid">
							<div class="chat-view-messages">
		                    	<div class="row">
		                    		<div class="col-md-12">
		                    			<div class="chat-user-list">
		                    				<div class="chat-user-list-view">
		                    					@if(empty($chatlist))
		                    						<div class="row pt-3 pb-3">
		                    							<h2 class="text-center mt-3 mb-3">No new Messages</h2>
		                    						</div>
		                    					@endif
		                    					@foreach($chatlist as $chat)
		                    						<div class="row chat-user" title="Click to view" onclick="window.location.href= '{{ url('profile/messages/'.$chat['otheruser']->name) }}'">
		                    							<div class="col-md-12">
		                    								<div class="d-flex just-between">
		                    									<h3>{{ $chat['otheruser']->display_name }}</h3>
		                    									<div class="chat-user-action">View messages</div>
		                    								</div>
		                    							</div>
		                    						</div>
		                    					@endforeach
		                    				</div>
		                    			</div>
		                    		</div>
		                    	</div>
		                    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection