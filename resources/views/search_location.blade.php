@extends('layout')

@section('title','Find a Location - E-Medic')

@section('content')

<div class="search-location-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center">Find a Location</h2>
			</div>
		</div>
		<div class="row mt-5 mb-5">
			<div class="col-md-12">
				<div class="search-location-form-container">
					<form action="{{url('search-location')}}" method="post">
						<i class="fa fa-search"></i>
						<input type="text" name="search-location" required placeholder="Search Name, Location, Speciality" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection