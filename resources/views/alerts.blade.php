@extends('layout')

@section('title','Doctor Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-name w-85">
						<h1>Alerts</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row bb-1">
				<div class="col-md-4 ml-auto mr-auto mt-3 mb-3">
					<h3 class="text-center  red">Maximum area Affected</h3>
					<div class="alert-slide">
						<h1 class="alert-elem red">{{ empty($alertsMax) ? '':$alertsMax->affected_no }}</h1>
						<h1 class="alert-elem red">{{ empty($alertsMax) ? '':$alertsMax->area }}</h1>
						<h1 class="alert-elem red">{{ empty($alertsMax) ? '':$alertsMax->disease }}</h1>
					</div>
				</div>
				<div class="col-md-4 ml-auto mr-auto mt-3 mb-3">
					<h3 class="text-center blue">Alerts</h3>
					<div class="alert-slide">
						<h1 class="alert-elem counter blue"></h1>
						<h1 class="alert-elem area blue"></h1>
						<h1 class="alert-elem name blue"></h1>
					</div>
				</div>
				<div class="col-md-4  ml-auto mr-auto mt-3 mb-3">
					<h3 class="text-center green">Minimum area Affected</h3>
					<div class="alert-slide">
						<h1 class="alert-elem green">{{ empty($alertsMin) ? '':$alertsMin->affected_no }}</h1>
						<h1 class="alert-elem green">{{ empty($alertsMin) ? '':$alertsMin->area }}</h1>
						<h1 class="alert-elem green">{{ empty($alertsMin) ? '':$alertsMin->disease }}</h1>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-12">
					<table border="1" class="w-100 mt-2 mb-5">
						<thead>
							<tr>
								<th class="p-1 text-center bg-blue">Disease</th>
								<th class="p-1 text-center bg-blue">Area</th>
								<th class="p-1 text-center bg-blue">Affected Patients.</th>
							</tr>
						</thead>
						<tbody>
							@foreach($alerts as $alert)
								<tr>
									<td class="p-2">{{ $alert->disease }}</td>
									<td class="p-2">{{ $alert->area }}</td>
									<td class="p-2 red redglow">{{ $alert->affected_no }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

<script src="{{ asset('js/lib/bubbletext.js') }}"></script>

@php
	$jsonArray = json_encode($alerts);
@endphp

<script type="text/javascript">
	var alerts = @php echo $jsonArray; @endphp;
	var index = 0;
	(function loopAnimation() {
		var $element = $('#bubble');

        startCounter($('.alert-slide .counter'),alerts[index].affected_no);
        bubbleText({
            element: $('.alert-slide .area'),
            newText: ''+alerts[index].area,
            letterSpeed: 30,
            callback: function() {
                setTimeout(loopAnimation, 15000);
            },
        });
        bubbleText({
            element: $('.alert-slide .name'),
            newText: ''+alerts[index].disease,
            letterSpeed: 25,
            callback: function() {
                setTimeout(loopAnimation, 15000);
            },
        });
        index++;
        if(index >= alerts.length){
        	index = 0;
        }
    })();

    function startCounter(elem, num){
    	let i = 0;
    	elem.text(i);
    	var intv = setInterval(function(){
    		elem.text(i);
    		i++;
    		if(i > num){
    			clearInterval(intv);
    		}
    	},50);
    }
	
</script>

@endsection