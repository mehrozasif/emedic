@extends('layout')

@section('title','Reports - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-3 mt-5">
					@include('partials.profile_nav', ['active_reports'=>TRUE])
				</div>
				<div class="col-md-8 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Reports</h2>
						</div>
						<div class="container-fluid">
							@foreach($data as $d)
							<div class="row mt-5 mb-5 bb-1">
								<div class="col-md-12">
									<h4>Report Details</h4>
								</div>
								<div class="col-md-12">
									<table class="w-100 p-relative">
										<tr>
											<th class="w-50">Doctor</th>
											<td>{{ $d['user']->display_name }}</td>
										</tr>
										<tr>
											<th class="w-50">Time</th>
											<td>{{ $d['report']->updated_at->format('jS F Y, h:i a') }}</td>
										</tr>
										<tr>
											<th class="w-50">Price</th>
											<td>{{ "Rs.".$d['report']->amount.".00"}}</td>
										</tr>
										<tr>
											<th class="w-50">Action</th>
											<td>
												@if($d['report']->paid_status == 'paid')
												<a href="{{ url('/report/'.$d['report']->id) }}">View</a> 
												| <a href="{{ url('/report/'.$d['report']->id.'/generate') }}">Download</a>
													@else
												 Not Paid
												@endif
											</td>
										</tr>
									</table>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection