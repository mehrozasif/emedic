@extends('layout')

@section('title','Health Posts - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-12 mb-3">
				<h2>Doctor Health Posts</h2>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					@foreach($posts as $key => $post)
						<div class="blog-post">
							<div class="row">
								<div class="col-md-12">
									<div class="blog-title">
										<h2><a href="{{ url('/post/'.$post->slug) }}">{{ $post->title }}</a></h2>
									</div>
								</div> 
								<div class="col-md-12">
									<div class="post-meta">
										<div class="post-author">
											<span>By <a href="{{ url('doctor/'.$doctors[$key]->name) }}">{{ $doctors[$key]->display_name }}</a> | {{ $post->created_at->format('F j, Y') }}</span>
										</div>
									</div>
								</div> 
								@if(!empty($post->featured_image))
								<div class="col-md-4 pt-3 pb-3">
									<div class="blog-image">
										<img class="img-thumbnail" src="{{ asset($post->featured_image) }}" width="100%" />
									</div>
								</div>
								<div class="col-md-8 pt-3 pb-3">
								@else
								<div class="col-md-12 pt-3 pb-3">
								@endif
									<div class="blog-content">
										{{ Str::words(strip_tags($post->body),100,'...') }} <a href="{{ url('post/'.$post->slug) }}">View more</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="col-md-4">
					@include('blog_sidebar')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection