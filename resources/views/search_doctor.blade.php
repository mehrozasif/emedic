@extends('layout')

@section('title','Find a Doctor - E-Medic')

@section('content')

<div class="search-doctor-container bg-pattern">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center">Find a Doctor</h2>
			</div>
		</div>
		<div class="row mt-5 mb-5">
			<div class="col-md-12">
				<p class="text-center">Our find a doctor tool assists you in choosing from our diverse pool of health specialists. Discover better health & wellness by using our doctor ratings & reviews to make your choice.</p>
				<div class="search-doctor-form-container">
					<form id="search-doctor-form" action="{{url('search-doctor')}}" method="post">
						<i class="fa fa-search"></i>
						<input type="text" class="search_doctor_inputs" name="search-doctor" required placeholder="Search Name, Location, Speciality" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row mt-5">
		<div class="col-md-4 br-1 mb-5">
			@include('partials.filters_search_doctors')
		</div>
		<div class="col-md-8 search-doctor-results-container">
			<div class="row bb-1">
				<div class="col-md-12">
					<h4 class="text-center pb-4"><span class="search-doctor-results-count">{{ count($doctors) }}</span> -- Doctor(s)</h4>
				</div>
			</div>
			<div class="search-doctor-results mb-5">
				<!-- LOOP -->

				@foreach($doctors as $key => $doctor)
				<div class="row pt-5 pb-5 bb-1">
					<div class="col-md-12">
						<div class="d-flex justify-between">
							<a class="blue-link" href="{{ url('/doctor/'.$users[$key]->name.'') }}"><h3>{{ $users[$key]->display_name}}</h3></a>
							<a class="blue-link" href="tel:{{ $users[$key]->phoneno }}">{{ $users[$key]->phoneno }}</a>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="search-doctor-left">
									<div class="search-doctor-picture mt-2">
										<img src="{{ ( (isset($doctor->image)) ? $doctor->image : asset('images/default-user.jpg') ) }}">
									</div>
									@if(!empty($ratings[$key]))

									@include('partials.rating',['ratingavg' => (($ratings[$key]['rating']*100) /5), 'rating' => $ratings[$key]['rating'],'ratingcount' => $ratings[$key]['rated'] , 'ratingcomments'=> $ratings[$key]['comments']])

									@else
									<div class="text-center">Not Rated</div>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="search-doctor-right">
									<div class="search-doctor-institute">
										<h5>Department</h5>
										<p>{{ $doctor->department }}</p>
									</div>
									<div class="search-doctor-location">
										<h5>Specialization</h5>
										<p>{{ $doctor->specialization }}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				<!-- LOOP -->

				


			</div>
		</div>
	</div>
</div>
@endsection