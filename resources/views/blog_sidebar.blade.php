<div class="widgets-area">
	<div class="widget">
		<div class="widget-title">
			Latest Health Posts
		</div>
		<div class="widget-content">
			@foreach($healthposts as $post)
				<li><a href="{{ $post->slug }}">{{ $post->title }}</a></li>
			@endforeach
		</div>
	</div>
</div>