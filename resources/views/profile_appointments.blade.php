@extends('layout')

@section('title','Appointments - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-3 mt-5">
					@include('partials.profile_nav', ['active_appointments'=>TRUE])
				</div>
				<div class="col-md-8 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Appointments</h2>
						</div>
						<div class="container-fluid">
							@foreach($app_data as $app)
							<div class="row mt-5 mb-5 bb-1">
								<div class="col-sm-2 col-xs-6">
									<h5>Doctor</h5>
								</div>
								<div class="col-sm-3 col-xs-6">
									<p>{{ $app['doctor']->display_name }}</p>
								</div>
								<div class="col-sm-2 col-xs-6">
									<h5>Time</h5>
								</div>
								<div class="col-sm-5 col-xs-6">
									<p>
									@if($app['missed'])
                                        {{ $app['schedule'] }} ( <span class="red">Not Taken</span> )
                                   	@elseif($app['taken'])
                                        {{ $app['schedule'] }} ( <span class="green">Taken</span> )
                                    @else
                                        {{ $app['schedule'] }}
                                    @endif
									</p>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection