@extends('layout')

@section('title','Find a Location - E-Medic')

@section('content')

<div class="search-container bg-pattern">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center">Search E-medic</h2>
			</div>
		</div>
		<div class="row mt-5 mb-5">
			<div class="col-md-12">
				<div class="search-form-container">
					<form action="{{url('search')}}" method="post">
						<i class="fa fa-search"></i>
						<input type="text" name="s" required placeholder="Search E-Medic" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection