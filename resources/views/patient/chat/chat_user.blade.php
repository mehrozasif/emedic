@extends('layout')

@section('title',$doctor->display_name.' Chat - E-Medic')

@section('headmeta') 
    <meta name="chat_receiver_id" content="{{ $doctor->id }}">
    <meta name="chat_sender_id" content="{{ $user->id }}">
    
    <meta name="chat_sender_id" content="{{ $user->id }}">
@stop

@section('scripts')

	<script src="https://js.pusher.com/4.3/pusher.min.js"></script>

    <script type='text/javascript' src="{{ asset('js/chat/main.js') }}"></script>

@stop

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-3 mt-5">
					@include('partials.profile_nav', ['active_messages'=>TRUE])
				</div>
				<div class="col-md-8 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>{{ $doctor->display_name }}</h2>
						</div>
						<div class="container-fluid">
							<div class="chat-view-messages">
		                    	<div class="row">
		                    		<div class="col-md-12">
		                    			<div class="chat-user-list">
		                    				<div class="chat-user-list-view">
		                    					@if(empty($chatlist))
		                    						<div class="row pt-3 pb-3 chat-no-messages">
		                    							<div class="col-md-12">
		                    								<h2 class="text-center mt-3 mb-3">No new Messages</h2>
		                    							</div>
		                    						</div>
		                    					@endif
		                    					@foreach($chatlist as $chat)
		                    						<div class="row" title="{{ $chat['chat']->created_at->format('jS F, Y h:i a') }}" >
		                    							<div class="col-md-12">
		                                                    <div class="chat-user-message{{ ( $chat['otheruser']->id == $chat['sender']->id) ? ' chat-text-left':' chat-text-right' }}">
		                        								<p>{{ $chat['chat']->message }}</p>
		                                                        <p><small>{{ $chat['chat']->created_at->format('jS F, Y h:i a') }}</small></p>
		                                                    </div>
		                    							</div>
		                    						</div>
		                    					@endforeach
		                                        <div class="clear"></div>
		                    				</div>
		                    			</div>
		                    		</div>
		                    	</div>
		                    </div>
		                    <div class="chat-form">
		                        <form action="#" class="chat-form" method="POST">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <div class="form-group">
		                                        <textarea class="form-control chat-message-input" placeholder="Enter your message..." ></textarea>
		                                    </div>
		                                </div>
		                                <div class="col-sm-2">
		                                    <div class="form-group">
		                                        <input type="submit" class="btn btn-primary w-100 h-100 chat-message-btn" value="Send" />
		                                    </div>
		                                </div>
		                            </div>
		                        </form>
		                    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection