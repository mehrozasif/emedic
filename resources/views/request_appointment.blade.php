@extends('layout')

@section('title','Request Appointment')

@section('content')

<div class="contact-container">
	<div class="container">
		<div class="row mt-5 mb-5">
			<div class="col-md-12 mb-5">
				<p>
					@if ( \Session::has('succ_msg'))
                    <span class="help-block green">
                    	<strong>{{ session('succ_msg') }}</strong>
                    </span>
                    <script>
                    	setTimeout(function(){
                    		window.location.href = window.location.origin;
                    	},3000);
                    </script>
                	@endif
                	
                	@if ( \Session::has('err_msg'))
                    <span class="help-block red">
                    	<strong>{{ session('err_msg') }}</strong>
                    </span>
                	@endif
            	</p>
				<h2 class="text-center">Request Appointment</h2>
			</div>
			<div class="col-md-8 mb-md-0 mb-5 mr-auto ml-auto">
	            <form id="contact-form" name="contact-form" action="{{ route('submit_appointment') }}" method="POST">
	            	{{ csrf_field() }}
	            	<input type="hidden" name="doctor_id" value="{{ $doctor_user->id }}" />
	            	<input type="hidden" name="patient_id" value="{{ $user->id }}" />
	            	<h2 class="mb-3 mt-3">Doctor Information</h2>
	                <!--Grid row-->
	                <div class="row">

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                        	<label for="name" class="">Doctor Name</label>
	                            <input type="text" value="{{ $doctor_user->display_name }}" readonly class="form-control">
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                            <label for="email" class="">Doctor Email</label>
	                            <input type="text" value="{{ $doctor_user->email }}" readonly class="form-control">
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                </div>
	                <!--Grid row-->


	            	<h2 class="mb-3 mt-3">Patient Personal Details</h2>


	                <!--Grid row-->
	                <div class="row">

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                        	<label for="name" class="">Your Name</label>
	                            <input type="text" value="{{ $user->display_name }}" readonly class="form-control">
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                            <label for="email" class="">Your Email</label>
	                            <input type="text" value="{{ $user->email }}" readonly class="form-control">
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                </div>
	                <!--Grid row-->

	                <div class="row">
	                	<div class="col-md-6">
                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">

                                <label for="gender" class="control-label">Gender:</label><br />
                                <select class="form-control" readonly name="gender">
                                    <option selected disabled>Select your Gender</option>
                                    <option value="A+" {{ ($user->gender == 'male') ? 'selected':'' }}>Male</option>
                                    <option value="A-" {{ ($user->gender == 'female') ? 'selected':'' }}>Female</option>
                                </select>
                                 @if ($errors->has('gender'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                                <label for="blood_group" class="control-label">Gender:</label><br />
                                <select class="form-control" readonly name="blood_group">
                                    <option selected disabled>Select your Blood Group</option>
                                    <option value="A+" {{ ($patient_user->blood_group == 'A+') ? 'selected':'' }}>A+</option>
                                    <option value="A-" {{ ($patient_user->blood_group == 'A-') ? 'selected':'' }}>A-</option>
                                    <option value="B+" {{ ($patient_user->blood_group == 'B+') ? 'selected':'' }}>B+</option>
                                    <option value="B-" {{ ($patient_user->blood_group == 'B-') ? 'selected':'' }}>B-</option>
                                    <option value="O+" {{ ($patient_user->blood_group == 'O+') ? 'selected':'' }}>O+</option>
                                    <option value="O-" {{ ($patient_user->blood_group == 'O-') ? 'selected':'' }}>O-</option>
                                    <option value="AB+" {{ ($patient_user->blood_group == 'AB+') ? 'selected':'' }}>AB+</option>
                                    <option value="AB-" {{ ($patient_user->blood_group == 'AB-') ? 'selected':'' }}>AB-</option>
                                </select>
                                 @if ($errors->has('blood_group'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('blood_group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
	                </div>

	                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">Address</label>
                                <input type="text" name="address" id="address" class="form-control" placeholder="Enter your Address" readonly value="{{ $user->address }}" required />
                                 @if ($errors->has('address'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                                <label for="area" class="control-label">Area</label>
                                <input type="text" name="area" id="area" class="form-control" placeholder="Enter your Area" value="{{ $patient_user->area }}" readonly required />
                                 @if ($errors->has('area'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

	                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <input type="date" name="dob" id="dob" readonly class="form-control" value="{{ $user->dob }}" required />
                                 @if ($errors->has('dob'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('phoneno') ? ' has-error' : '' }}">
                                <label for="phoneno" class="control-label">Phone No.</label>
                                <input type="tel" name="phoneno" id="phoneno" class="form-control" value="{{ $user->phoneno }}" placeholder="Enter your Phone No." readonly required />
                                 @if ($errors->has('phoneno'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('phoneno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('symtoms') ? ' has-error' : '' }}">
                                <label for="symtoms" class="control-label">Symtoms</label>
                                <textarea type="text" name="symtoms" id="symtoms" class="form-control" placeholder="Enter your Symtoms" readonly required >{{ $patient_user->symtoms }}</textarea>
                                 @if ($errors->has('symtoms'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('symtoms') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('allergies') ? ' has-error' : '' }}">
                                <label for="allergies" class="control-label">Allergies</label>
                                <textarea type="text" name="allergies" id="allergies" class="form-control"placeholder="Enter your Allergies" readonly required >{{ $patient_user->allergies }}</textarea>
                                 @if ($errors->has('allergies'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('allergies') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>


	            	<h2 class="mb-3 mt-3">Appointment Details</h2>
<!--	            	<div class="row">
	                	<div class="col-md-6">
                            <div class="form-group{{ $errors->has('history') ? ' has-error' : '' }}">

                                <label for="gender" class="control-label">Has the patient been seen at E-Medic in the past?</label><br />
                                <select class="form-control" name="gender">
                                    <option selected disabled>Already Treated at E-Medic?</option>
                                    <option value="yes" {{ ($hasreport == 'yes') ? 'selected':'' }}>Yes</option>
                                    <option value="no" {{ ($hasreport == 'no') ? 'selected':'' }}>No</option>
                                </select>
                                 @if ($errors->has('gender'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                <label for="reason" class="control-label">Reason for appointment</label>
                                <textarea type="text" rows="5" name="reason" id="reason" class="form-control" placeholder="Enter your reason for appointment" required ></textarea>
                                @if ($errors->has('reason'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('reason') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!--<div class="form-group{{ $errors->has('app_time') ? ' has-error' : '' }}">

                                <label for="app_time" class="control-label">Appointment Time:</label><br />
                                <select class="form-control" name="app_time" required>
                                    @if(empty($apps_time['monday']) && empty($apps_time['tuesday']) && empty($apps_time['wednesday']) && empty($apps_time['thursday']) && empty($apps_time['friday']) && empty($apps_time['saturday']) && empty($apps_time['sunday']) )
                                        <option disabled selected>No Appointment Time Available for {{ $doctor_user->display_name }}</option>
                                    @else
                                        <option disabled selected>Select Appointment time</option>
                                    @endif

                                    @foreach($apps_time['monday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach

                                    @foreach($apps_time['tuesday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                    @foreach($apps_time['wednesday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                    @foreach($apps_time['thursday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                    @foreach($apps_time['friday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                    @foreach($apps_time['saturday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                    @foreach($apps_time['sunday'] as $app_time)
                                        <option value="{{ $app_time->id }}">{{ $app_time->day }} ( {{ $app_time->time }} )</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('app_time'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('app_time') }}</strong>
                                    </span>
                                @endif
                            </div> -->

                            <div class="form-group{{ $errors->has('app_date') ? ' has-error' : '' }}">
                                <label for="app_date" class="control-label">Appointment Date</label>
                                <input type="date" name="app_date" id="app_date" class="form-control" placeholder="Enter the appointment date" required />
                                 @if ($errors->has('app_date'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('app_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('app_time') ? ' has-error' : '' }}">
                                <label for="app_time" class="control-label">Appointment Time</label>
                                <input type="time" name="app_time" id="app_time" class="form-control" placeholder="Enter the appointment time" required />
                                 @if ($errors->has('app_time'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('app_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('app_type') ? ' has-error' : '' }}">

                                <label for="app_type" class="control-label">Appointment Type:</label><br />
                                <select class="form-control" name="app_type" required>
                                    <option selected disabled>Select your Appointment Type</option>
                                    <option value="Emergency">Normal</option>
                                    <option value="Emergency">Emergency</option>
                                </select>
                                 @if ($errors->has('app_type'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('app_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>


	                <div class="row">

	                    <!--Grid column-->
	                    <div class="col-md-12">
	                    	<div class="text-center text-md-left">
				                <input type="submit" class="btn btn-primary" value="Send" />
				            </div>
	                    </div>
	                </div>
	                <!--Grid row-->
	            </form>

	        </div>
		</div>
	</div>
</div>
@endsection