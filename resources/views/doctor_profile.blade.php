@extends('layout')

@section('title',$user->display_name.' Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image img-thumbnail w-25">
						<img src="{{ ( (isset($doctor->image)) ? $doctor->image : asset('images/default-user.jpg') ) }}" width="100%" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
						<p><strong>Institute</strong> {{ $doctor->department }}</p>
						<p><strong>Grade</strong> {{ $doctor->grade }}</p>

						@if(!empty($ratings))
						@include('partials.rating',['ratingavg' => (($ratings['rating']*100) /5), 'rating' => $ratings['rating'],'ratingcount' => $ratings['rated'] , 'ratingcomments'=> $ratings['comments']])
						@else
						<div><p>Not Rated</p></div>

						@endif
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="profile-request-appointment">
					<h4 class="text-center mb-3">Call <a href="tel:{{ $user->phoneno }}">{{ $user->phoneno }}</a></h4>
					@if(!empty($appointment_link))
					<a class="text-center appointment-btn" href="{{ $appointment_link }}">Request for appointment</a>
					@endif
					@if(!empty($chatlink))
					<a class="text-center btn mt-2" href="{{ $chatlink }}">Send a message</a>
					@endif
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<!-- DETAILS -->
		<div class="row">
			<div class="col-md-12 mb-3">
				<div class="container">
					<div class="profile-about info-block">
						<h1 class="title">About</h1>
						<p class="desc">{{ $doctor->bio }}</p>
					</div>
					<div class="profile-education info-block">
						@if(!empty($educations) && !$educations->isEmpty())
							<h1 class="title">Education</h1>
							@foreach($educations as $education)
								<div class="profile-education-block">
									<h5>{{ $education->title.' - '.$education->institute }}</h5>
									<p>{{ $education->location }}</p>
									<p>{{ $education->year }}</p>
								</div>
							@endforeach
						@endif
					</div>
					<div class="profile-certification info-block">
						@if(!empty($certifications))
							<h1 class="title">Certifications</h1>
							<ul>
							@foreach($certifications as $certification)
								<li>{{ $certification }}</li>
							@endforeach
							</ul>
						@endif
					</div>
					<div class="profile-certification info-block">
						@if(!empty($awards))
							<h1 class="title">Awards & Honors</h1>
							<ul>
							@foreach($awards as $award)
								<li>{{ $award }}</li>
							@endforeach
							</ul>
						@endif
					</div>

					<div class="profile-schedule info-block">
						<h1 class="title">Schedule</h1>
						<table border="1" class="w-100">
							<tr>
								<th class="text-center">Day</th>
								<th colspan="{{ $maxschedulecols }}" class="text-center">Time</th>
							</tr>
							@foreach($schedules as $key => $schedule)
								@php ($i = 0)
								<tr>
									<th>{{ ucfirst($key) }}</th>
									@foreach($schedule as $s)
										<td class="text-center">{{ $s->time }}</td>
										@php ($i++)
									@endforeach

									@if(empty($schedule))
										<td class="text-center" colspan="{{ $maxschedulecols }}"> No Schedule on {{ ucfirst($key) }}</td>
									@else
										@if( $i < $maxschedulecols )
											@php ( $r = $maxschedulecols - $i )
											@for($j=$r;$j>0;$j--)
												<td class="text-center"></td>
											@endfor
										@endif
									@endif


								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- DETAILS -->
	</div>
</div>
@endsection