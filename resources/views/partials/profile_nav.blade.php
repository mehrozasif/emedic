<div class="profile-nav-left">
	<div class="profile-nav-btn @if(!empty($active_profile)) {{ 'active' }} @endif">
		<a href="{{ url('/profile') }}">Profile</a>
	</div>
	<div class="profile-nav-btn @if(!empty($active_messages)) {{ 'active' }} @endif">
		<a href="{{ url('/profile/messages') }}">Messages</a>
	</div>
	<div class="profile-nav-btn @if(!empty($active_notifications)) {{ 'active' }} @endif">
		<a href="{{ url('/profile/notifications') }}">Notifications</a>
	</div>
	<div class="profile-nav-btn @if(!empty($active_appointments)) {{ 'active' }} @endif">
		<a href="{{ url('/profile/appointments') }}">Appointments</a>
	</div>
	<div class="profile-nav-btn @if(!empty($active_doctors)) {{ 'active' }} @endif">
		<a href="{{ url('/profile/doctors') }}">Doctors</a>
	</div>
	<div class="profile-nav-btn @if(!empty($active_reports)) {{ 'active' }} @endif">
		<a href="{{ url('/profile/reports') }}">Reports</a>
	</div>
</div>