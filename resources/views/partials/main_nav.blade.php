<header class="header">
    <div class="row">
        <div class="col-md-12 text-right top-nav">
            <a href="#">+92 3216111123</a>
            <span> | </span>
            <a href="{{url('/')}}">Home</a>
            <a href="{{url('blog')}}">Blog</a>
            <a href="{{url('contact')}}">Contact Us</a>
            <a href="{{ url('search')}}">Search</a>
            @guest
                <a href="{{ route('login')}} ">Log-in</a>
                <a href="{{ route('register.patient.form')}} ">Register</a>
            @else
                @php
                    $chatcount = count(Auth::user()->getUnseenAllChat()); 
                    $chatmsghtml = '';
                @endphp

                @if($chatcount > 0)
                @php
                    $chatmsghtml = "<span class=\"green\" >( {$chatcount } new )</span>";
                @endphp
                @endif

                @if(Auth::user()->hasRole('patient'))
                    <a href="{{ route('profile.messages')}} ">Chat {!! $chatmsghtml !!}</a>
                @elseif(Auth::user()->hasRole('doctor'))
                    <a href="{{ route('dashboard.doctor.messages')}} ">Chat {!! $chatmsghtml !!}</a>
                @elseif(Auth::user()->hasRole('admin'))
                    <a href="{{ route('dashboard.admin.messages')}} ">Chat {!! $chatmsghtml !!}</a>
                @endif
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <a href="{{ (Auth::user()->hasRole('doctor') || Auth::user()->isAdmin() ) ? route('dashboard.home'):route('profile')}} ">{{ Auth::user()->display_name }}</a>
            @endif
        </div>
    </div>
    <div class="row">
        <a href="{{url('/')}}">
            <div class="col-md-12 site-logo">
              <img src="{{asset('images/logo.png')}}" />
              <h4> E-Medic</h4>
            </div>
        </a>
    </div>
    <div class="row">
        <div class="col-md-12 bottom-nav">
            <a href="{{url('search-doctor')}}">Find a Doctor</a>
            <a href="{{url('patients-visitors')}}">Patients/Visitors</a>
            <a href="{{url('health-library')}}">Health Library</a>
            <a href="{{route('alerts')}}">Alerts</a>
        </div>
    </div>
</header>
