<div class="row mb-3">
				<div class="col-md-12">
					<a class="blue-link search_clear_filters" href="#">Clear all filters</a>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-md-12">
					<h5>By Specialization</h5>
					<select class="form-control search_doctor_inputs" name="search_doctor_specialization" onmousedown="if(this.options.length>8){this.size=8;}"  onchange='this.size=0;' onblur="this.size=0;">
						<option disabled selected>Filter by Specialization</option>
						@foreach($specializations as $specialization)
							<option value="{{ $specialization }}">{{ ucfirst($specialization) }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-md-12">
					<h5>By Grade</h5>
					<select class="form-control search_doctor_inputs" name="search_doctor_grade" onmousedown="if(this.options.length>8){this.size=8;}"  onchange='this.size=0;' onblur="this.size=0;">
						<option value="" disabled selected>Filter by Grade</option>
						@foreach($grades as $grade)
							<option value="{{ $grade }}">{{ ucfirst($grade) }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<!--<div class="row mb-5">
				<div class="col-md-12">
					<h5>By Rating</h5>
					<select class="form-control">
						<option value="" disabled selected>Filter by Rating</option>
						<option value="rating_1">Rating 1</option>
						<option value="rating_2">Rating 2</option>
						<option value="rating_3">Rating 3</option>
						<option value="rating_4">Rating 4</option>
						<option value="rating_5">Rating 5</option>
					</select>
				</div>
			</div> -->
			<div class="row mb-5">
				<div class="col-md-12">
					<h5>By Gender</h5>
					<select name="search_doctor_gender" class="form-control search_doctor_inputs">
						<option value="" disabled selected>Filter by Gender</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
					</select>
				</div>
			</div>