<div class="search-doctor-rating-container">
	<div class="search-doctor-rating">
		<div class="search-doctor-rating-stars">
			<i class="rating-empty" >
				<span class="rating-full" style="width: {{ $ratingavg }}%;" ></span>
			</i>
		</div>
		<p class="search-doctor-rating_avg"> {{ $rating }} out of 5.0</p>
	</div>
	<div class="search-doctor-ratings_count">
		-- {{ $ratingcount }} Patient(s) Rated
	</div>
	<div class="searc-doctor-comments">
		-- {{ $ratingcomments }} Patient(s) Commented
	</div>
</div>