<!-- Social -->
<div class="front-social">
  <a href="//facebook.com"><i class="fa fa-facebook-f"></i></a>
  <a href="//linkedin.com"><i class="fa fa-linkedin"></i></a>
  <a href="//twitter.com"><i class="fa fa-twitter"></i></a>
  <a href="//instagram.com"><i class="fa fa-instagram"></i></a>
</div>

<footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h4 class="bb-1 mb-2">E-Medic</h4>
            <a class="pl-1" href="{{ route('main') }}">Home</a>
            <a class="pl-1" href="{{ route('blog') }}">Health Library</a>
            <a class="pl-1" href="{{ route('contact') }}">Contact us</a>
            <a class="pl-1" href="{{ route('find_doctor_form') }}">Find A doctor</a>
        </div>
        <div class="col-md-4">
          <h4 class="bb-1 mb-2">Latest Health Posts</h4>
          @foreach(\App\Post::getLatest() as $post)
            <a class="pl-1" href="{{ url('/post/'.$post->slug) }}">{{ $post->title }}</a>
          @endforeach
        </div>
        <div class="col-md-4">
          <h4 class="bb-1 mb-2">Contact Details</h4>
            <p class="pl-1">Address:Superior University Gold Campus 6km Raiwind Rd, OPF Society, Lahore, Punjab</p>
            <p class="pl-1">Phone No. <a class="d-inline" href="tel:+92326111123" >+92326111123</a></p>
            <p class="pl-1">Facebook: <a class="d-inline" href="https://facebook.com">https://facebook.com</a></p>
            <p class="pl-1">Twitter: <a class="d-inline" href="https://twitter.com">https://twitter.com</a></p>
            <p class="pl-1">LinkedIn: <a class="d-inline" href="https://linkedin.com">https://linkedin.com</a></p>
            <p class="pl-1">Instagram: <a class="d-inline" href="https://instragram.com">https://instragram.com</a></p>
        </div>
      </div>
    </div>
</footer>