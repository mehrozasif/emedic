@extends('layout')

@section('title','Contact - E-Medic')

@section('content')

<div class="contact-container">
	<div class="container">
		<div class="row mt-5 mb-5">
			<div class="col-md-12 mb-5">
				<h2 class="text-center">Contact Us</h2>
			</div>
			<div class="col-md-8 mb-md-0 mb-5 mr-auto ml-auto">
	            <form id="contact-form" name="contact-form" action="{{ route('contact.submit') }}" method="POST">
	            	{{ csrf_field() }}
                        @if( session('succ_msg') )
                        <div class="succeed-msg">
                           {{ session('succ_msg') }}
                        </div>
                        <script>
                            setTimeout(function(){
                                window.location.href = "{{ route('main') }}";
                            },2000);
                        </script>
                    @endif
	                <!--Grid row-->
	                <div class="row">

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                        	<label for="name" class="">Your name</label>
	                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}">
	                            @if ($errors->has('name'))
                                    <span class="help-block red">
                                         <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                    <!--Grid column-->
	                    <div class="col-md-6 mb-2">
	                        <div class="md-form mb-0">
	                            <label for="email" class="">Your email</label>
	                            <input type="text" id="email" name="email" class="form-control"  value="{{ old('email') }}">
	                            @if ($errors->has('email'))
                                    <span class="help-block red">
                                         <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
	                        </div>
	                    </div>
	                    <!--Grid column-->

	                </div>
	                <!--Grid row-->

	                <!--Grid row-->
	                <div class="row mb-2">
	                    <div class="col-md-12">
	                        <div class="md-form mb-0">
	                        	<label for="subject" class="">Subject</label>
	                            <input type="text" id="subject" name="subject" class="form-control"  value="{{ old('subject') }}">
	                            @if ($errors->has('subject'))
                                    <span class="help-block red">
                                         <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
	                        </div>
	                    </div>
	                </div>
	                <!--Grid row-->

	                <!--Grid row-->
	                <div class="row mb-5">

	                    <!--Grid column-->
	                    <div class="col-md-12">

	                        <div class="md-form">

	                            <label for="message">Your message</label>
	                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"> {{ old('message') }}</textarea>
	                            @if ($errors->has('message'))
                                    <span class="help-block red">
                                         <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
	                        </div>

	                    </div>
	                </div>
	                <!--Grid row-->
	                <div class="row">

	                    <!--Grid column-->
	                    <div class="col-md-12">
	                    	<div class="text-center text-md-left">
				                <input type="submit" class="btn btn-primary" value="Send" />
				            </div>
	                    </div>
	                </div>
	                <!--Grid row-->
	            </form>

	        </div>
		</div>
	</div>
</div>
@endsection