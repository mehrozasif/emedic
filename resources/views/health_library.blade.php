@extends('layout')

@section('title','Health Library - E-Medic')

@section('content')

<div class="search-container bg-pattern">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Health Library</h2>
				<p>Access thousands of health articles, videos and tools to help manage your health.</p>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row mt-5 mb-5 bb-1">
		<div class="col-md-12">
			<h3 class="text-center">Search By All Topics (A-Z)</h3>
			<div class="aplhabetic-letters-inputs mt-5 mb-5">
				<div class="aplhabetic-letter" id="aplhabetic-letter-a" data-id="a">A</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-b" data-id="b">B</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-c" data-id="c">C</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-d" data-id="d">D</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-e" data-id="e">E</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-f" data-id="f">F</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-g" data-id="g">G</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-h" data-id="h">H</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-i" data-id="i">I</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-j" data-id="j">J</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-k" data-id="k">K</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-l" data-id="l">L</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-m" data-id="m">M</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-n" data-id="n">N</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-o" data-id="o">O</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-p" data-id="p">P</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-q" data-id="q">Q</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-r" data-id="r">R</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-s" data-id="s">S</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-t" data-id="t">T</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-u" data-id="u">U</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-v" data-id="v">V</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-w" data-id="w">W</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-x" data-id="x">X</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-y" data-id="y">Y</div>
				<div class="aplhabetic-letter" id="aplhabetic-letter-z" data-id="z">Z</div>
			</div>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-md-12">
			<div class="health-library-results mt-5 mb-5">
				<div class="row health-library-results-row"></div>
			</div>
		</div>
	</div>
</div>
@endsection