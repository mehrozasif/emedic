@extends('layout')

@section('title',$user->display_name.' Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Personal Details</h2>
						</div>
						<div class="container-fluid">
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Display Name</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->display_name }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Email</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->email }}</p>
								</div>
							</div>
							<div class="row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Age</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->getAge() }}</p>
								</div>
							</div>
							<div class="row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Gender</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->gender }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Address</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->address }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Area</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $patientdata->area }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Phone No.</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $user->phoneno }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Blood Group</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $patientdata->blood_group }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Symtoms</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $patientdata->symtoms }}</p>
								</div>
							</div>
							<div class="row profile-field-row pt-4 pb-4 bb-1">
								<div class="col-md-6">
									<h5>Allergies</h5>
								</div>
								<div class="col-md-6">
									<p>{{ $patientdata->allergies }}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection