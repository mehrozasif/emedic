@extends('layout')

@section('title','Doctor Profile - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="container">

		<!-- HEADER -->
		<div class="row mb-5">
			<div class="col-md-8 mb-3">
				<div class="profile-user-info">
					<div class="profile-image p-relative img-thumbnail w-25">
						<div class="profile-edit-dp-btn"><i class="fa fa-edit"></i></div>
						<img src="{{ ( (isset($user->image)) ? $user->image : asset('images/default-user.jpg') ) }}" width="100%" />
						<input type="file" class="hidden" name="profile_image" accept="image/*" />
					</div>
					<div class="profile-name w-85">
						<h1>{{ $user->display_name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- HEADER -->
	</div>
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-3 mt-5">
					@include('partials.profile_nav', ['active_doctors'=>TRUE])
				</div>
				<div class="col-md-8 mb-3 mt-5">
					<div class="profile-right-content">
						<div class="profile-content-title">
							<h2>Doctors</h2>
						</div>
						<div class="container-fluid">
							@if(empty($doctors))
							<div class="row mt-3 mb-3">
								<div class="col-md-12">
									<h4 class="text-center">You don't have any Doctor.</h4>
								</div>
							</div>
							@endif
							@foreach($doctors as $doctor)
							<div class="row mt-5 mb-5 bb-1">
								<div class="col-sm-6 col-xs-6">
									<h4><a href="{{ route('profile.doctor', $doctor['user']->name) }}">{{ $doctor['user']->display_name }}</a></h4>
								</div>
								<div class="col-sm-6 col-xs-6">
									@if($doctor['rated'])
										<div class="search-doctor-rating">
											<div class="search-doctor-rating-stars">
												<i class="rating-empty" >
													<span class="rating-full" style="width: {{ ($doctor['rating']*100) /5 }}%;" ></span>
												</i>
											</div>
											<p class="search-doctor-rating_avg"> {{ $doctor['rating'] }} out of 5.0</p>
											<p>{{ $doctor['rated']->comment }}</p>
										</div>

									<form action="#" class="rate-doctor-form" method="POST">
										<div class="row">
											<div class="col-md-12">
												<label>Rate*</label>
												<div class="p-relative">
													<div class="search-doctor-rating-stars-form" title="Click to toggle rating editting">
														<i class="rating-empty" >
															<span class="rating-full" style="width: 100%;" ></span>
														</i>
													</div>
												</div>
												<p><span class="rate-num">5.00</span> out of 5.00</p>
											</div>
											<div class="col-md-12 mt-2">
												<label>Comment</label>
												<textarea rows="3" cols="1" class="form-control" name="comment" placeholder=" ( Optional ) maximum 80 words" minlength="15" maxlength="80"></textarea>
											</div>
											<div class="col-sm-6"></div>
											<div class="col-sm-6 mt-2 mb-2">
												<input type="hidden" name="doctor_id" value="{{ $doctor['user']->id }}" required />
												<input type="hidden" name="rating_doctor" value="5.00" required/>
												<input type="submit" class="form-control" value="Rate Doctor">
											</div>
										</div>
									</form>
									@endif
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection