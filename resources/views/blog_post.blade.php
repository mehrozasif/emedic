@extends('layout')

@section('title',$post->title.' - E-Medic')

@section('content')

<div class="profile-container bg-pattern pb-0">
	<div class="profile-content bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="blog-post">
						<div class="row">
							<div class="col-md-12">
								<div class="blog-title">
									<h2><a href="{{ url('/post/'.$post->slug) }}">{{ $post->title }}</a></h2>
								</div>
							</div> 
							<div class="col-md-12">
								<div class="post-meta">
									<div class="post-author">
										<span>By <a href="{{ url('doctor/'.$doctor->name) }}">{{ $doctor->display_name }}</a> | {{ $post->created_at->format('F j, Y') }}</span>
									</div>
								</div>
							</div> 
							@if(!empty($post->featured_image))
							<div class="col-md-12 pt-3 pb-3">
								<div class="blog-image">
									<img class="img-thumbnail" src="{{ asset($post->featured_image) }}" width="100%" />
								</div>
							</div>
							@endif
							<div class="col-md-12 pt-3 pb-3">
								<div class="blog-content">
									@php echo $post->body @endphp
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					@include('blog_sidebar')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection