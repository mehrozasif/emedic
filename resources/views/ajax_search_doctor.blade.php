<div class="row pt-5 pb-5 bb-1">
	<div class="col-md-12">
		<div class="d-flex justify-between">
			<a class="blue-link" href="{{ url('/doctor/'.$doctordata['user']->name.'') }}"><h3>{{ $doctordata['user']->display_name}}</h3></a>
			<a class="blue-link" href="tel:{{ $doctordata['user']->phoneno }}">{{ $doctordata['user']->phoneno }}</a>
		</div>
	</div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="search-doctor-left">
					<div class="search-doctor-picture mt-2">
						<img src="{{ ( (isset($doctordata['user']->image)) ? $doctordata['user']->image : asset('images/default-user.jpg') ) }}">
					</div>
					@if(!empty($ratings))
					@include('partials.rating',['ratingavg' => (($ratings['rating']*100) /5), 'rating' => $ratings['rating'],'ratingcount' => $ratings['rated'] , 'ratingcomments'=> $ratings['comments']])
					@else
						<div class="text-center"><p>Not Rated</p></div>

					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="search-doctor-right">
					<div class="search-doctor-institute">
						<h5>Department</h5>
						<p>{{ $doctordata['doctor']->department }}</p>
					</div>
					<div class="search-doctor-location">
						<h5>Specialization</h5>
						<p>{{ $doctordata['doctor']->specialization }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>