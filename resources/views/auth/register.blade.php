@extends('layout')

@section('title','Register - E-Medic')


@section('content')

<div class="register-container bg-pattern">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                
            </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-6 p-5">
                <h2 class="text-center mb-5">Register As Patient</h2>
                <div class="text-center img-thumbnail m-auto w-60 hover-scale">
                    <a href="{{ url('register_patient') }}">
                        <img src="{{asset('images/default-user.jpg')}}" width="100%" />
                    </a>
                </div>
            </div>
            <div class="col-md-6 p-5">
                <h2 class="text-center mb-5">Register As Doctor</h2>
                <div class="text-center img-thumbnail m-auto w-60 hover-scale">
                    <a href="{{ url('register_doctor') }}">
                        <img src="{{asset('images/default-user-doctor.jpg')}}" width="100%" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
