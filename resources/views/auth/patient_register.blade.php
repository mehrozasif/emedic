@extends('layout')

@section('title','Register As Patient - E-Medic')


@section('content')


<div class="register-container">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <h2 class="text-center">Register As Patient</h2>
            </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-6 m-auto">
                <form method="POST" action="{{ route('emedic.register.patient') }}">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="control-label">First Name</label>
                                <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') }}" placeholder="Enter your First Name" required />
                                 @if ($errors->has('first_name'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="control-label">Last Name</label>
                                <input type="text" name="last_name" id="last_name" class="form-control" value="{{ old('last_name') }}" placeholder="Enter your Last Name" required />
                                 @if ($errors->has('last_name'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                        <label for="display_name" class="control-label">Display Name</label>
                        <input type="text" name="display_name" id="display_name" class="form-control" value="{{ old('display_name') }}" placeholder="Enter your Display Name" required />
                         @if ($errors->has('display_name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('display_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                        <label for="father_name" class="control-label">Father Name</label>
                        <input type="text" name="father_name" id="father_name" class="form-control" value="{{ old('father_name') }}" placeholder="Enter your Father Name" required />
                         @if ($errors->has('father_name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('father_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Enter your email" required />
                         @if ($errors->has('email'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" required />
                         @if ($errors->has('password'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="control-label">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Enter your password again" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="control-label">Gender:</label><br />
                                <input type="radio" name="gender" id="gender" value="male" required /> Male 
                                <input type="radio" name="gender" id="gender" value="female" required /> Female 
                                 @if ($errors->has('gender'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                                <label for="blood_group" class="control-label">Bloog Group:</label><br />
                                <select class="form-control" name="blood_group">
                                    <option selected disabled>Select your Blood Group</option>
                                    <option value="unknown">Unknown</option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                </select>
                                 @if ($errors->has('blood_group'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('blood_group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label">Address</label>
                                <input type="text" name="address" id="address" class="form-control" value="{{ old('address') }}" placeholder="Enter your Address" required />
                                 @if ($errors->has('address'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                                <label for="area" class="control-label">Area</label>
                                <input type="text" name="area" id="area" class="form-control" value="{{ old('area') }}" placeholder="Enter your Area" required />
                                 @if ($errors->has('area'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <input type="date" name="dob" id="dob" class="form-control" value="{{ old('dob') }}" required />
                                 @if ($errors->has('dob'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('phoneno') ? ' has-error' : '' }}">
                                <label for="phoneno" class="control-label">Phone No.</label>
                                <input type="tel" name="phoneno" id="phoneno" class="form-control" value="{{ old('phoneno') }}" placeholder="Enter your Phone No." required min="11" />
                                 @if ($errors->has('phoneno'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('phoneno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('symtoms') ? ' has-error' : '' }}">
                                <label for="symtoms" class="control-label">Symtoms</label>
                                <textarea type="text" name="symtoms" id="symtoms" class="form-control" value="{{ old('symtoms') }}" placeholder="Enter your Symtoms" ></textarea>
                                 @if ($errors->has('symtoms'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('symtoms') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('allergies') ? ' has-error' : '' }}">
                                <label for="allergies" class="control-label">Allergies</label>
                                <textarea type="text" name="allergies" id="allergies" class="form-control" value="{{ old('allergies') }}" placeholder="Enter your Allergies" ></textarea>
                                 @if ($errors->has('allergies'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('allergies') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    
                    <div class="form-group text-center{{ $errors->has('history') ? ' has-error' : '' }}">
                        <label for="history" class="control-label">Are you already a patient at Emedic?</label><br />
                        <input type="radio" name="history" id="history" value="yes" required /> Yes 
                        <input type="radio" name="history" id="history" value="no" required /> No 
                        @if ($errors->has('history'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('history') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group w-25 ml-auto">
                        <input type="submit" id="submit" class="form-control btn btn-primary" value="Register" />
                        {{ csrf_field() }}
                        <input type="hidden" id="user_type" name="user_type" value="patient" />
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection
