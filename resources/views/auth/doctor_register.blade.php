@extends('layout')

@section('title','Register As Doctor - E-Medic')


@section('content')


<div class="register-container">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <h2 class="text-center">Register As Doctor</h2>
            </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-6 m-auto">
                <form method="POST" action="{{ route('emedic.register.doctor') }}">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Enter your name" required />
                         @if ($errors->has('name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Enter your email" required />
                         @if ($errors->has('email'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" required />
                         @if ($errors->has('password'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="control-label">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Enter your password again" required>
                    </div>

                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <label for="gender" class="control-label">Gender:</label><br />
                        <input type="radio" name="gender" id="gender" value="male" required /> Male 
                        <input type="radio" name="gender" id="gender" value="female" required /> Female 
                         @if ($errors->has('gender'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group w-25 ml-auto">
                        <input type="submit" id="submit" class="form-control btn btn-primary" value="Register" />
                        {{ csrf_field() }}
                        <input type="hidden" id="user_type" name="user_type" value="doctor" />
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection
