@extends('layout')

@section('title','Login - E-Medic')

@section('content')

    <div class="login-container">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12">
                    <h2 class="text-center">Login</h2>

                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-md-6 m-auto">
                    <form method="POST" action="{{ route('emedic.login') }}">
                        <input type="hidden" name="redirect" value="{{ url()->previous() }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Enter your email" required />
                            @if ($errors->has('email'))
                                <span class="help-block red">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" required />
                            @if ($errors->has('password'))
                                <span class="help-block red">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <a href="{{url('password/reset')}}">forgot password</a>                                </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ \Session::has('msg') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-4">
                                @if ( \Session::has('err_msg'))
                                    <span class="help-block red">
                                    <strong>{{ session('err_msg') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group w-25 ml-auto">
                            <input type="submit" id="submit" class="form-control btn btn-primary" value="Sign in" />
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection