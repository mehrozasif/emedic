
@extends('layout')

@section('title','E-Medic')

@section('content') <!-- start section -->
<!-- Banner -->
<div class="banner-container">
  <div class="site-banner">
    <img src="{{asset('images/banner.jpg')}}" />
  </div>
</div>

<!-- call to action -->
<div class="call-to-action">
  <div class="row call-to-action-row">
    <div class="col-md-8 col-sm-12 col-xs-12">
      <div class="row call-to-action-inner">
        <div class="col-md-6 call-to-action-block">
          <h2>Our Doctors</h2>
          <p>Choose by name, specialty, city and more.</p>
          <a href="{{url('search-doctor')}}" class="btn btn-default">Find a Doctor</a>
        </div>
        <div class="col-md-6 call-to-action-block">
          <h2>Appointment</h2>
          <p>Click in, walk in or call us today.</p>
          <a href="#" class="btn btn-default">Appointment & Access</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Services -->
<div class="services">
  <div class="row services-block">
    <div class="col-md-6">
      <div class="patients-container">
        <img src="{{asset('images/icon-patients-green.svg')}}" />
        <h1>For Patients</h1>
        <h3>Featured Resources</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="doctors-container">
        <img src="{{asset('images/icon-for-doctors-green.svg')}}" />
        <h1>For Doctors</h1>
        <h3>Featured Resources</h3>
      </div>
    </div>
  </div>
</div>


<!-- contact -->
<div class="front-contact">
  <div class="front-contact-block">
    <h3>Get a Cancer Second Opinion from One of the Nation’s Best</h3>
    <h4>E-medic is here for you. Call anytime +92 3216111123</h4>
  </div>
</div>


<!-- Recent Blog -->
<div class="front-blog-container">
  <div class="container mt-5 mb-5">
    <div class="row mb-3">
      <div class="col-md-12">
        <h3>Latest Health Posts</h3>
      </div>
    </div>
    <div class="row">

      @foreach($lastestposts as $post) <!-- LOOP -->
        <div class="col-md-3">
          <div class="front-blog-block">
            <div class="blog-img">
              @if(!empty($post->featured_image))
              <img src="{{asset($post->featured_image)}}" />
              @else
              <img src="{{asset('images/default-image.png')}}" />
              @endif
            </div>
            <div class="blog-title">
              <h5><a href="{{ url('post/'.$post->slug) }}" class="blue-title-link">{{ $post->title }}</a></h5>
            </div>
          </div>
        </div>
      @endforeach

      
    </div>
  </div>
</div>

<!-- Secend contact -->
<div class="front-contact">
  <div class="front-contact-block">
    <h4>CONNECT WITH US ON SOCIAL MEDIA  </h4>
    <h5>Stay in touch and keep up-to-date with all our latest news, events and campaigns. You can do this by joining us on social media </h5>
  </div>
</div>


@endsection