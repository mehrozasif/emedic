<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call([
        	UsersTableSeeder::class,
        	PatientTableSeeder::class,
        	DoctorTableSeeder::class,
        ]);*/


        DB::table('users')->insert([
            'first_name' => 'Test First',
            'last_name' => 'Test Last',
            'display_name' => 'Admin',
            'name' => 'admin',
            'email' => 'test@test.com',
            'password' => bcrypt('test123'),
            'gender' => 'male',
            'address' => 'Test Address Test, Address, Address',
            'dob'=> '1975-01-01',
            'phoneno'=>'+123521535264',
            'type' => 'admin',
            'status' => '0',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Test First',
            'last_name' => 'Test Last',
            'display_name' => 'Test Doctor User',
            'name' => 'test-doctor-user',
            'email' => 'te@t.com',
            'password' => bcrypt('test123'),
            'gender' => 'male',
            'address' => 'Test Address Test1, Address, Address',
            'dob'=> '1980-01-01',
            'phoneno'=>'+123521534515',
            'type' => 'doctor',
            'status' => '0',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Test First',
            'last_name' => 'Test Last',
            'display_name' => 'Test Patient User',
            'name' => 'test-patient-user',
            'email' => 'te1@t.com',
            'password' => bcrypt('test123'),
            'gender' => 'male',
            'address' => 'Test Address Test122, Address, Address',
            'dob'=> '1985-01-01',
            'phoneno'=>'+123521524511',
            'type' => 'patient',
            'status' => '0',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);


        DB::table('patients')->insert([
            'user_id' => '3',
            'father_name' => 'Test Father',
            'blood_group' => 'B+',
            'symtoms' => 'Test Symtoms',
            'allergies' => 'Test Allergies',
            'area' => 'Test Area',
            'history' => '0',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);


        DB::table('doctors')->insert([
            'user_id' => '2',
            'grade' => '12',
            'department' => 'Test Dept.',
            'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id odio ex. Integer quis erat at quam luctus dapibus in eget sem. Praesent gravida posuere mollis. Etiam venenatis commodo interdum. Morbi imperdiet sapien et risus consequat, ac malesuada elit lacinia. Cras efficitur arcu eget rhoncus pulvinar. Etiam viverra odio eget elit tempus condimentum. Maecenas ut finibus magna.',
            'certifications' => 'test,test1,test2',
            'interests'=>'interest,interst1',
            'awards' => 'Award 1,Award 2',
            'specialization' => 'test',
            'patients' => '3,',
            'patients_treated' => '',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);


        
        DB::table('feedbacks_doctors')->insert([
            'to_id' => '2',
            'from_id' => '3',
            'rating' => '5.00',
            'comment' => 'test',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);

        DB::table('feedbacks_doctors')->insert([
            'to_id' => '2',
            'from_id' => '3',
            'rating' => '3.00',
            'comment' => '',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);

        DB::table('feedbacks_doctors')->insert([
            'to_id' => '2',
            'from_id' => '3',
            'rating' => '2.00',
            'comment' => '',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);




        DB::table('education')->insert([
            'user_id' => '2',
            'title' => 'Undergraduate',
            'degree' => 'Biology',
            'institute' => 'Princeton University',
            'location' => 'Princeton, NJ USA',
            'year' => '1987',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);
        DB::table('education')->insert([
            'user_id' => '2',
            'title' => 'Medical School',
            'degree' => '',
            'institute' => 'David Geffen School of Medicine @ UCLA',
            'location' => 'Los Angeles, CA USA',
            'year' => '1992',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);
        DB::table('education')->insert([
            'user_id' => '2',
            'title' => 'Residency',
            'degree' => 'Pediatrics',
            'institute' => 'University Hospitals of Test',
            'location' => 'Princeton, NJ USA',
            'year' => '1995',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);
        DB::table('education')->insert([
            'user_id' => '2',
            'title' => 'Fellowship',
            'degree' => 'Pediatrics',
            'institute' => 'University of Michigan Health System',
            'location' => 'Ann Arbor, MI USA',
            'year' => '2003',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);


        DB::table('reports')->insert([
            'doctor_id' => '2',
            'patient_id' => '3',
            'department' => 'Test Department',
            'initial_dignosis' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id odio ex. Integer quis erat at quam luctus dapibus in eget sem. Praesent gravida posuere mollis. Etiam venenatis commodo interdum. Morbi imperdiet sapien et risus consequat, ac malesuada elit lacinia. Cras efficitur arcu eget rhoncus pulvinar. Etiam viverra odio eget elit tempus condimentum. Maecenas ut finibus magna.',
            'final_dignosis' => 'Quisque sodales sapien libero, a accumsan magna dignissim et. Sed blandit consequat porta. Vivamus dignissim pharetra vehicula. Nunc laoreet ante a luctus mattis. Sed tempor ipsum rutrum, iaculis tortor sit amet, fringilla ligula. Donec ultricies, justo et pellentesque scelerisque, quam justo volutpat sem, ut efficitur risus erat eget tortor. Aliquam ultrices id ex eu imperdiet. Curabitur tellus tortor, malesuada gravida aliquet sit amet, dignissim sit amet metus.',
            'disease' => 'Kira',
            'tdisease' => 'Non-treatable',
            'examination' => 'Nulla et fermentum nibh. Vivamus ac lorem sit amet eros interdum malesuada rutrum quis elit. Curabitur sapien magna, fringilla vitae quam et, tincidunt hendrerit orci. Etiam vel pretium ipsum. Phasellus vitae semper magna. Morbi sed ipsum iaculis, rhoncus libero quis, egestas turpis. Nullam in nunc vehicula sapien lobortis finibus. Ut tristique vel justo in gravida. Donec facilisis sodales mauris, non vestibulum enim tristique ac. Sed aliquam lorem tortor, quis condimentum mi viverra eu. Nunc nec magna viverra, rutrum libero nec, dictum elit. Ut lorem justo, dapibus id accumsan eu, tempus et justo. Mauris ultrices aliquet ornare. Donec tempus bibendum nisl. Nunc cursus dictum eros, ac lobortis felis egestas sodales.',
            'treatment' => 'Pellentesque sit amet arcu rhoncus, laoreet quam et, volutpat nibh. Donec bibendum, mauris ac mattis malesuada, nibh nisi dictum tortor, quis tincidunt turpis felis vitae risus. Vestibulum neque orci, ultrices nec dui eget, pulvinar iaculis ante. Phasellus ac felis cursus, aliquet sapien sed, molestie mi. Cras mattis dui ut sem elementum, et porta nulla rutrum. Mauris vitae rutrum velit. Phasellus molestie ipsum at nisi vehicula ultricies. Morbi non dui sit amet nibh efficitur ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus ut tortor vitae lorem faucibus maximus at sed dui. Pellentesque fermentum porttitor sem, ac dictum nunc molestie sed. Aliquam at ipsum id ligula faucibus venenatis. Sed eros felis, finibus quis dolor et, sodales pulvinar enim. Nunc a quam lacinia, cursus elit non, vestibulum elit. Proin consequat risus a maximus blandit. Quisque a tincidunt eros, sit amet dapibus massa.',
            'finished' => '0',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]);


    }
}
