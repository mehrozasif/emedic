<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id');
            $table->integer('patient_id');
            $table->string('department');
            $table->text('initial_dignosis');
            $table->text('final_dignosis');
            $table->string('disease');
            $table->string('tdisease');
            $table->text('examination');
            $table->text('treatment');
            $table->integer('finished');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
