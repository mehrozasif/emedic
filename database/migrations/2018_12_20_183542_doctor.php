<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Doctor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('grade');
            $table->string('department');
            $table->longText('bio')->nullable();
            $table->text('certifications')->nullable();
            $table->text('interests')->nullable();
            $table->text('awards')->nullable();
            $table->string('specialization');
            $table->text('patients')->nullable(); //It will be patient_id,patient_id,patient_id e.g 10,2,35, In API, it will be given in JSON Array
            $table->text('patients_treated')->nullable(); //same as above
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
